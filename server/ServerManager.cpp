
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include "SDL.h"
#include "common.h"
#include "ServerManager.h"
#include "Server.h"

using namespace std;


ServerManager::ServerManager()
{
	m_clients.setMessageHandler(*this);
	m_playerLowestId = 0;
	m_playersWaiting = 0;
}

ServerManager::~ServerManager()
{
}

int ServerManager::findHost(IServer *pServer, Message &msg)
{
	playerMap_t::iterator iMap = m_playerMap.find(msg.dstPlayer);
	if(iMap == m_playerMap.end()) {
		ERROR << "Failed to find player for server." << endl;
		return -1;
	}
	ServerManagerMap *pMap = iMap->second;
	//
	// sanity check on playerId's.
	//
	if(msg.dstPlayer != pMap->pPlayer->playerId) {
		ERROR << "Sanity check on playerId's failed" << endl;
		return -1;
	}
	//
	// sanity check on servers
	//
	if(pServer != pMap->pServer) {
		ERROR << "Sanity check on IServer's failed" << endl;
		return -1;
	}
	return pMap->host;
}

int ServerManager::dispatch(IServer *pServer, Message& msg)
{
	int host = findHost(pServer, msg);
	if(host < 0)
		return -1;

	if(m_clients.send((unsigned int)host, msg) <= 0) {
		ERROR << "Failed to send msg: " << endl;
		msg.dump();
		return -1;
	}
	return 1;
}

int ServerManager::broadcast(IServer *pServer, Message& msg)
{
	/*
	 * TODO: need a new multimap from IServer to ServerManagerMap
	 *
	int host = findHost(pServer, msg);
	if(host < 0)
		return -1;

	if(m_clients.broadcast((unsigned int)host, msg) <= 0) {
		ERROR << "Failed to send msg: " << msg;
		return -1;
	}
	*/
	return 1;
}

int ServerManager::msgHandler(unsigned int host, Message& msg)
{
	hostMap_t::iterator iterFind = m_hostMap.find(host);
	if(iterFind != m_hostMap.end())
	{
		//
		// host entry exists, retrieve the player and server info
		//
		ServerManagerMap *pMap = m_hostMap[host];

		if(pMap->pServer == NULL) {
			//
			// player has no server assigned to it... TODO
			//
			return 1;
		}
		//
		// pass the message to the server the player is on
		//
		return pMap->pServer->msgHandler(msg);
	}
	else
	{
		// uhh.. unknown host value, disconnect it
		ERROR << "Received unknown host: " << host << endl;
		return -1; 
	}

	return 1;
}

MessagePlayer* ServerManager::getPlayer(playerId_t playerId)
{
	try {
		m_players.at(playerId);
	}
	catch(exception &e) {
		ERROR << "Invalid playerId: " << playerId << endl;
		return NULL;
	}
	return m_players[playerId];
}

int ServerManager::listen(const char *addr, short port)
{
	return m_clients.listen(addr, port);
}

int ServerManager::playerInsert(MessagePlayer *pPlayer)
{
	vector<MessagePlayer*>::iterator iPlayer;

	if(m_playerLowestId == m_players.size())
		goto push_at_end;
	//
	// insertion somewhere in the array.  insert it immediately.
	//
	pPlayer->playerId = m_playerLowestId;
	m_players[m_playerLowestId] = pPlayer;
	//
	// now the pain.. finding the next lowest empty player spot.
	//
	iPlayer = m_players.begin() + m_playerLowestId+1; // start at lowest id
	while(iPlayer != m_players.end())
	{
		if(*iPlayer == NULL)
			break; // we the newest unused player slot
		++iPlayer;
	}
	if(iPlayer == m_players.end())
		goto push_at_end;
	//
	// record the index of the newly found lowest playerId
	//
	m_playerLowestId = iPlayer - m_players.begin();
	return 1;

push_at_end:
	//
	// insertion at end of array
	//
	pPlayer->playerId = m_players.size();
	m_players.push_back(pPlayer);
	m_playerLowestId = m_players.size();
	return 1;
}

int ServerManager::execute()
{
	try
	{
		//
		// listen for new client connections.  an exception will be
		// thrown if there is one.
		//
		if(m_clients.acceptClients() < 0)
			return -1;
	}
	catch(HostConnectException& e)
	{
		unsigned int host = e.host; // exception contains unique hostId

		USER << "Got new client connection from hostId: " << host << endl;

		hostMap_t::iterator iterFind = m_hostMap.find(host);
		if(iterFind != m_hostMap.end())
		{
			//
			// TODO: a host with this id already exists... wtf
			//
		}
		ServerManagerMap *pMap = new ServerManagerMap();
		m_serverMaps.push_back(pMap);
		pMap->host = host;

		MessagePlayer *pPlayer = new MessagePlayer;
		playerInsert(pPlayer); // aggregate and give it a unique playerId
		pMap->pPlayer = pPlayer;
		//
		// create reverse mappings to the ServerManagerMap class from
		// both unique hostId's and playerId's
		//
		m_hostMap[host] = pMap;
		m_playerMap[pPlayer->playerId] = pMap;
		//
		// send an initial player message to the client so they can
		// fill in their info and send it back
		//
		DUMP << "Sending MessagePlayer to hostId: " << pMap->host << endl;
		m_clients.send(host, *pPlayer);
		//
		// if we now have enough players to play a game, allocate a
		// new Server and assign each player to the server.
		//
		m_playersWaiting++;
		if(m_playersWaiting >= MAX_PLAYER)
			spawnNewServer();
	}

	//
	// check all the clients for incomming messages.  This will call
	// the message handler if a message is received.
	//
	try
	{
		if(m_clients.checkClients() < 0)
			return -1;
	}
	catch(HostCloseException& e)
	{
		USER << "Client closed connection with hostId: " << e.host << endl;
		//
		// TODO: cleanup the ServerManagerMap mess this just created.
		//
	}
	return 1;
}

int ServerManager::spawnNewServer()
{
	MessageEvent event;
	//
	// We pass a reference of ourself to the new server so that the
	// server knows where to send outgoing messages to.  We use an
	// IMessageHandler here to do that.
	//
	Server *pSrv = new Server(*this);
	m_servers.push_back(pSrv);

	//
	// find new players to join the new server.
	// 
	// TODO: replace this with a more efficient search algorithm.
	//
	hostMap_t::iterator iMap = m_hostMap.begin();
	for(; iMap != m_hostMap.end(); ++iMap)
	{
		ServerManagerMap *pMap = iMap->second;

		if(pMap->pServer)
			continue;
		pMap->pServer = pSrv;
		//
		// send GAME_START event to this player.  This should start
		// the game on the client side.
		//
		event.code = MSG_CODE_GAME_START;
		event.dstPlayer = pMap->pPlayer->playerId;
		m_clients.send(pMap->host, event);

		m_playersWaiting--;
		if(!m_playersWaiting)
			break;
	}
	return 1;
}


