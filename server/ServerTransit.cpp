
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include <vector>
#include "ServerTransit.h"
#include "common.h"
#include "RemoteHost.h"

using namespace std;

#define MAX_SOCKETS 1024


class ServerTransitData
{
public:
	IPaddress m_listenAddr;
	TCPsocket m_listenSock;

	//
	// ServerTransitData aggregates the RemoteHosts
	//
	vector<RemoteHost*> m_hosts;
	unsigned int m_emptyHostId;
	unsigned int m_maxHostId;

	SDLNet_SocketSet m_sockSet;

	//
	// Data Helper Methods
	//
	RemoteHost* getHost(unsigned int host) { return m_hosts[host]; }
	int deleteHost(unsigned int host)
	{
		if(host >= m_maxHostId)
			return 0;
		if(m_hosts[host] == NULL)
			return 1;
		m_hosts[host]->setDel(m_sockSet);
		delete m_hosts[host];
		m_hosts[host] = NULL;

		if(host < m_emptyHostId)
			m_emptyHostId = host;
		return 1;
	}

	void deleteHosts()
	{
		unsigned int i;

		for(i = 0; i < m_maxHostId; i++)
		{
			if(m_hosts[i] != NULL) {
				m_hosts[i]->setDel(m_sockSet);
				delete m_hosts[i];
				m_hosts[i] = NULL;
			}
		}
		m_emptyHostId = 0;
	}

	void resetEmptyHostId()
	{
		unsigned int i;

		for(i = m_emptyHostId; i < m_maxHostId; i++)
			if(m_hosts[i] == NULL)
				break;
		m_emptyHostId = i;
	}

	unsigned int addHost(RemoteHost& host)
	{
		unsigned int hostId = m_emptyHostId;

		host.m_hostId = hostId;
		if(m_emptyHostId >= m_maxHostId)
		{
			m_maxHostId += 6; // we just consumed maxHostId.. incr by 5
			m_hosts.reserve(m_maxHostId);
			unsigned int i;
			for(i = m_maxHostId-5; i < m_maxHostId; i++)
				m_hosts[i] = NULL;
		}
		m_hosts[hostId] = &host;
		resetEmptyHostId();
		host.setAdd(m_sockSet);
		return hostId;
	}
};

/*
 */

ServerTransit::ServerTransit()
{
	SDLNet_Init();
	m_data = new ServerTransitData();

	m_data->m_listenSock = NULL;
	m_data->m_emptyHostId = 0;
	m_data->m_maxHostId = 0;

	m_data->m_sockSet = SDLNet_AllocSocketSet(MAX_SOCKETS);
}

ServerTransit::~ServerTransit()
{
	if(m_data->m_listenSock != NULL)
		SDLNet_TCP_Close(m_data->m_listenSock);

	SDLNet_Quit();

	m_data->deleteHosts();
	delete m_data;
}

int ServerTransit::listen(const char* addr, short port)
{
	//
	// convert from host/port to IP address
	//
	if(SDLNet_ResolveHost(&m_data->m_listenAddr, addr, port) < 0) {
		WARN << "Couldn't resolve hostname: " << addr << endl;
		return 0;
	}
	//
	// try to start listening on address
	//
	m_data->m_listenSock = SDLNet_TCP_Open(&m_data->m_listenAddr);
	if(m_data->m_listenSock == NULL) {
		WARN << "Failed to listen on hostname: " << addr << endl;
		return 0;
	}
	return 1;
}

int ServerTransit::close(unsigned int host)
{
	return m_data->deleteHost(host);
}

int ServerTransit::send(unsigned int host, Message& msg)
{
	RemoteHost *pHost = m_data->getHost(host);
	if(pHost == NULL) 
		return -1;
	return pHost->send(msg);
}

int ServerTransit::acceptClients()
{
	TCPsocket sock;
	IPaddress *pAddr;

	if(m_data->m_listenSock == NULL) {
		ERROR << "Not listening yet" << endl;
		return -1;
	}
	sock = SDLNet_TCP_Accept(m_data->m_listenSock);
	if(sock == NULL)
		return 0;
	pAddr = SDLNet_TCP_GetPeerAddress(sock);

	RemoteHost *pHost = new RemoteHost(*pAddr, sock);
	if(pHost == NULL) {
		ERROR << "Failed to allocate new client" << endl;
		return -1;
	}
	unsigned int hostId = m_data->addHost(*pHost);
	throw HostConnectException(hostId);
	return 0;
}

int ServerTransit::checkClients()
{
	int i;
	unsigned int iHost;
	vector<Message*> msgs;
	Message *msg;

	//
	// TODO: IMPORTANT: sometimes the RemoteHost will still have data
	// in its ByteBuf, but won't trigger the socketSet... so we need
	// to be checking all the clients for emtpy ByteBuf's.
	//
	i = SDLNet_CheckSockets(m_data->m_sockSet, 0); // non-blocking
	if(i < 0) {
		ERROR << "CheckSockets failed: " << SDLNet_GetError() << endl;
		return -1; // probably some system error
	}

	for(iHost = 0; iHost < m_data->m_maxHostId; iHost++)
	{
		if(m_data->m_hosts[iHost] == NULL)
			continue;
		RemoteHost *pHost = m_data->m_hosts[iHost];

		if(!pHost->ready())
			continue;

		try
		{
			msgs = pHost->get();

			vector<Message*>::iterator iter = msgs.begin();
			for(; iter != msgs.end(); ++iter)
			{
				msg = *iter;

				if(m_pMsgHandler->msgHandler(pHost->m_hostId, *msg) < 0)
				{
					// message handler doesn't like this host.. delete it.
					m_data->deleteHost(iHost);
					throw HostCloseException(iHost);
				}
			}
		}
		catch(RemoteCloseException& e)
		{
			m_data->deleteHost(iHost);
			throw HostCloseException(iHost);
		}
	}
	return 1;
}


