
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#ifndef SERVER_MANAGER_H
#define SERVER_MANAGER_H

#include <map>
#include <queue>
#include <vector>
#include "IServerMsgHandler.h"
#include "ServerTransit.h"
#include "IServer.h"
#include "Messages.h"

class ServerManagerMap
{
public:
	ServerManagerMap()
	{
		pPlayer = NULL;
		host = 0;
		pServer = NULL;
	}

	~ServerManagerMap()
	{
		if(pPlayer != NULL)
			delete pPlayer;
	}

	MessagePlayer *pPlayer;
	unsigned int host;
	IServer *pServer;
};

//!
// map for incoming messages.  we want to map the incoming hostId to
// the appropriate ServerManagerMap class.
typedef std::map<unsigned int, ServerManagerMap*> hostMap_t;

//!
// map for outgoing messages. Maps the universal playerId to a
// ServerManagerMap class in order to get at the 'host' param.
typedef std::map<playerId_t, ServerManagerMap*> playerMap_t;

//!
// map IServer to multiple ServerManagerMaps
typedef std::multimap<IServer*, ServerManagerMap*> serverMap_t;


class ServerManager : public IServerMsgHandler
{
public:
	ServerManager();
	~ServerManager();

	int dispatch(IServer *pServer, Message& msg);
	int broadcast(IServer *pServer, Message& msg);
	int msgHandler(unsigned int host, Message& msg);
	MessagePlayer* getPlayer(playerId_t playerId);

	int listen(const char *addr, short port);
	int execute();
protected:

	int findHost(IServer *pServer, Message &msg);
	int playerInsert(MessagePlayer *pPlayer);
	int spawnNewServer();

	//!
	// The Transit handles all things socket related, and translates
	// between funky sockets and a unique unsigned int host id.
	ServerTransit m_clients;

	//!
	// This maps the unique host id from ServerTransit to both a
	// Server and a player on that server.  It's possible that the
	// Server field is NULL, in which the player is in waiting mode to
	// start a game with other players.
	hostMap_t m_hostMap;
	playerMap_t m_playerMap;

	std::deque<ServerManagerMap*> m_serverMaps;

	//!
	// Since ServerManagerMap can contain duplicate Server's, we
	// need a consistent aggregation of the Servers we have created.
	std::deque<IServer*> m_servers;
	std::vector<MessagePlayer*> m_players; // unique playerId for each player
	playerId_t m_playerLowestId; // prefer unfragmented player vector

	//!
	// Number of players waiting to join a game.  In the future I
	// would remove this, and let players communicate in limbo to
	// organize a game.  For now, when this value reaches MAX_PLAYERS,
	// a game is started and each player is assigned to the new
	// Server.
	unsigned int m_playersWaiting;
};

#endif

