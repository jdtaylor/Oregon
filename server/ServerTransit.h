
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#ifndef SERVER_TRANSIT_H
#define SERVER_TRANSIT_H

#include <string>
#include "Message.h"
#include "IServerMsgHandler.h"


class HostConnectException
{
public:
	HostConnectException(unsigned int host) { this->host = host; }
	unsigned int host;
};

class HostCloseException
{
public:
	HostCloseException(unsigned int host) { this->host = host; }
	unsigned int host;
};

/// \brief Forward declaration of hidden data in ServerTransit class
class ServerTransitData;

/*! \brief Takes care of the transmission of messages.
 *
 * Used by the server to abstract the message passing implementation.
 * It hides its data structures in the implementation file, and allocates
 * it in its constructor.
 */

class ServerTransit
{
public:
	ServerTransit();
	~ServerTransit();

	void setMessageHandler(IServerMsgHandler& msgHandler)
	{
		m_pMsgHandler = &msgHandler;
	}

	int listen(const char *addr, short port);
	int close(unsigned int host);
	int send(unsigned int host, Message& msg);
	int acceptClients();
	int checkClients();
protected:
	IServerMsgHandler *m_pMsgHandler;
	ServerTransitData *m_data;
};

#endif

