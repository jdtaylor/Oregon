
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#ifndef SERVER_H
#define SERVER_H

#include "IServer.h"
#include "IServerMsgHandler.h"
#include "ServerPhase.h"
#include "State.h"

/*! \brief Implements the server interface.
 */
class Server : public IServer
{
public:
	Server(IServerMsgHandler& msgHandler);
	virtual ~Server();
	int execute();
	int dispatch(Message& msg);
	int broadcast(Message& msg);
	int msgHandler(Message& msg);
	State& getState() { return m_state; }

protected:
	IServerMsgHandler& m_outHandler; //! place to send outgoing messages
	State m_state;
	ServerPhase *m_phase[MAX_PHASE];

private:
	Server();
	Server& operator=(const Server&);
};

#endif
