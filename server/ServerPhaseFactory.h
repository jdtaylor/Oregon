
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#ifndef SERVER_PHASE_FACTORY_H
#define SERVER_PHASE_FACTORY_H

#include "ServerPhase.h"
#include "oregon_defs.h"

class ServerPhaseFactory
{
public:
	ServerPhaseFactory(IServer& server);
	virtual ~ServerPhaseFactory() {} // GCC 4.0 whines if this isn't here

	//! \brief Constructs a phase class for each phase of the game.
	//
	// This is an Abstract Factory Class.  It should be overriden with
	// an implementation specific phase factory. 
	//
	virtual int create(ServerPhase* phase[MAX_PHASE]) = 0;

protected:
	IServer& m_server;

private:
	ServerPhaseFactory();
	ServerPhaseFactory& operator=(const ServerPhaseFactory&);
};

#endif

