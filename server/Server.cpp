
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include "Server.h"
#include "basic/ServerPhaseFactoryBasic.h"


Server::Server(IServerMsgHandler& msgHandler)
	: m_outHandler(msgHandler)
{
	ServerPhaseFactory *pPhaseFactory;

	pPhaseFactory = (ServerPhaseFactory*)new ServerPhaseFactoryBasic(*this);

	//
	// fill in the m_phase array with pointers to the newly
	// constructed phases.
	//
	pPhaseFactory->create(m_phase); // constructs each phase class

	//
	// we only need the factory for creation.  we don't need it anymore.
	//
	delete pPhaseFactory;

	//
	// start in the title phase
	//
	m_state.sys->phase = PHASE_TITLE;

	//
	// tell the phase to wake up and draw its stuff
	//
	m_phase[m_state.sys->phase]->open();
}

Server::~Server()
{
}

int Server::execute()
{
	return 1;
}

int Server::dispatch(Message& msg)
{
	return m_outHandler.dispatch(this, msg);
}

int Server::broadcast(Message& msg)
{
	return m_outHandler.broadcast(this, msg);
}

int Server::msgHandler(Message& msg)
{
	//
	// Incoming message from a client directed toward us.  Send the
	// message to the current phase.
	//
	return m_phase[m_state.sys->phase]->msgHandler(msg);;
}


