/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		Gerald Jones <grjones@mail.uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include <iostream>

#include <cppunit/TestRunner.h>
#include <cppunit/TestResult.h>
#include <cppunit/TestResultCollector.h>
#include <cppunit/extensions/HelperMacros.h>
#include <cppunit/BriefTestProgressListener.h>
#include <cppunit/extensions/TestFactoryRegistry.h>

#include "SDL/SDL.h"
#include "Log.h"
#include "MessageFactory.h"
#include "ClientTransit.h"
#include "ServerTransit.h"

class ServerMsgHandler;
class MyLogCallback;

class TestMsgPass : public CPPUNIT_NS::TestCase
{
	CPPUNIT_TEST_SUITE(TestMsgPass);
	CPPUNIT_TEST(test);
	CPPUNIT_TEST_SUITE_END();
public:
	void setUp()
	{
	}
	void tearDown()
	{
	}
protected:
	void test()
	{
		unsigned int i, j;
		ServerMsgHandler msgHandler;
		Message* pMsg;
		LogCallback* origLogCallback;
		MyLogCallback myLogCallback;

		origLogCallback = Log::Instance().getCallback();

		//
		// any messages to the server will be passed to this interface
		//
		server.setMessageHandler(msgHandler);

		//
		// server listens on 0.0.0.0.  I'm not sure how to get it
		// to only listen on localhost.
		//
		CPPUNIT_ASSERT(server.listen(0, 1337) > 0);

		//
		// client connects to the server via localhost
		//
		string host("localhost");
		CPPUNIT_ASSERT(client.connect(host, 1337) > 0);

		//
		// wait for server to receive connection request from client
		//
		j = 0;
		while(!server.acceptClients() && j < TIMEO) {
			SDL_Delay(1000);
			j++;
		}

		for(i = MSG_EVENT; i < MAX_MSG; i++)
		{
			const char *msg_name = g_msgTypeNames[i];

			Message* msg = newMsg((MsgType)i);

			if(i == MSG_PLAYER) {
				// test string serialization
				MessagePlayer *p = dynamic_cast<MessagePlayer*>(msg);
				p->name = "unit testing thingy";
			}

			client.send(*msg);

			//
			// server waits for message from client, message will go to
			// the handler defined above.
			//
			j = 0;
			while(server.checkClients() <= 0 && j < TIMEO) {
				SDL_Delay(1000);
				j++;
			}
			CPPUNIT_ASSERT(j < TIMEO);

			//
			// client waits for server's bounce
			//
			j = 0;
			while((pMsg = client.poll()) == NULL && j < TIMEO) {
				SDL_Delay(1000);
				j++;
			}
			CPPUNIT_ASSERT(pMsg);

			//
			// do byte-for-byte comparison of before and after messages
			//
			string str1, str2;

			Log::Instance().setCallback(myLogCallback);

			myLogCallback.reset();
			msg->dump();
			str1 = myLogCallback.m_str;
			//
			myLogCallback.reset();
			pMsg->dump();
			str2 = myLogCallback.m_str;

			Log::Instance().setCallback(*origLogCallback);

			CPPUNIT_ASSERT(str1 == str2);
			delete pMsg;
			delete msg;
		}
	}
};

CPPUNIT_TEST_SUITE_REGISTRATION(TestMsgPass);

//
// Set timeout to 5 seconds.
//
#define TIMEO 5

ServerTransit server;
ClientTransit client;

class ServerMsgHandler : public IMessageHandler
{
public:
	int messageHandle(unsigned int host, Message& msg)
	{
		server.send(host, msg); // bounce msg back to client
		return 1;
	}
};

class MyLogCallback : public LogCallback
{
public:
	int operator() (LogType type, const string& msg)
	{
		m_str += msg;
		return 1;
	}

	void reset()
	{
		m_str = "";
	}

	string m_str;
};
