
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		Gerald Jones <grjones@mail.uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include <iostream>
#include <string>
#include "SDL/SDL.h"
#include "mutil/log.h"
#include "common/MessageFactory.h"
#include "client/ClientTransit.h"
#include "server/ServerTransit.h"

using namespace std;

//
// set timeout to 5 seconds
#define TIMEO 5

ServerTransit server;
ClientTransit client;

class ServerMsgHandler : public IServerMsgHandler
{
public:
	~ServerMsgHandler() {}
	int dispatch(IServer *pServer, Message& msg) { return 1; }
	int broadcast(IServer *pServer, Message& msg) { return 1; }

	int msgHandler(unsigned int host, Message& msg)
	{
		VLOG_USER("Server bouncing message: %s", g_msgTypeNames[msg.type]);
		server.send(host, msg); // bounce msg back to client
		return 1;
	}
	MessagePlayer* getPlayer(playerId_t playerId) { return NULL; }
};

class MyLogCallback : public LogCallback
{
public:
	~MyLogCallback() {}

	int operator() (LogType type, const std::string& msg)
	{
		m_str += msg;
		return 1;
	}

	void reset()
	{
		m_str = "";
	}

	string m_str;
};

int main(int argc, char *argv[])
{
	unsigned int i, j;
	ServerMsgHandler msgHandler;
	vector<Message*> msgs;
	LogCallback* origLogCallback;
	MyLogCallback myLogCallback;

	origLogCallback = Log::Instance().getCallback();

	//
	// any messages to the server will be passed to this interface
	//
	server.setMessageHandler(msgHandler);

	//
	// server listens on 0.0.0.0.  I'm not sure how to get it
	// to only listen on localhost.
	//
	if(server.listen(0, 1337) <= 0) {
		LOG_ERR("Unit Test Failed");
		return 1;
	}
	//
	// client connects to the server via localhost
	//
	string host("localhost");
	if(client.connect(host.c_str(), 1337) <= 0) {
		LOG_ERR("Unit Test Failed");
		return 1;
	}
	//
	// wait for server to receive connection request from client
	//
	j = 0;
	try
	{
		//
		// listen for new client connections.  an exception will be
		// thrown if there is one.
		//
		if(server.acceptClients() < 0)
			return -1;
	}
	catch(HostConnectException& e)
	{
		LOG_USER("Server got connection from client");
	}
//	while(!server.acceptClients() && j < TIMEO) {
//		SDL_Delay(1000);
//		j++;
//	}

	for(i = MSG_EVENT; i < MAX_MSG; i++)
	{
		const char *msg_name = g_msgTypeNames[i];

		Message* msg = newMsg((MsgType)i);

		if(i == MSG_PLAYER) {
			// test string serialization
			MessagePlayer *p = dynamic_cast<MessagePlayer*>(msg);
			p->name = "unit testing thingy";
		}
		VLOG_USER("Client sending message: %s", msg_name);

		client.send(*msg);

		//
		// server waits for message from client, message will go to
		// the handler defined above.
		//
		j = 0;
		while(server.checkClients() <= 0 && j < TIMEO) {
			SDL_Delay(1000);
			j++;
		}
		if(j >= TIMEO) {
			LOG_ERR("Unit Test Failed: Server failed to receive msg");
			return 1;
		}
		//
		// client waits for server's bounce
		//
		j = 0;
		msgs = client.poll();
		while(msgs.size() == 0 && j < TIMEO) {
			SDL_Delay(1000);
			msgs = client.poll();
			j++;
		}
		if(msgs.size() == 0) {
			LOG_ERR("Unit Test Failed: Client failed to receive bounced msg");
			return 1;
		}
		vector<Message*>::iterator iMsg = msgs.begin();
		for(; iMsg != msgs.end(); ++iMsg)
		{
			Message *pMsg = *iMsg;

			VLOG_USER("Client got message: %s", g_msgTypeNames[pMsg->type]);
			//
			// do byte-for-byte comparison of before and after messages
			//
			string str1, str2;

			Log::Instance().setCallback(&myLogCallback);

			myLogCallback.reset();
			msg->dump();
			str1 = myLogCallback.m_str;
			//
			myLogCallback.reset();
			pMsg->dump();
			str2 = myLogCallback.m_str;

			Log::Instance().setCallback(origLogCallback);

			if(str1 != str2)
			{
				VLOG_ERR("Unit Test Failed: message: '%s' transit mismatch",
						msg_name);
				LOG_DUMP("#############  Message1:");
				msg->dump();
				LOG_DUMP("#############  Message2:");
				pMsg->dump();
				return 1;
			}
			//delete pMsg; // TODO: no clue
		}
		delete msg;
	}
	return 0;
}


