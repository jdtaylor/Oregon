
/*
 * tests that the RAND() function
 * is returning the correct ranges
 * given lots of trial runs.
 */

#include <stdio.h>
#include "../src/common.h"

typedef struct _range {
	int min;
	int max;
} range_t;

#define NRANGE 4
const static range_t g_range[NRANGE] =  {
	{ 4,	20 },
	{ -10,	10 },
	{ 0,	200 },
	{ -2342,	4563}
};

int main(int argc, char *argv)
{
	long i;
	long n = 999999;
	int min, max;
	int r;
	int iRange;
	unsigned int prand; // keeps state for rand_r()

	for(iRange = 0; iRange < NRANGE; iRange++)
	{
		min = RAND(&prand, g_range[iRange].min, g_range[iRange].max);
		max = RAND(&prand, g_range[iRange].min, g_range[iRange].max);
		for(i = 0; i < n; i++)
		{
			r = RAND(&prand, g_range[iRange].min, g_range[iRange].max);
			if(r < min)
				min = r;
			if(r > max)
				max = r;
		}
		printf("[%d,%d] random ranges: [%d,%d]\n", g_range[iRange].min, g_range[iRange].max, min, max);
		if(min != g_range[iRange].min || max != g_range[iRange].max)
			printf("Unit Test Failed\n");
	}
	return 0;
}
