
#ifndef _MUTIL_VECT_H
#define _MUTIL_VECT_H

#include <cstring>
#include <cmath>
#include <exception>

namespace mutil
{

//!
// Out Of Bounds exception
//
class VectOobException : public std::exception
{
public:
	~VectOobException() throw() {}
};

//!
// Generic Vector
//
template <class Type, unsigned int Degree>
class Vect
{
public:
	Vect() { memset(v, 0, sizeof(Type) * Degree); }
	Vect(Type *vect) { memcpy(v, vect, sizeof(Type) * Degree); }
	Vect(const Vect<Type,Degree>& vect) {
		memcpy(v, vect.v, sizeof(Type) * Degree);
	}
	virtual ~Vect() {}

	size_t size() { return static_cast<size_t>(Degree); }

	Type operator[](unsigned int i) const
	{
		if(i >= Degree)
			throw VectOobException();
		return v[i];
	}

	Type& operator[](unsigned int i)
	{
		if(i >= Degree)
			throw VectOobException();
		return v[i];
	}

	Vect<Type,Degree>& operator=(const Vect<Type,Degree>& vect)
	{
		memcpy(v, vect.v, sizeof(Type) * Degree);
		return *this;
	}

	bool operator==(const Vect<Type,Degree>& vOther)
	{
		for(unsigned int i = 0; i < Degree; ++i)
			if(v[i] != vOther.v[i])
				return false;
		return true;
	}

	bool operator!=(const Vect<Type,Degree>& vect) { return !(*this == vect); }

	//!
	// Distance from vect to us
	//
	Type dist(Vect<Type,Degree> &vect)
	{
		int i;
		Type square;
		Type d = 0;

		for(i = 0; i < Degree; i++) {
			square = std::abs(v[i] - vect[i]);
			d += square * square;
		}
		return std::sqrt(d);
	}

protected:
	Type v[Degree];
};

typedef Vect<float,2> Vect2f;
typedef Vect<float,3> Vect3f;

}
#endif

