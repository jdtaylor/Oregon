
#ifndef _MUTIL_TEST_H
//#error "mutil/Test.h should only be included ONCE"
//#endif
#define _MUTIL_TEST_H

#include <string>
#include <vector>
#include <algorithm>
#include <exception>
#include <cstdlib>
#include "mutil/log.h"


namespace mutil
{

class TestFailException: public std::exception
{
public:
	TestFailException(const std::string reason="", const std::string file="",
			const int line=0, const std::string func="") :
		m_reason(reason), m_file(file), m_line(line), m_func(func) { }
	virtual ~TestFailException() throw() { }
	virtual std::string reason()
	{
		std::stringstream s;
		s << "<" << m_file << ":" << m_line << ":" << m_func << ">: " << m_reason;
		return s.str();
	}
private:
	std::string m_reason;
	std::string m_file;
	int m_line;
	std::string m_func;
};

class Test
{
public:
	virtual void run() = 0;
	virtual void start() {}
	virtual void stop() {}
	std::string name;
};

std::vector<Test*> g_tests;

Test* _test_add(Test* test, const char *name)
{
	test->name = name;
	g_tests.push_back(test);
	return test;
}
#define TEST_ADD(t) Test* g_pTest##t = _test_add(new t(), #t)

#define TEST_MSG_PASS std::cout << "PASS: "
#define TEST_MSG_FAIL std::cout << "FAIL: (" << file << ":" << line << ":" << func << ") "

void _test_run(const char *file, const int line, const char *func)
{
	std::cout << "Running " << g_tests.size() << " Tests:" << std::endl;

	std::vector<Test*>::iterator i = g_tests.begin();
	for(; i != g_tests.end(); ++i)
	{
		Test *pTest = *i;

		try {
			pTest->start();
			pTest->run();
			pTest->stop();
			TEST_MSG_PASS << pTest->name << std::endl;
		} catch(TestFailException &e) {
			TEST_MSG_FAIL << pTest->name << ": " << e.reason() << std::endl;
			exit(1);
		} catch(...) {
			TEST_MSG_FAIL << pTest->name << ": " << "Unknown Exception" << std::endl;
			exit(1);
		}
	}
}
#define TEST_RUN() _test_run(__FILE__, __LINE__, __FUNCTION__)
#define TEST_FAIL(m) throw TestFailException(m, __FILE__, __LINE__, __FUNCTION__)
#define TEST_ASSERT(t,m) if(!(t)) TEST_FAIL(m)

}

#endif
