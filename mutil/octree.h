
#ifndef _MUTIL_OCTREE_H
#define _MUTIL_OCTREE_H

#include <vector>
#include <stack>
#include "mutil/vect.h"

namespace mutil
{

template <typename T>
class Octree
{
private:
	typedef std::vector<T> Node;
	/*
	class Node
	{
	public:
		typename std::vector<T> n;
		*/
		//typename std::vector<T>::iterator i;
	/*
		Node()
		{
			int i;
			VECTINIT3F(vCenter, 0.0f);
			radius = 0.0f;
			for(i = 0; i < 8; i++)
				child[i] = NULL;
			val = NULL;
		}

		Vect3f vCenter;
		float radius;
		Node *child[8];
		T *val;
	*/
	/*
		Node& operator=(const Node& node)
		{
			n = node.n;
		}
	};
		*/

public:
	Octree() {}
	virtual ~Octree() {}

	void insert(T& node)
	{
		m_root.push_back(node);
	}

	std::vector<T> find(Vect3f &v, float reach)
	{
		std::vector<T> nodes;

		unsigned int i;
		for(i = 0; i < m_root.size(); i++)
		{
			Vect3f &pos = m_root[i]->getPos();
			float d = v.dist(pos);

			if(d < reach)
				nodes.push_back(m_root[i]);
		}
		return nodes;
	}


	class iterator
	{
	public:
		iterator(typename Node::iterator p) { cur = p; }
		~iterator() {}

		// The assignment and relational operators are straightforward
		iterator& operator=(const iterator& other)
		{
			cur = other.cur;
			return *this;
		}

		bool operator==(const iterator& other)
		{
			return cur == other.cur;
		}

		bool operator!=(const iterator& other)
		{
			return cur != other.cur;
		}

		// Update my state such that I refer to the next element
		iterator& operator++()
		{
			iterator *me = this;
			cur++;
			return *me;
		}

		iterator& operator++(int)
		{
			iterator tmp(*this);
			++(*this);
			return tmp;
		}

		// Return a reference to the value in the node.  I do this instead
		// of returning by value so a caller can update the value in the
		// node directly.
		T& operator*()
		{
			return *cur;
		}

		// Return the address of the value referred to.
		T* operator->()
		{
			return &(*cur);
		}

	private:
		typename Node::iterator cur;
		//std::stack<int> child;
	};

	iterator begin()
	{
		return iterator(m_root.begin());
	}

	iterator end()
	{
		return iterator(m_root.end());
	}

protected:
	Node m_root;

};


};

#endif

