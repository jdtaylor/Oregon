
#ifndef _MUTIL_SINGLETON_H
#define _MUTIL_SINGLETON_H

namespace mutil
{

/*! /brief The Singleton design pattern.
 *
 * http://en.wikipedia.org/wiki/Singleton_pattern
 */
template<typename T> class Singleton
{
public:
	static T& Instance()
	{
		static T instance; // only one instance
		return instance;
	}
};

}
#endif
