
#include <cerrno>
#include <cstdio>
#include <iostream>
#include <stdarg.h>
#include "log.h"

using namespace std;

namespace mutil
{
	class LogStream muser(LOG_TYPE_USER);
	class LogStream mwarn(LOG_TYPE_WARN);
	class LogStream merror(LOG_TYPE_ERROR);
	class LogStream mperror(LOG_TYPE_PERROR);
	class LogStream mdump(LOG_TYPE_DUMP);
}

using namespace mutil;

#define MAX_LOG_LEN 1024

//pthread_mutex_t log_mutex_t;

int LogCallback::operator() (LogType type, const string& msg)
{
	string str = msg;

	if(str[str.length()-1] == '\n')
		str.resize(str.length()-1); // strip trailing newline

	switch(type)
	{
	case LOG_TYPE_WARN:
		cerr << "[w ] " << str.data() << endl;
		fflush(stderr);
		break;
	case LOG_TYPE_ERROR:
		cerr << "[E ] " << str.data() << endl;
		fflush(stderr);
		break;
	case LOG_TYPE_PERROR:
		cerr << "[E ] " << str.data() << ": " << strerror(errno) << endl;
		fflush(stderr);
		break;
	case LOG_TYPE_DUMP:
		cout << "[d ] " << str.data() << endl;
		fflush(stdout);
		break;
	default:
	case LOG_TYPE_USER:
		cout << "[m ] " << str.data() << endl;
		fflush(stdout);
		break;
	}
	return 1;
}

namespace mutil
{

int logMsg(LogType type, const string& msg)
{
	return Log::Instance().logMsg(type, msg);
}

int vlogMsg(LogType type, const char *format, ...)
{
	char buf[MAX_LOG_LEN];
	va_list vl;
	string str;

	va_start(vl, format);
	vsnprintf(buf, MAX_LOG_LEN, format, vl);
	va_end(vl);

	buf[MAX_LOG_LEN-1] = '\0'; // just in case
	str = buf;
	return Log::Instance().logMsg(type, str);
}

int vflogMsg(LogType type, const char *file, int line, const char *format, ...)
{
	char buf[MAX_LOG_LEN];
	va_list vl;
	stringstream stream;

	va_start(vl, format);
	vsnprintf(buf, MAX_LOG_LEN, format, vl);
	va_end(vl);

	buf[MAX_LOG_LEN-1] = '\0'; // just in case
	stream << "(" << file << ":" << line << ") " <<  buf;
	return Log::Instance().logMsg(type, stream.str());
}

}

