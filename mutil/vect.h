
#ifndef _MUTIL_VECT_H
#define _MUTIL_VECT_H

#include <cstring>
#include <cmath>

namespace mutil
{

//!
// Out Of Bounds exception
//
class VectOobException {};

//!
// Generic Vector
//
template <class Type, unsigned int Degree>
class Vect
{
public:
	Vect() { memset(v, 0, sizeof(Type) * Degree); }
	Vect(Type *vect) { memcpy(v, vect, sizeof(Type) * Degree); }
	Vect(const Vect<Type,Degree>& vect) {
		memcpy(v, vect.v, sizeof(Type) * Degree);
	}
	virtual ~Vect() {}

	size_t size() { return static_cast<size_t>Degree; }

	Type operator[](unsigned int i) const
	{
		if(i < 0 || i >= Degree)
			throw VectOobException();
		return v[i];
	}

	Type& operator[](unsigned int i)
	{
		if(i < 0 || i >= Degree)
			throw VectOobException();
		return v[i];
	}

	Vect<Type,Degree>& operator=(const Vect<Type,Degree>& vect)
	{
		memcpy(v, vect.v, sizeof(Type) * Degree);
		return *this;
	}

	//!
	// Distance from vect to us
	//
	float dist(Vect<Type,Degree> &vect)
	{
		int i;
		float square;
		float f = 0.0f;

		for(i = 0; i < Degree; i++) {
			square = std::abs(v[i] - vect[i]);
			f += square * square;
		}
		return std::sqrt(f);
	}

protected:
	Type v[Degree];
};

typedef Vect<float,2> Vect2f;
typedef Vect<float,3> Vect3f;

}
#endif

