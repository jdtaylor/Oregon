
#ifndef _MUTIL_LOG_H
#define _MUTIL_LOG_H

#include <iostream>
#include <sstream>
#include <cstring>
#include <string>
#include "mutil/singleton.h"

namespace mutil
{

typedef enum
{
	LOG_TYPE_USER=0,
	LOG_TYPE_WARN,
	LOG_TYPE_ERROR,
	LOG_TYPE_PERROR,
	LOG_TYPE_DUMP,
	MAX_LOG_TYPE
} LogType;

/*! \brief Logging callback functor.
 *
 *  Use it to redirect global logging messages.
 */
class LogCallback
{
public:
	virtual ~LogCallback() {}
	virtual int operator() (LogType type, const std::string& msg);
};


/*! \brief Logging facility capable of being overriden.
 */
class Log : public Singleton<Log>
{
public:
	Log() : pfnCallback(new LogCallback()) { }
	virtual ~Log() { }

	int logMsg(LogType type, const std::string& msg)
	{
		return (*pfnCallback)(type, msg);
	}

	int setCallback(LogCallback *pFunc)
	{
		pfnCallback = pFunc;
		return 1;
	}

	LogCallback* getCallback() { return pfnCallback; }

protected:
	LogCallback *pfnCallback; // callback functor
};

/* \brief Global functions and macros for logging in this fashion.
 */
int logMsg(LogType type, const std::string& msg);
int vlogMsg(LogType type, const char *format, ...);
int vflogMsg(LogType type, const char *file, int line, const char *format, ...);

#define LOG_USER(msg) logMsg(LOG_TYPE_USER, msg)
#define LOG_WARN(msg) vflogMsg(LOG_TYPE_WARN, __FILE__, __LINE__, "%s", msg)
#define LOG_ERR(msg) vflogMsg(LOG_TYPE_ERROR, __FILE__, __LINE__, "%s", msg)
#define LOG_PERR(msg) vflogMsg(LOG_TYPE_PERROR, __FILE__, __LINE__, "%s", msg)
#define LOG_DUMP(msg) logMsg(LOG_TYPE_DUMP, msg)

#define VLOG_USER(msg, args...) vlogMsg(LOG_TYPE_USER, msg, args)
#define VLOG_WARN(msg, args...) vflogMsg(LOG_TYPE_WARN, __FILE__, __LINE__, msg, args)
#define VLOG_ERR(msg, args...) vflogMsg(LOG_TYPE_ERROR, __FILE__, __LINE__, msg, args)
#define VLOG_PERR(msg, args...) vflogMsg(LOG_TYPE_PERROR, __FILE__, __LINE__, msg, args)
#define VLOG_DUMP(msg, args...) vlogMsg(LOG_TYPE_DUMP, msg, args)


/*! \brief Logging class for cout-like stream access.  Use the
 *         predefined LogStreams below instead of cout/cerr/etc.
 */
class LogStream
{
public:
	LogStream(LogType t) { type = t; }

	template<typename T>
	LogStream& operator<<(const T& t)
	{
		stream << t;
		streamIn();
		return *this;
	}

	LogStream& operator<<(std::ostream& (*func)(std::ostream&))
	{
		stream << func;
		streamIn();
		return *this;
	}

	LogStream& operator<<(std::ios& (*func)(std::ios&))
	{
		stream << func;
		streamIn();
		return *this;
	}

	LogStream& operator<<(std::ios_base& (*func)(std::ios_base&))
	{
		stream << func;
		streamIn();
		return *this;
	}

protected:
	LogType type;
	std::ostringstream stream;

	void streamIn()
	{
		//
		// stream operation can only be performed on this class with a
		// predefined LogType
		if(type == MAX_LOG_TYPE) {
			logMsg(LOG_TYPE_ERROR, "stream operation; LogType needs to be set");
			return;
		}
		if(strstr(stream.str().c_str(), "\n") == NULL)
			return; // do half-assed-line buffering

		logMsg(type, stream.str());

		stream.str(""); // empty ostringstream
	}
};

// \brief These clases aren't meant to be used directly.  Instead use
// the MACROS provided below them.
extern class LogStream muser;
extern class LogStream mwarn;
extern class LogStream merror;
extern class LogStream mperror;
extern class LogStream mdump;

#define USER muser
#define WARN mwarn << "(" << __FILE__ << ":" << __LINE__ << ":" << __FUNCTION__ << ") "
#define ERROR merror << "(" << __FILE__ << ":" << __LINE__ << ":" << __FUNCTION__ << ") "
#define PERROR mperror << "(" << __FILE__ << ":" << __LINE__ << ":" << __FUNCTION__ << ") "
#define DUMP mdump

}
#endif

