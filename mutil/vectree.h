
#ifndef _MUTIL_VECTREE_H
#define _MUTIL_VECTREE_H

#include <vector>
#include <stack>
#include "mutil/vect.h"

namespace mutil
{

//!
// 1. Implements size_t size()
// 2. Implements or overloads operator[]
// 3. Types returned by operator[] overload '<' and '=='
//

template class<T>
class IVectTree
{
public:
    size_t size() const = 0;
    T& operator[](unsigned int) const = 0;
};

template class<T>
class VectTree : IVectTree<T>
{
public:
protected:
    std::vector<VectTree<T> > m_tree;
};


VectTree<Vect3f> octree;

};

#endif

