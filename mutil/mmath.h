
#ifndef _MUTIL_MATH_H
#define _MUTIL_MATH_H

#include <cmath>

namespace mutil
{

//!
// output a random value between min and max (inclusive)
// 'p' is an 'unsigned int*' value for keeping PRNG state (thread safety).
//
#define RAND(p,min,max) \
	((min)+(int)((double)((max+1)-(min))*rand_r(p)/(RAND_MAX+1.0)))

#ifndef MIN_INT
#define MIN_INT (-2147483600)
#endif
#ifndef MAX_INT
#define MAX_INT 2147483600
#endif

}
#endif

