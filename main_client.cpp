
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include <iostream>
#include <getopt.h>
#include "SDL.h"
#include "common.h"
#include "Client.h"

using namespace std;

#define CLIENT_VERSION 0.01f
#define DEFAULT_SERVER "127.0.0.1"
#define DEFAULT_PORT "55479"

static struct option opts[] = {
	{"help", no_argument, NULL,			'h'},
	{"verbose", no_argument, NULL,		'v'},
	{"quite", no_argument, NULL,		'q'},
	{"interactive", no_argument, NULL,	'i'},
	{"log", required_argument, NULL,	'l'},
	{"config", required_argument, NULL,	'c'},
	{"server", required_argument, NULL,	's'},
	{"port", required_argument, NULL,	'p'},
	{"ai", optional_argument, NULL,		'a'},
	{"name", required_argument, NULL,	'n'},
	{"watch", no_argument, NULL,		'w'},
	{"theme", required_argument, NULL,	't'},
	{NULL, 0, NULL, 0}
};

static FILE* g_fpLog = NULL;

int printUsage(char *appName);

//
// NOTE: for the Client and the Server to exist in the same process,
// the Client should be constructed first so it can call SDL_Init with
// SDL_INIT_TIMER, which the server uses for its timing
//

int main(int argc, char *argv[])
{
	char c;
	bool bQuite = false;
	bool bConsole = false;
	bool bSpectator = false;
	char theme[16] = "";
	char *confFile = NULL;
	char *server = NULL;
	char port[8] = DEFAULT_PORT;
	char *aiFile = NULL;
	char *tmpBuf = new char[PATH_MAX];

	while((c = getopt_long(argc, argv, "hvqil:c:s:p:a:n:wt:", opts, NULL)) >= 0)
	{
		switch(c)
		{
			case 'h':
				printUsage(argv[0]);
				exit(0);
				break;
			case 'v':
				bQuite = FALSE;
				break;
			case 'q':
				bQuite = TRUE;
				break;
			case 'l':
				g_fpLog = fopen(optarg, "w");
				break;
			case 'i':
				bConsole = TRUE;
				break;
			case 'c':
				strncpy(tmpBuf, optarg, sizeof(tmpBuf));
				tmpBuf[PATH_MAX-1] = '\0';
				if(confFile != NULL)
					FREE(confFile);
				confFile = (char*)MALLOC(sizeof(char) * (strlen(tmpBuf)+1));
				strcpy(confFile, tmpBuf);
				break;
			case 's':
				strncpy(tmpBuf, optarg, sizeof(tmpBuf));
				tmpBuf[PATH_MAX-1] = '\0';
				if(server != NULL)
					FREE(server);
				server = (char*)MALLOC(sizeof(char) * (strlen(tmpBuf)+1));
				strcpy(server, tmpBuf);
				break;
			case 'p':
				strncpy(port, optarg, sizeof(port));
				port[sizeof(port)-1] = '\0';
				break;
			case 'a':
				strncpy(tmpBuf, optarg, sizeof(tmpBuf));
				tmpBuf[PATH_MAX-1] = '\0';
				if(aiFile != NULL)
					FREE(aiFile);
				aiFile = (char*)MALLOC(sizeof(char) * (strlen(tmpBuf)+1));
				strcpy(aiFile, tmpBuf);
				break;
				/*
			case 'n':
				strncpy(moleClient.playerName, optarg, MAX_MOLE_PLAYER_NAME);
				moleClient.playerName[MAX_MOLE_PLAYER_NAME-1] = '\0';
				break;
				*/
			case 'w':
				bSpectator = TRUE;
				break;
			case 't':
				strncpy(theme, optarg, sizeof(theme));
				theme[sizeof(theme)-1] = '\0';
				break;
			case ':':
				// missing option argument
				printUsage(argv[0]);
				break;
		}
	}
	FREE(tmpBuf);
    Client client(CLIENT_FRONTEND_SDL);

	//
	// TODO: for testing client/server. REMOVE me later.
	//
	client.setServer("localhost", 55479);

	while(true)
	{
		if(client.execute() <= 0)
			break;
		//SDL_Delay(1000);
	}
	return 0;
}	

int printUsage(char *appName)
{
	printf("oregon client v%.3f [coded by UH COSC4351 Fall 2006]\n", CLIENT_VERSION);
	printf("usage: %s [options]\n", appName);
	printf("options:\n");
	printf("  help:      -h\n");
	printf("  verbose:   -v\n");
	printf("  quite:     -q\n");
	printf("  logfile:   -l <log file>\n");
	printf("  config:    -c <config file>\n");
	printf("  connect:   -s <ip-address> (default=%s)\n", DEFAULT_SERVER);
	printf("  port:      -p <port>       (default=%s)\n", DEFAULT_PORT);
	printf("  AI file:   -a <file>\n");
	printf("  name:      -n <player name>\n");
	printf("  spectator  -w\n");
	printf("  theme:     -t <theme_name>\n");
	printf("\n");
	printf("Themes supported in this build:\n");
	printf("\n");
	return 1;
}

