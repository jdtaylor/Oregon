
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#ifndef STATE_H
#define STATE_H

#include "oregon_defs.h"
#include "Messages.h"

class State
{
public:
	State();

    MessageSys			*sys;
    MessageStore		*store;
    MessagePlayer		*player[MAX_PLAYER];
    MessageMove			*playerPos[MAX_PLAYER];
    MessagePlotInfo		*plotInfo;
    MessagePlot			*plot[PLOT_NX][PLOT_NY];
    MessageDevel		*devel;
    MessageAuction		*auction;
    MessageStandings	*standings;
	//
	int grantPlot; // which land plot is currently highlighted for grant

	MessagePlayer* getPlayer(int playerid);
	//!
	// \brief Returns the amount of resource needed by player to
	// avoid penalties.  Only valid for RES_FOOD, and RES_ENERGY.
	//
	int playerResNeed(MessagePlayer *pPlayer, ResourceType res);
	int playerGoodsValue(MessagePlayer *pPlayer);
	int updatePlayerResource(int res);

	int getPlotsAvailable();
	void mapRandomize();
	
	int calcProduction();
	int calcStorePrice();
	int plotProduced[PLOT_NX][PLOT_NY]; // how much resource each plot produced
};

#endif
