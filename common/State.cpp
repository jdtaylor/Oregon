
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include <string>
#include <cstdio>
#include "State.h"
#include "common.h"
#include "MessageFactory.h"

using namespace std;


State::State()
{
	int i, j;

	sys = dynamic_cast<MessageSys*>(newMsg(MSG_SYS));
	store = dynamic_cast<MessageStore*>(newMsg(MSG_STORE));

	for(i = 0; i < MAX_PLAYER; i++)
	{
		player[i] = dynamic_cast<MessagePlayer*>(newMsg(MSG_PLAYER));
		char tempname[50];
		snprintf(tempname, 49, "player%d", i+1);
		player[i]->name = tempname;
		player[i]->playerId = i;

		playerPos[i] = dynamic_cast<MessageMove*>(newMsg(MSG_MOVE));
	}
	plotInfo = dynamic_cast<MessagePlotInfo*>(newMsg(MSG_PLOT_INFO));

	for(i = 0; i < PLOT_NX; i++)
	{
		for(j = 0; j < PLOT_NY; j++)
		{
			plot[i][j] = dynamic_cast<MessagePlot*>(newMsg(MSG_PLOT));
			plot[i][j]->plotId = plotInfo->nPlotX * j + i;
		}
	}
	devel = dynamic_cast<MessageDevel*>(newMsg(MSG_DEVEL));
	auction = dynamic_cast<MessageAuction*>(newMsg(MSG_AUCTION));
	standings = dynamic_cast<MessageStandings*>(newMsg(MSG_STANDINGS));

	grantPlot = -1;
}

MessagePlayer* State::getPlayer(int playerId)
{
	if(playerId < 0 || playerId >= MAX_PLAYER) {
		ERROR << "Invalid playerId: " << playerId << endl;
		return NULL;
	}
	return player[playerId];
}

int State::playerResNeed(MessagePlayer *pPlayer, ResourceType res)
{
	int x, y;
	float f;
	int need = 0;

	if(res == RES_FOOD)
	{
		//
		// food requirements go up as the rounds increase
		//
		f = (float)sys->round / MAX_GAME_ROUNDS;
		return (int)(f * g_playerResNeedMax[res]);
	}
	if(res == RES_ENERGY)
	{
		//
		// Energy requirements increase depending on player's plot count.
		//
		MessagePlot *pPlot;

		for(x = 0; x < PLOT_NX; x++)
		for(y = 0; y < PLOT_NY; y++)
		{
			pPlot = plot[x][y];
			if(pPlot->owner != pPlayer->playerId)
				continue;
			if(pPlot->resOutfit == RES_NONE)
				continue;
			if(pPlot->resOutfit == RES_ENERGY && pPlot->resProduced > 2)
				continue; // don't count producing energy plots
			need++;
		}
	}
	return need;
}

int State::playerGoodsValue(MessagePlayer *pPlayer)
{
	int i;
	int value = 0;

	for(i = 1; i <MAX_RESOURCE; i++)
		value += pPlayer->prev[i]*g_resType[i].value;

	return value;
}

int State::updatePlayerResource(int res)
{
	unsigned int i;
	for(i = 0; i < sys->nPlayer; i++)
	{
		MessagePlayer *pPlayer = player[i];

		pPlayer->prev[res] = pPlayer->resCur(res);
		pPlayer->use[res] = 0;
		pPlayer->spoil[res] = 0;
		pPlayer->prod[res] = 0;
	}
	return 1;
}

int State::getPlotsAvailable()
{
	int x, y;
	int nPlot = 0;

	for(x = 0; x < PLOT_NX; x++) {
		for(y = 0; y < PLOT_NY; y++) {
			if(plot[x][y]->owner < 0)
				nPlot++; // no owner
		}
	}
	return nPlot;
}

void State::mapRandomize()
{
	int x, y;

	// Seed the random number generator.
	srand(time(NULL));

	// Randomize the terrain.
	for(x = 0; x < PLOT_NX; x++)
	{
		for(y = 0; y < PLOT_NY; y++)
		{
			// Set the owner of that plot to -1 (no owner)
			plot[x][y]->owner = -1;
			// Set the resource for that plot to nothing.
			plot[x][y]->resOutfit = RES_NONE;
			// The store is always in the X and Y center.
			if(x == STORE_X && y == STORE_Y) {
				plot[x][y]->owner = MAX_PLAYER; // computer owns it
				plot[x][y]->terrain = TERRAIN_STORE;
				plot[x][y]->mountain = false;
				continue;
			}
			// The river must be surrounded by grass.
			if(x == RIVER_X - 1 || x == RIVER_X + 1)
				plot[x][y]->terrain = TERRAIN_GRASS;
			// The river is always in the X center.
			else if(x == RIVER_X)
				plot[x][y]->terrain = TERRAIN_RIVER;
			// The rest of the plots are either dirt or grass.
			else
				plot[x][y]->terrain = rand() % 2 ? TERRAIN_DIRT : TERRAIN_GRASS;

			// Mountains can't grow on rivers.  Mountains grow on
			// 1/3rd of the other plots.
			if(x == RIVER_X)
				plot[x][y]->mountain = false;
			else
				plot[x][y]->mountain = rand() % 3 == 2;

			//
			// maximum plot resource potential is 8.  we set it to 6
			// here because 8 is reserved for special events like
			// meteors or high ore with mountains.
			//
			int quality = rand() % 2 + 1;

			switch (plot[x][y]->terrain)
			{
			case TERRAIN_DIRT:
				plot[x][y]->resPotential[RES_FOOD] = 	quality * 1;
				plot[x][y]->resPotential[RES_ENERGY] = 	quality * 2;
				plot[x][y]->resPotential[RES_ORE]  = 	quality * 3;
				break;
			case TERRAIN_GRASS:
				plot[x][y]->resPotential[RES_FOOD] = 	quality * 2;
				plot[x][y]->resPotential[RES_ENERGY] = 	quality * 3;
				plot[x][y]->resPotential[RES_ORE]  = 	quality * 1;
				break;
			case TERRAIN_RIVER:
				plot[x][y]->resPotential[RES_FOOD] = 	quality * 3;
				plot[x][y]->resPotential[RES_ENERGY] = 	quality * 2;
				plot[x][y]->resPotential[RES_ORE]  = 	quality * 1;
				break;
			default:
				plot[x][y]->resPotential[RES_FOOD] = 0;
				plot[x][y]->resPotential[RES_ENERGY] = 0;
				plot[x][y]->resPotential[RES_ORE]  = 0;
				break;
			}
			//
			// oil is a separate "pot-luck" random, but doesn't occur
			// on mountains.
			//
			if(plot[x][y]->mountain)
			{
				plot[x][y]->resPotential[RES_ORE] += 2;
				plot[x][y]->resPotential[RES_OIL] = 0;
			}
			else
				plot[x][y]->resPotential[RES_OIL] = rand() % 6;
		}
	}
}

int State::calcProduction()
{
	unsigned int prand;
	int i, j;
	
	DUMP << "===-BEFORE-===" << endl;
	for(i = RES_NONE+1; i < MAX_RESOURCE; i++)
	for(j = 0; j < MAX_PLAYER; j++)
	{
		MessagePlayer *p = player[j];

		VLOG_DUMP("id:%d res:%s\t prev:%d use:%d spoil:%d prod:%d need:%d",
				p->playerId, g_resType[i].name.c_str(), p->prev[i],
				p->use[i], p->spoil[i], p->prod[i], p->need[i]);
	}

	srand(time(NULL));

	int x, y;
	for(x = 0; x < PLOT_NX; x++)
	for(y = 0; y < PLOT_NY; y++) 
	{
		plotProduced[x][y] = 0;

		MessagePlot *pPlot = plot[x][y];

		if(pPlot->owner < 0 || pPlot->owner >= MAX_PLAYER)
			continue; // not owned
		if(pPlot->resOutfit == RES_NONE)
			continue; // not outfitted

		MessagePlayer *pPlayer = getPlayer(pPlot->owner);
		if(pPlayer == NULL)
			continue;
		//
		// the amount of energy needed for a player depends on how
		// many plots of land the player owns
		//
		int energyNeed = playerResNeed(pPlayer, RES_ENERGY);

		ResourceType res = pPlot->resOutfit;
		
		//
		// give the game a bit of randomness no matter what.
		// DEVIATION is defined in oregon_defs.h
		//
		int potential = pPlot->resPotential[res] + 
			RAND(&prand, -PRODUCTION_DEVIATION, PRODUCTION_DEVIATION);
		if(potential < 0)
			potential = 0;
		if(potential > PRODUCTION_MAX)
			potential = PRODUCTION_MAX;

		int prod = potential;
		//
		// the amount of resource produced is dependent on how much
		// energy the player has.  If the player is lacking energy, 
		// ie. energyNeed > 0, then the full potential of the plot
		// should not be produced.
		//
		if(energyNeed < 0)
		{
			// hurt production based on radom DEVIATION
			prod -= RAND(&prand, 0, PRODUCTION_DEVIATION);
			if(prod < 0)
				prod = 0;
		}
		else // energyNeed > 0
		{
			// reward production for having higher than needed energy
			if(prod > PRODUCTION_MAX)
				prod = PRODUCTION_MAX;
		}
		//
		// accumulate all the players resources for all its plots
		//
		pPlayer->prod[res] += prod;
		plotProduced[x][y] = prod; // only one value per plot
	}
	
	for(i = 0; i < MAX_PLAYER; i++)
	{
		MessagePlayer *pPlayer = getPlayer(i);
		if(pPlayer == NULL)
			continue;
			
		int energyNeed = playerResNeed(pPlayer, RES_ENERGY);

		int res;
		for(res = 0; res < MAX_RESOURCE; res++)
		{	
			int prev = pPlayer->prev[res];
	
			//
			// amount of spoilage depends on randomization between set percents
			//
			float prct = (float)RAND(&prand, g_resType[res].minDecay,
									 g_resType[res].maxDecay) / 100.0f;
			pPlayer->spoil[res] = (int)(pPlayer->prev[res] * prct);
			if(pPlayer->spoil[res] > prev)
				pPlayer->spoil[res] = prev;
			prev -= pPlayer->spoil[res]; // keep track to not go negative
			//
			// amount of usage is also a set percent
			//
			if(res == RES_FOOD)
			{
				pPlayer->use[res] = (int)(FOOD_USAGE/100.0 * pPlayer->prev[res]);
			}
			else if(res == RES_ENERGY)
			{
				pPlayer->use[res] = energyNeed - 
								RAND(&prand, 0, PRODUCTION_DEVIATION);
				if(pPlayer->use[res] < 0)
				    pPlayer->use[res] = 0;
			}
			else
				pPlayer->use[res] = 0; // ORE and OIL aren't "used"
	
			if(pPlayer->use[res] > prev)
				pPlayer->use[res] = prev;

			//
			// prev[] stays the same, and is to be modified at end of auction.
			//
		}
	}

	DUMP << "===-AFTER-===" << endl;
	for(i = RES_NONE+1; i < MAX_RESOURCE; i++)
	for(j = 0; j < MAX_PLAYER; j++)
	{
		MessagePlayer *p = player[j];

		VLOG_DUMP("id:%d res:%s\t prev:%d use:%d spoil:%d prod:%d need:%d",
				p->playerId, g_resType[i].name.c_str(), p->prev[i],
				p->use[i], p->spoil[i], p->prod[i], p->need[i]);
	}

	return 1;
}

int State::calcStorePrice()
{
	unsigned int prand;
	//
	// TODO: this involves a bit of AI like whether a certain resource
	// is scarce or not, etc.  I'm just making it random for now. :P
	//
	// factors:
	// 		- basic supply and demand calculation
	// 		- how much of the resource the store has.
	// 		- how much of the resource the colony has
	// 		- some random factor
	//
	int i;
	for(i = 0; i < MAX_RESOURCE; i++)
	{
		int resPrice = RAND(&prand, g_storeMinPrice[i], g_storeMaxPrice[i]);
		resPrice = (int)(resPrice * 0.2f); // Max is awfully high...
		//
		// apply an overhead to the price because the store wants to
		// make a profit on any sales.
		//
		store->resBuy[i] = resPrice - (STORE_BUYSELL_DELTA/2);

		if(store->resBuy[i] < 0)
			store->resBuy[i] = 10;

		store->resSell[i] = store->resBuy[i] + STORE_BUYSELL_DELTA;
	}
	return 1;
}

