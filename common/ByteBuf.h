
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#ifndef BYTEBUF_H
#define BYTEBUF_H

#include <string>
#include <exception>

class ByteBufEmptyException: public std::exception {};

class ByteBuf
{
public:
	ByteBuf(bool bByteSwap = true);
	virtual ~ByteBuf();

	int reset();
	int consume();
	int reserve(unsigned int bytes);

	int clear();
	char* getBuf(unsigned int &size);
	unsigned int getSize() const { return size; }

	int bufIn(const char *buf, unsigned int nByte);
	ByteBuf& operator << (const int i);
	ByteBuf& operator << (const unsigned int i);
	ByteBuf& operator << (const bool b);
	ByteBuf& operator << (const float f);
	ByteBuf& operator << (const std::string& s);

	//!
	// careful consideration needs to be made when calling these
	// output methods.  Once all the output is read, make sure to call
	// 'consume()' to update the internal buffer markers.
	//
	// They all throw a ByteBufEmptyException if you try to read more
	// bytes than the buffer contains.
	//
	int bufOut(char *buf, unsigned int &nByte);
	ByteBuf& operator >> (int &i);
	ByteBuf& operator >> (unsigned int &i);
	ByteBuf& operator >> (bool &b);
	ByteBuf& operator >> (float &f);
	ByteBuf& operator >> (std::string& s);

protected:
	bool swap;
	unsigned int size;
	unsigned int max;
	char *buf;
	char *pBuf; //!< used as marker when reading from buffer

	//!
	// size method for intermediate reads.  A consume() should be
	// performed when enough reading has taken place.
	unsigned int _size() { return (unsigned int)(&buf[size] - pBuf); }

	int in32(const char *bytes);
	int in16(const char *bytes);
	int in8(const char *bytes);
	int out32(char *bytes);
	int out16(char *bytes);
	int out8(char *bytes);
};


#endif
