
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#ifndef OREGON_DEFS_H
#define OREGON_DEFS_H

#include "Resource.h"


#define OREGON_VERSION 0.001f
#define OREGON_TITLE "Oregon v0.001"

#define MAX_PLAYER 4
#define MAX_GAME_ROUNDS 12

#define MAX_VEL 0.1f
//!
// normal going price of a plot of land
#define PLOT_VALUE 100
//!
// initial amount of time to develop plots of land
#define DEVEL_TIME 15
//!
// clipping development times in seconds to keep game sane
#define MIN_DEVEL_TIME 5
#define MAX_DEVEL_TIME 25

//! seconds lost or gained per food unit above or below requirement
#define SECONDS_PER_FOOD 4.0f

//! quality of a plot is determined by how much of a resource it can produce
#define PLOT_RES_MAX 5

//! how many units can a plot produce at once
#define PRODUCTION_MAX 6
//! spread of random production values possible (absolute value)
#define PRODUCTION_DEVIATION 1

#define STORE_INITIAL_MONEY 1000
#define STORE_BUYSELL_DELTA 35
#define STORE_INITIAL_MULE_PRICE 50
#define STORE_INITIAL_MULE_COUNT 10
#define STORE_OUTFIT_TIME 2000

#define BOARD_WIDTH 64
#define BOARD_HEIGHT 26
#define PLOT_NX 7
#define PLOT_NY 5
#define STORE_X	3
#define STORE_Y	2
#define RIVER_X	3

#define SELECT 0
#define TRADE 1

//!
//  In percentage, how much food can spoil in one round.
//
#define MIN_FOOD_SPOIL 10
#define MAX_FOOD_SPOIL 20
#define FOOD_USAGE 10

//!
// enumeration of different store types available.
// used in the data2 of a MSG_CODE_STORE event
enum {
	STORE_NONE = 0,
	STORE_CRYSTITE,
	STORE_SMITHORE,
	STORE_ENERGY,
	STORE_FOOD,
	STORE_ASSAY,
	STORE_LAND,
	STORE_PUB,
	STORE_MULE,
	STORE_NUM
};

//!
// positions of store doors.
// L - left percentage along the x axis
// R - right pos
#define MALL_CRYS_L (8.0f / BOARD_WIDTH)
#define MALL_CRYS_R (11.0f / BOARD_WIDTH)
#define MALL_SMIT_L (24.0f / BOARD_WIDTH)
#define MALL_SMIT_R (27.0f / BOARD_WIDTH)
#define MALL_ENER_L (38.0f / BOARD_WIDTH)
#define MALL_ENER_R (41.0f / BOARD_WIDTH)
#define MALL_FOOD_L (52.0f / BOARD_WIDTH)
#define MALL_FOOD_R (55.0f / BOARD_WIDTH)
#define MALL_ASSA_L (5.0f / BOARD_WIDTH)
#define MALL_ASSA_R (8.0f / BOARD_WIDTH)
#define MALL_LAND_L (16.0f / BOARD_WIDTH)
#define MALL_LAND_R (18.0f / BOARD_WIDTH)
#define MALL_PUB_L (26.0f / BOARD_WIDTH)
#define MALL_PUB_R (27.0f / BOARD_WIDTH)
#define MALL_MULE_L (44.0f / BOARD_WIDTH)
#define MALL_MULE_R (47.0f / BOARD_WIDTH)
#define MALL_STORE_ENTRANCE (2.0f / BOARD_HEIGHT)
#define MALL_HALL_TOP (7.0f / BOARD_HEIGHT)
#define MALL_HALL_BOT (11.0f / BOARD_HEIGHT)
#define MALL_HALL_LEFT 0.0f
#define MALL_HALL_RIGHT ((float)(BOARD_WIDTH-1) / BOARD_WIDTH)
#define MALL_START_X (32.0f / BOARD_WIDTH)
#define MALL_START_Y (9.0f / BOARD_HEIGHT)

typedef enum {
    PHASE_IDLE=0, /* idle mode is only used by server */
    PHASE_TITLE, // character picking
    PHASE_GRANT, // land grant
    PHASE_DEVEL, // land development
    PHASE_PROD, // land production
    PHASE_AUCTION, // resource auction
    PHASE_STANDINGS, // player scores
    MAX_PHASE
} PhaseType;

#endif

