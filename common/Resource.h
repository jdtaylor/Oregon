
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#ifndef RESOURCE_H
#define RESOURCE_H

#include <string>

typedef enum
{
	RES_NONE=0,
	RES_FOOD,
	RES_ENERGY,
	RES_ORE,
	RES_OIL,
	MAX_RESOURCE
} ResourceType;


typedef struct _Resource
{
	ResourceType type;
	std::string name;
	unsigned int value;
	unsigned int rarity;
	float minDecay;
	float maxDecay;
	unsigned int outfitCost;
} Resource;

extern const Resource g_resType[MAX_RESOURCE];

extern const int g_storeInitial[MAX_RESOURCE];
extern const int g_storeInitialPrice[MAX_RESOURCE];
extern const int g_storeMinPrice[MAX_RESOURCE];
extern const int g_storeMaxPrice[MAX_RESOURCE];

//! effective maximums of a resource for a player. Note: the
//  player can obtain more, but it won't effect the ai.
extern const int g_playerResMax[MAX_RESOURCE];
extern const int g_playerResNeedMax[MAX_RESOURCE];

#endif

