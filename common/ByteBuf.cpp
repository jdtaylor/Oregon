
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include "ByteBuf.h"
#include "common.h"

using namespace std;


ByteBuf::ByteBuf(bool bByteSwap)
{
	swap = bByteSwap;
	size = 0;
	max = 0;
	buf = NULL;
	pBuf = NULL;
}

ByteBuf::~ByteBuf()
{
	if(buf != NULL) {
		FREE(buf);
	}
}

int ByteBuf::reset()
{
	pBuf = buf;
	return 1;
}

int ByteBuf::consume()
{
	unsigned int nBytes = (unsigned int)(pBuf - buf);
	size -= nBytes;
	if(size) {
		memmove(buf, pBuf, size);
		reset();
	}
	return 1;
}

int ByteBuf::reserve(unsigned int bytes)
{
	void *pVoid;

	if(!bytes)
		return 1;

	if(buf == NULL)
	{
		buf = (char*)MALLOC(sizeof(char) * bytes);
		if(buf == NULL) {
			ERROR << "failed to allocate " << bytes << " bytes" << endl;
			return -1;
		}
		max = bytes;
		size = 0;
		pBuf = buf;
		return 1;
	}

	while(size + bytes > max)
		max *= 2;

	pVoid = REALLOC(buf, max);
	if(pVoid == NULL) {
		ERROR << "failed to reallocate to " << max << " bytes" << endl;
		return -1;
	}
	buf = (char*)pVoid;
	pBuf = buf; // pBuf might not be valid anymore
	return 1;
}

int ByteBuf::clear()
{
	size = 0;
	return 1;
}

char* ByteBuf::getBuf(unsigned int &size)
{
	size = this->size;
	return buf;
}

int ByteBuf::bufIn(const char *buf, unsigned int nByte)
{
	if(reserve(nByte) < 0) {
		ERROR << "failed to reserve " << nByte << " bytes" << endl;
		return -1;
	}
	memcpy(&this->buf[size], buf, nByte);
	size += nByte;
	return 1;
}

ByteBuf& ByteBuf::operator << (const int i)
{
	in32((const char*)&i);
	return *this;
}

ByteBuf& ByteBuf::operator << (const unsigned int i)
{
	in32((const char*)&i);
	return *this;
}

ByteBuf& ByteBuf::operator << (const bool b)
{
	char c = b ? 1 : 0;
	in8(&c);
	return *this;
}

ByteBuf& ByteBuf::operator << (const float f)
{
	//
	// TODO: unimplemented... that's not good..
	//
	return *this;
}

ByteBuf& ByteBuf::operator << (const string& s)
{
	if(reserve(s.length() + 1) < 0) {
		ERROR << "failed to reserve " << s.length()+1 << " bytes" << endl;
		return *this;
	}
	char nul = '\0';
	bufIn(s.data(), s.length()); // TODO: multi-byte characters?
	in8(&nul);
	return *this;
}

int ByteBuf::bufOut(char *buf, unsigned int &nByte)
{
	if(nByte > _size()) {
		ERROR << nByte << " bytes isn't in byte buffer" << endl;
		throw ByteBufEmptyException();
	}
	memcpy(buf, pBuf, nByte);
	pBuf += nByte;
	return 1;
}

ByteBuf& ByteBuf::operator >> (int &i)
{
	out32((char*)&i);
	return *this;
}

ByteBuf& ByteBuf::operator >> (unsigned int &i)
{
	out32((char*)&i);
	return *this;
}

ByteBuf& ByteBuf::operator >> (bool &b)
{
	char c;
	out8(&c);
	b = c ? true : false;
	return *this;
}

ByteBuf& ByteBuf::operator >> (float &f)
{
	//
	// TODO: unimplemented
	//
	return *this;
}

ByteBuf& ByteBuf::operator >> (string& s)
{
	char c;
	if(_size() < 2) {
		ERROR << "not 2 bytes in buffer to retrieve" << endl;
		throw ByteBufEmptyException();
	}
	unsigned int nChar = 0;
	while(nChar < _size() && pBuf[nChar] != '\0')
		nChar++;
	s = pBuf;
	pBuf += nChar;
	out8(&c); // c should be '\0'
	return *this;
}

//
// Protected Members
//

int ByteBuf::in32(const char *bytes)
{
	if(reserve(4) < 0)
		return -1;

	if(swap)
	{
		buf[size+3] = bytes[0];
		buf[size+2] = bytes[1];
		buf[size+1] = bytes[2];
		buf[size+0] = bytes[3];
	}
	else
	{
		buf[size+0] = bytes[0];
		buf[size+1] = bytes[1];
		buf[size+2] = bytes[2];
		buf[size+3] = bytes[3];
	}
	size += 4;
	return 1;
}

int ByteBuf::in16(const char *bytes)
{
	if(reserve(2) < 0)
		return -1;

	if(swap)
	{
		buf[size+1] = bytes[0];
		buf[size+0] = bytes[1];
	}
	else
	{
		buf[size+0] = bytes[0];
		buf[size+1] = bytes[1];
	}
	size += 2;
	return 1;
}

int ByteBuf::in8(const char *bytes)
{
	if(reserve(1) < 0)
		return -1;

	buf[size] = bytes[0];
	size += 1;
	return 1;
}

int ByteBuf::out32(char *bytes)
{
	if(_size() < 4) {
		ERROR << "not 4 bytes in buffer to retrieve" << endl;
		throw ByteBufEmptyException();
	}
	if(swap)
	{
		bytes[0] = pBuf[3];
		bytes[1] = pBuf[2];
		bytes[2] = pBuf[1];
		bytes[3] = pBuf[0];
	}
	else
	{
		bytes[0] = pBuf[0];
		bytes[1] = pBuf[1];
		bytes[2] = pBuf[2];
		bytes[3] = pBuf[3];
	}
	pBuf += 4;
	return 1;
}

int ByteBuf::out16(char *bytes)
{
	if(_size() < 2) {
		ERROR << "not 2 bytes in buffer to retrieve" << endl;
		throw ByteBufEmptyException();
	}
	if(swap)
	{
		bytes[0] = pBuf[1];
		bytes[1] = pBuf[0];
	}
	else
	{
		bytes[0] = pBuf[0];
		bytes[1] = pBuf[1];
	}
	pBuf += 2;
	return 1;
}

int ByteBuf::out8(char *bytes)
{
	if(_size() < 1) {
		ERROR << "not 1 byte in buffer to retrieve" << endl;
		throw ByteBufEmptyException();
	}
	bytes[0] = pBuf[0];
	pBuf += 1;
	return 1;
}

