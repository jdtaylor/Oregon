
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#ifndef MESSAGE_H
#define MESSAGE_H

#include <climits>
#include "ByteBuf.h"


//! \brief Type field of the Message class.
//
// Defines what type of message it is, and therefore, which Message class
// is derived.
//
typedef enum 
{
	MSG_NULL = 0, //!< used for catching uninitializeed msgs
    MSG_EVENT,
    MSG_SYS, //!< nPlayers, gamestate, console msgs...
    MSG_STORE,
    MSG_PLAYER, //!< entire player data
    MSG_PLAYER_MSG,
    MSG_MOVE, //!< player position 
    MSG_MOVING, //!< 
    MSG_PLOT_INFO, //!< info about the entire landscape
    MSG_PLOT, //!< server sending client land plot info
    MSG_DEVEL,
    MSG_AUCTION,
    MSG_STANDINGS, //!< player standings/rank
    MAX_MSG
} MsgType;

extern const char g_msgTypeNames[MAX_MSG][32];

//!
// large unsigned int reserved for broadcasts to entire server.
//
typedef unsigned int playerId_t;
#define PLAYER_ID_RESERVE (32)
#define PLAYER_ID_MAX (UINT_MAX - PLAYER_ID_RESERVE + 1)
#define PLAYER_ID_NONE (PLAYER_ID_MAX)
#define PLAYER_ID_BROADCAST (PLAYER_ID_MAX+1)


/*! \brief Generic Message Class
 *
 * All Oregon messages are derived from this class and implement the
 * methods required by it.  Use this class in places where the actual type
 * of message isn't needed to be known.
 */
class Message
{
public:
	Message();
	virtual ~Message();

	//! \brief Verifies the message integrity.
	//
	//  Does things like bounds checking and validity of the message's
	//  member data.  It's mostly used on incoming messages because we
	//  don't trust foreign code on the internet.
	//
	virtual int verify();

	//! \brief Either packs or unpacks data for transmission.
	//
	//  Converts the message data fields into a consistent series of bytes
	//  which are suitable for internet transmission and standards.  Strict
	//  byte ordering and swapping is done here, and stored in the 'buf'
	//  parameter.
	//
	//  @param buf Buffer for reading or writing byte-specific data.
	//
	//  @param bPack bPack is true if data is going into 'buf', and false
	//  if data is coming out of 'buf' and filling the message with data.
	//
	virtual int pack(ByteBuf& buf, bool bPack);
	
	//! \brief Uses logging system to output contents of message
	virtual void dump();

	unsigned int flags;	//!< Contains bits of info about the message
	MsgType type;	//!< One of the MessageType enumerations
	playerId_t srcPlayer;	//!< player id of player that sent the msg
	playerId_t dstPlayer;	//!< player id of player to receive the msg
};

#endif
