
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include "common.h"
#include "Messages.h"
#include "oregon_defs.h"

using namespace std;


extern const char g_eventCodeNames[MAX_MSG_CODE][32] = {
	"NULL",
	"PLAYER_COLOR",
	"PLAYER_SELECT",
	"PLAYER_COUNTDOWN",
	"WAIT_STATUS",
	"GAME_START",
	"GAME_STOP",
	"MODE_CHANGE"
};

/*
 * Message
 */

MessageEvent::MessageEvent()
{
	type = MSG_EVENT;

	code = MSG_CODE_NULL;
	data1 = 0;
	data2 = 0;
}

int MessageEvent::verify()
{
	return 1;
}

int MessageEvent::pack(ByteBuf& buf, bool bPack)
{
	if(bPack)
	{
		Message::pack(buf, bPack);

		buf << (int)code;
		buf << data1;
		buf << data2;
	}
	else
	{
		int eventCode;

		try
		{
			buf >> eventCode;
			buf >> data1;
			buf >> data2;
		}
		catch(ByteBufEmptyException& e) {
			return -1;
		}

		if(eventCode < 0 || eventCode >= MAX_MSG_CODE) {
			ERROR << "Bad message code: " << eventCode;
			return -1;
		}
		eventCode = (MessageEventCode)eventCode;
	}
	return 1;
}

void MessageEvent::dump()
{
	Message::dump();

	DUMP << "code: \t" << g_eventCodeNames[code] << endl;
	DUMP << "data1: \t" << data1 << endl;
	DUMP << "data2: \t" << data2 << endl;
}

/*
 * Message
 */

MessageSys::MessageSys()
{
	type = MSG_SYS;

	version = OREGON_VERSION;
	round = 0;
	phase = 0;
	nPlayer = MAX_PLAYER;
	curPlayer = 0;
	countdown = 0;
	isWaiting = false;
}

int MessageSys::verify()
{
	return 1;
}

int MessageSys::pack(ByteBuf& buf, bool bPack)
{
	if(bPack)
	{
		Message::pack(buf, bPack);

		buf << version;
		buf << round;
		buf << phase;
		buf << nPlayer;
		buf << curPlayer;
		buf << countdown;
		buf << isWaiting;
	}
	else
	{
		try
		{
			buf >> version;
			buf >> round;
			buf >> phase;
			buf >> nPlayer;
			buf >> curPlayer;
			buf >> countdown;
			buf >> isWaiting;
		}
		catch(ByteBufEmptyException& e) {
			return -1;
		}
	}
	return 1;
}

void MessageSys::dump()
{
	Message::dump();

	DUMP << "version: \t" << version << endl;
	DUMP << "round: \t" << round << endl;
	DUMP << "phase: \t" << phase << endl;
	DUMP << "nPlayer: \t" << nPlayer << endl;
	DUMP << "curPlayer: \t" << curPlayer << endl;
	DUMP << "countdown: \t" << countdown << endl;
	DUMP << "isWaiting: \t" << isWaiting << endl;
}

/*
 * Message
 */

MessageStore::MessageStore()
{
	unsigned int i;

	type = MSG_STORE;

	plot = (PLOT_NX * PLOT_NY) / 2;
	money = MAX_INT; // I don't think the store has a limit on money

	for(i = 0; i < MAX_RESOURCE; i++)
	{
		res[i] = g_storeInitial[i];
		resBuy[i] = g_storeInitialPrice[i];
		resSell[i] = g_storeInitialPrice[i];
	}
	mulePrice = STORE_INITIAL_MULE_PRICE;
	muleCount = STORE_INITIAL_MULE_COUNT;
}

int MessageStore::verify()
{
	return 1;
}

int MessageStore::pack(ByteBuf& buf, bool bPack)
{
	unsigned int i;

	if(bPack)
	{
		Message::pack(buf, bPack);

		buf << plot;
		buf << money;
		for(i = 0; i < MAX_RESOURCE; i++)
		{
			buf << res[i];
			buf << resBuy[i];
			buf << resSell[i];
		}
		buf << mulePrice;
		buf << muleCount;
	}
	else
	{
		try
		{
			buf >> plot;
			buf >> money;
			for(i = 0; i < MAX_RESOURCE; i++)
			{
				buf >> res[i];
				buf >> resBuy[i];
				buf >> resSell[i];
			}
			buf >> mulePrice;
			buf >> muleCount;
		}
		catch(ByteBufEmptyException& e) {
			return -1;
		}
	}
	return 1;
}

void MessageStore::dump()
{
	unsigned int i;

	Message::dump();

	DUMP << "plot: \t" << plot << endl;
	DUMP << "money: \t" << money << endl;

	for(i = 0; i < MAX_RESOURCE; i++)
	{
		DUMP << "res[" << i << "]: \t" << res[i] << endl;
		DUMP << "resBuy[" << i << "]: \t" << resBuy[i] << endl;
		DUMP << "resSell[" << i << "]: \t" << resSell[i] << endl;
	}
	DUMP << "mulePrice: \t" << mulePrice << endl;
	DUMP << "muleCount: \t" << muleCount << endl;
}

/*
 * Message
 */

MessagePlayer::MessagePlayer()
{
	int i;

	type = MSG_PLAYER;

	playerId = 0; //player starts as id 0 until connect to server
	isReady = true;
	countdown = 0;
	inStore = false;
	haveMule = false;
	name = "player";
	server = "localhost";
	direction = 2; //face south
	character = 0; //player starts with boy character
	money = STORE_INITIAL_MONEY; //player starts with this much money
	nPlotOwned = 0;
	muleRes = RES_NONE; //resource outfitted on mule,
	for(i = 0; i < MAX_RESOURCE; i++)
	{
		prev[i] = 0;
		use[i] = 0;
		spoil[i] = 0;
		prod[i] = 0;
		need[i] = 0;
	}
	prev[1] = 6;  // Initialize Food
	prev[2] = 6;  // Initialize Energy
	pos_x = 0;
	pos_y = 0;
}

int MessagePlayer::verify()
{
	return 1;
}

int MessagePlayer::pack(ByteBuf& buf, bool bPack)
{
	unsigned int i;

	if(bPack)
	{
		Message::pack(buf, bPack);

		buf << playerId;
		buf << isReady;
		buf << countdown;
		buf << inStore;
		buf << haveMule;
		buf << name;
		buf << server;
		buf << character;
		buf << money;
		buf << nPlotOwned;
		buf << (int)muleRes;
		for(i = 0; i < MAX_RESOURCE; i++)
		{
			buf << prev[i];
			buf << use[i];
			buf << spoil[i];
			buf << prod[i];
			buf << need[i];
		}
		buf << pos_x;
		buf << pos_y;
	}
	else
	{
		try
		{
			int r;

			buf >> playerId;
			buf >> isReady;
			buf >> countdown;
			buf >> inStore;
			buf >> haveMule;
			buf >> name;
			buf >> server;
			buf >> character;
			buf >> money;
			buf >> nPlotOwned;
			buf >> r; //muleRes;
			for(i = 0; i < MAX_RESOURCE; i++)
			{
				buf >> prev[i];
				buf >> use[i];
				buf >> spoil[i];
				buf >> prod[i];
				buf >> need[i];
			}
			buf >> pos_x;
			buf >> pos_y;
			if(r < 0 || r >= MAX_RESOURCE) {
				ERROR << "Invalid muleRes" << r << endl;
				return -1;
			}
			muleRes = (ResourceType)r;
		}
		catch(ByteBufEmptyException& e) {
			return -1;
		}
	}
	return 1;
}

void MessagePlayer::dump()
{
	unsigned int i;

	Message::dump();

	DUMP << "playerId: \t" << playerId << endl;
	DUMP << "isReady: \t" << isReady << endl;
	DUMP << "countdown: \t" << countdown << endl;
	DUMP << "inStore: \t" << (inStore ? "true" : "false") << endl;
	DUMP << "haveMule: \t" << (haveMule ? "true" : "false") << endl;
	DUMP << "name: \t" << name.data() << endl;
	DUMP << "server: \t" << name.data() << endl;
	DUMP << "character: \t" << character << endl;
	DUMP << "money: \t" << money << endl;
	DUMP << "nPlotOwned: \t" << nPlotOwned << endl;
	DUMP << "muleRes: \t" << muleRes << endl;
	for(i = 0 ; i < MAX_RESOURCE ; i++)
	{
		DUMP << "prev[" << i << "]: \t" <<  prev[i] << endl;
		DUMP << "use[" << i << "]: \t" <<  use[i] << endl;
		DUMP << "spoil[" << i << "]: \t" <<  spoil[i] << endl;
		DUMP << "prod[" << i << "]: \t" <<  prod[i] << endl;
		DUMP << "need[" << i << "]: \t" <<  need[i] << endl;
	}
	DUMP << "pos_x: \t" << pos_x << endl;
	DUMP << "pos_y: \t" << pos_y << endl;
}

int MessagePlayer::resCur(int res)
{
	if(use[res] + spoil[res] > prev[res] + prod[res]) {
		ERROR << "Invalid resource count: " << res << endl;
		dump();
		return -1;
	}
	return (prev[res] + prod[res]) - (use[res] + spoil[res]);
}

float MessagePlayer::resMotive(int res)
{
	int cur = resCur(res);
	if(cur < 0)
		return -1;

	if(cur < need[res]) {
		//
		// how bad does the player need this resource.
		// returns [0.0f, 0.5f] depending on how bad it needs the res.
		//
		return 0.5f - (float)cur / (need[res] * 2.0f);
	}

	if(need[res] > g_playerResNeedMax[res]) {
		ERROR << "OOB need[" << g_resType[res].name.c_str() << "]:" << 
			need[res] << endl;
		dump();
		return -1;
	}
	//
	// TODO: wtf?  the motive should be _really_ low.. right? 
	//
	return (float)(cur - need[res]) / (g_playerResNeedMax[res] - need[res] + 1);
}

/*
 * Message
 */

MessagePlayerMsg::MessagePlayerMsg()
{
	type = MSG_PLAYER_MSG;
}

int MessagePlayerMsg::verify()
{
	return 1;
}

int MessagePlayerMsg::pack(ByteBuf& buf, bool bPack)
{
	if(bPack)
	{
		Message::pack(buf, bPack);
	}
	else
	{
		try
		{
		}
		catch(ByteBufEmptyException& e) {
			return -1;
		}
	}
	return 1;
}

void MessagePlayerMsg::dump()
{
}

/*
 * Message
 */

MessageMove::MessageMove()
{
	type = MSG_MOVE;
}

int MessageMove::verify()
{
	return 1;
}

int MessageMove::pack(ByteBuf& buf, bool bPack)
{
	if(bPack)
	{
		Message::pack(buf, bPack);
	}
	else
	{
		try
		{
		}
		catch(ByteBufEmptyException& e) {
			return -1;
		}
	}
	return 1;
}

void MessageMove::dump()
{
}

/*
 * Message
 */

MessageMoving::MessageMoving()
{
	type = MSG_MOVING;
}

int MessageMoving::verify()
{
	return 1;
}

int MessageMoving::pack(ByteBuf& buf, bool bPack)
{
	if(bPack)
	{
		Message::pack(buf, bPack);
	}
	else
	{
		try
		{
		}
		catch(ByteBufEmptyException& e) {
			return -1;
		}
	}
	return 1;
}

void MessageMoving::dump()
{
}

/*
 * Message
 */

MessagePlotInfo::MessagePlotInfo()
{
	type = MSG_PLOT_INFO;

	nPlotX = PLOT_NX;
	nPlotY = PLOT_NY;
	nPlot = nPlotX * nPlotY;
}

int MessagePlotInfo::verify()
{
	return 1;
}

int MessagePlotInfo::pack(ByteBuf& buf, bool bPack)
{
	if(bPack)
	{
		Message::pack(buf, bPack);

		buf << nPlot;
		buf << nPlotX;
		buf << nPlotY;
	}
	else
	{
		try
		{
			buf >> nPlot;
			buf >> nPlotX;
			buf >> nPlotY;
		}
		catch(ByteBufEmptyException& e) {
			return -1;
		}
	}
	return 1;
}

void MessagePlotInfo::dump()
{
	DUMP << "nPlot:" << nPlot << endl;
	DUMP << "nPlotX:" << nPlotX << endl;
	DUMP << "nPlotY:" << nPlotY << endl;
}

/*
 * Message
 */

MessagePlot::MessagePlot()
{
	int i;

	type = MSG_PLOT;

	plotId = 0;
	owner = PLAYER_ID_NONE;
	terrain = 0;
	mountain = false;
	resOutfit = RES_NONE;
	resProduced = 0;
	for(i = 0; i < MAX_RESOURCE; i++)
		resPotential[i] = 0;
	speedResistance = 0.0f;
}

int MessagePlot::verify()
{
	return 1;
}

int MessagePlot::pack(ByteBuf& buf, bool bPack)
{
	int i;

	if(bPack)
	{
		Message::pack(buf, bPack);

		buf << plotId;
		buf << owner;
		buf << terrain;
		buf << mountain;
		buf << (int)resOutfit;
		buf << resProduced;
		for(i = 0; i < MAX_RESOURCE; i++)
			buf << resPotential[i];
		buf << speedResistance;
	}
	else
	{
		try
		{
			int r;

			buf >> plotId;
			buf >> owner;
			buf >> terrain;
			buf >> mountain;
			buf >> r;
			buf >> resProduced;
			for(i = 0; i < MAX_RESOURCE; i++)
				buf >> resPotential[i];
			buf >> speedResistance;
			if(r < 0 || r >= MAX_RESOURCE) {
				ERROR << "Invalid resource: " << r << endl;
				return -1;
			}
			resOutfit = (ResourceType)r;
		}
		catch(ByteBufEmptyException& e) {
			return -1;
		}
	}
	return 1;
}

void MessagePlot::dump()
{
}

/*
 * Message
 */

MessageDevel::MessageDevel()
{
	type = MSG_DEVEL;
}

int MessageDevel::verify()
{
	return 1;
}

int MessageDevel::pack(ByteBuf& buf, bool bPack)
{
	if(bPack)
	{
		Message::pack(buf, bPack);
	}
	else
	{
		try
		{
		}
		catch(ByteBufEmptyException& e) {
			return -1;
		}
	}
	return 1;
}

void MessageDevel::dump()
{
}

/*
 * Message
 */

MessageAuction::MessageAuction()
{
	type = MSG_AUCTION;

	res = RES_NONE;
	mode = AUCTION_MODE_NONE;

	int i;
	for(i = 0; i < MAX_PLAYER; i++)
		playerBuying[i] = true;
}

int MessageAuction::verify()
{
	return 1;
}

int MessageAuction::pack(ByteBuf& buf, bool bPack)
{
	if(bPack)
	{
		Message::pack(buf, bPack);
	}
	else
	{
		try
		{
		}
		catch(ByteBufEmptyException& e) {
			return -1;
		}
	}
	return 1;
}

void MessageAuction::dump()
{
}

/*
 * Message
 */

MessageStandings::MessageStandings()
{
	type = MSG_STANDINGS;
}

int MessageStandings::verify()
{
	return 1;
}

int MessageStandings::pack(ByteBuf& buf, bool bPack)
{
	if(bPack)
	{
		Message::pack(buf, bPack);
	}
	else
	{
		try
		{
		}
		catch(ByteBufEmptyException& e) {
			return -1;
		}
	}
	return 1;
}

void MessageStandings::dump()
{
}

