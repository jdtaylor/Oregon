
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#ifndef REMOTE_HOST_H
#define REMOTE_HOST_H

#include <string>
#include <vector>
#include <exception>
#include "SDL/SDL_net.h"
#include "Message.h"
#include "ByteBuf.h"


class RemoteCloseException: public std::exception {};

class RemoteHost
{
public:
	RemoteHost();
	RemoteHost(IPaddress& addr, TCPsocket& sock);
	~RemoteHost();

	unsigned int m_hostId;

	int connect(const char *host, short port);
	int set(IPaddress& addr, TCPsocket& sock);
	int close();

	bool dataWaiting();
	bool ready();
	int send(Message& msg);
	std::vector<Message*> get();

	int setAdd(SDLNet_SocketSet set);
	int setDel(SDLNet_SocketSet set);

	RemoteHost& operator=(const RemoteHost& host);
	bool operator==(const RemoteHost& r) const
	{
		return r.m_hostId == m_hostId;
	}
protected:
	IPaddress m_addr;
	TCPsocket m_sock;
	ByteBuf m_txBuf; //!< transmission buffer
	ByteBuf m_rxBuf; //!< reception buffer

	//!
	// bit of a hack to provide temporary read buffer for
	// all instances of this class.  It remains allocated
	// for the life of the program.
	//
	// WARNING: replace if threads are used
	//
	static char *m_buf;
	static unsigned int m_bufSize;
};


#endif
