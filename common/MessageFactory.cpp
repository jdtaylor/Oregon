
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include "common.h"
#include "MessageFactory.h"

using namespace std;

Message* MessageFactory::newMessage(MsgType type)
{
	switch(type)
	{
	case MSG_EVENT:
		return new MessageEvent();
	case MSG_SYS:
		return new MessageSys();
	case MSG_STORE:
		return new MessageStore();
	case MSG_PLAYER:
		return new MessagePlayer();
	case MSG_PLAYER_MSG:
		return new MessagePlayerMsg();
	case MSG_MOVE:
		return new MessageMove();
	case MSG_MOVING:
		 return new MessageMoving();
	case MSG_PLOT_INFO:
		return new MessagePlotInfo();
	case MSG_PLOT:
		return new MessagePlot();
	case MSG_DEVEL:
		return new MessageDevel();
	case MSG_AUCTION:
		return new MessageAuction();
	case MSG_STANDINGS:
		return new MessageStandings();
	default:
		ERROR << "Invalid message type: " << type << endl;
		return NULL;
	}
	return NULL;
}

Message* newMsg(MsgType type)
{
	return MessageFactory::Instance().newMessage(type);
}

