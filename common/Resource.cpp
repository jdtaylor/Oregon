/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include "Resource.h"

const Resource g_resType[MAX_RESOURCE] = {
	{RES_NONE,		"nothing",	0,	0,	0.0f,	0.0f,	0},
	{RES_FOOD,		"Food",		10,	10,	40.0f,	80.0f,	25},
	{RES_ENERGY,	"Energy",	10,	10,	20.0f,	40.0f,	50},
	{RES_ORE,		"Ore",		40,	40,	0.0f,	0.0f,	75},
	{RES_OIL,		"Oil",		90,	90,	0.0f,	0.0f,	75}
};

const int g_storeInitial[MAX_RESOURCE] = { 0, 10, 10, 10, 10 };
const int g_storeInitialPrice[MAX_RESOURCE] = { 0, 10, 10, 10, 30 };
const int g_storeMinPrice[MAX_RESOURCE] = { 0, 5, 10, 48, 48 };
const int g_storeMaxPrice[MAX_RESOURCE] = { 0, 500, 500, 500, 500 };

const int g_playerResMax[MAX_RESOURCE] = { 0, 50, 50, 50, 50 };
const int g_playerResNeedMax[MAX_RESOURCE] = {
	0,
	7, //! food is based on what round it is (round / 4 * 3)
	50,
	50,
	50
};

