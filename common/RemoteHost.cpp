
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include "SDL/SDL_net.h"
#include "RemoteHost.h"
#include "common.h"
#include "MessageFactory.h"

using namespace std;

unsigned int RemoteHost::m_bufSize = 1024;
char* RemoteHost::m_buf = new char[RemoteHost::m_bufSize];


RemoteHost::RemoteHost()
{
	m_sock = NULL;
}

RemoteHost::RemoteHost(IPaddress& addr, TCPsocket& sock)
{
	m_addr.host = addr.host;
	m_addr.port = addr.port;
	m_sock = sock;
}

RemoteHost::~RemoteHost()
{
	close();
}

int RemoteHost::connect(const char *host, short port)
{
	//
	// convert from host/port to IP address
	//
	if(SDLNet_ResolveHost(&m_addr, host, port) != 0) {
		WARN << "Couldn't resolve hostname: " << host << endl;
		return 0;
	}
	//
	// try to connect to server
	//
	m_sock = SDLNet_TCP_Open(&m_addr);
	if(m_sock == NULL) {
		WARN << "Failed to connect to hostname: " << host << endl;
		return 0;
	}
	return 1;
}

int RemoteHost::set(IPaddress& addr, TCPsocket& sock)
{
	m_addr.host = addr.host;
	m_addr.port = addr.port;
	m_sock = sock;
	return 1;
}

int RemoteHost::close()
{
	if(m_sock != NULL)
		SDLNet_TCP_Close(m_sock);
	return 1;
}

bool RemoteHost::dataWaiting()
{
	return m_rxBuf.getSize() ? true : false;
}

bool RemoteHost::ready()
{
	if(m_sock == NULL)
		return false;
	return SDLNet_SocketReady(m_sock) ? true : false;
}

int RemoteHost::send(Message& msg)
{
	char *buf;
	unsigned int size;

	if(m_sock == NULL) {
		ERROR << "Socket not open" << endl;
		return -1;
	}
	msg.pack(m_txBuf, true); // pack message contents into byte buffer
	buf = m_txBuf.getBuf(size);

	if(SDLNet_TCP_Send(m_sock, buf, size) < (int)size) {
		ERROR << "TCP_Send didn't send all data: " << SDLNet_GetError() << endl;
		SDLNet_TCP_Close(m_sock); // most likely fatal error
		return -1;
	}
	m_txBuf.clear();

	DUMP << "<-- " << g_msgTypeNames[msg.type] << endl;
	return 1;
}

vector<Message*> RemoteHost::get()
{
	int nByte;
	Message header;
	vector<Message*> msgs;

	nByte = SDLNet_TCP_Recv(m_sock, m_buf, m_bufSize);
	if(nByte <= 0) {
		ERROR << "Failed to read data. closed connection" << endl;
		throw RemoteCloseException();
		return msgs;
	}
	if(nByte)
		m_rxBuf.bufIn((const char*)m_buf, nByte);

	while(42)
	{
		if(m_rxBuf.getSize() == 0)
			break; // no incoming messages

		m_rxBuf.reset();

		if(header.pack(m_rxBuf, false) <= 0) {
			m_rxBuf.reset();
			break;
		}

		Message* msg = newMsg(header.type);
		if(msg == NULL) {
			ERROR << "Failed to allocate msg of type " << header.type << endl;
			break;
		}
		*msg = header;

		if(msg->pack(m_rxBuf, false) <= 0) {
			m_rxBuf.reset();
			break;
		}
		 // successful read of Message; consume bytes in buffer
		m_rxBuf.consume();

		DUMP << "--> " << g_msgTypeNames[msg->type] << endl;

		msgs.push_back(msg);
	}
	return msgs;
}

int RemoteHost::setAdd(SDLNet_SocketSet set)
{
	if(m_sock == NULL)
		return 0;
	SDLNet_TCP_AddSocket(set, m_sock);
	return 1;
}

int RemoteHost::setDel(SDLNet_SocketSet set)
{
	if(m_sock == NULL)
		return 0;
	SDLNet_TCP_DelSocket(set, m_sock);
	return 1;
}

RemoteHost& RemoteHost::operator=(const RemoteHost& host)
{
	// cheap copies by value
	m_addr.host = host.m_addr.host;
	m_addr.port = host.m_addr.port;
	m_sock = host.m_sock;
	return *this;
}

