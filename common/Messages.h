
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#ifndef MESSAGES_H
#define MESSAGES_H

#include <string>
#include <vector>
#include "oregon_defs.h"
#include "Message.h"


//! \brief The 'code' field of the MessageEvent class.
typedef enum
{
    MSG_CODE_NULL = 0, //!< should _never_ be sent. an error if it is
	MSG_CODE_PLAYER_COLOR,
    MSG_CODE_PLAYER_SELECT, //!< data1 == playerId
    MSG_CODE_PLAYER_COUNTDOWN, //!< data1 == playerId, data2 == countdown
    MSG_CODE_WAIT_STATUS, //!< data1 == playerId, data2 = waiting ? 1 : 0
    MSG_CODE_GAME_START,
    MSG_CODE_GAME_STOP,
    MSG_CODE_PHASE_CHANGE, //!< data1 is the mode to change to
    MAX_MSG_CODE
} MessageEventCode;

extern const char g_eventCodeNames[MAX_MSG_CODE][32];

/*! \brief Contains small messages that only have a few parameters
 *
 *   Sinmple event message.  Contains a 'code' field which determines what
 *   type of event the message is, and two generic 'data' parameters which
 *   can be used for whatever purpose.
 */
class MessageEvent : public Message
{
public:
	MessageEvent();
	int verify();
	int pack(ByteBuf& buf, bool bPack);
	void dump();

	MessageEventCode code; //!< one of the code enumerations below
	int data1;
	int data2;
};


/*! \brief System message used for global-specific game information.
 */
class MessageSys : public Message
{
public:
	MessageSys();
	int verify();
	int pack(ByteBuf& buf, bool bPack);
	void dump();

	float version;	//!< communicate what version is running remotely
	int round;		//!< what round/month is it?
	int phase;		//!< which phase of the round is it.
	unsigned int nPlayer;	//!< number of players in the game
	playerId_t curPlayer;	//!< current server-side player id
	int countdown;	//!< generic countdown. (shouldn't this be an event?)
	bool isWaiting;	//!< whether server is waiting on a player or not
};

/*! \brief
 */
class MessageStore : public Message
{
public:
	MessageStore();
	int verify();
	int pack(ByteBuf& buf, bool bPack);
	void dump();

	int plot; // reserved for store location
	int money; // uuhm.. I think this should be MAX_INT
	int res[MAX_RESOURCE]; // amount of resource the store has
	int resBuy[MAX_RESOURCE]; // price store is willing to buy at
	int resSell[MAX_RESOURCE]; // price store is willing to sell at
	int mulePrice; // price of each mule
	int muleCount; // number of mules available to buy from store.
};


/*! \brief
 */
class MessagePlayer : public Message
{
public:
	MessagePlayer();
	int verify();
	int pack(ByteBuf& buf, bool bPack);
	void dump();

	playerId_t playerId;
	bool isReady;
	unsigned int countdown;
	bool inStore;
	bool haveMule;
	std::string name;
	std::string server;
	int character;
	unsigned int money;
	int nPlotOwned;
	ResourceType muleRes;
	int prev[MAX_RESOURCE];
	int use[MAX_RESOURCE];
	int spoil[MAX_RESOURCE];
	int prod[MAX_RESOURCE];
	int need[MAX_RESOURCE];
	int pos_x;
	int pos_y;
	int direction; //0=N, 1=E, 2=S, 3=W

	//!
	// \brief Returns current amount of resource the player has.
	//
	int resCur(int res);
	//!
	// \brief Returns motivation of player to seek 'res' [0.0, 1.0]
	//
	float resMotive(int res);
};

/*! \brief
 */
class MessagePlayerMsg : public Message
{
public:
	MessagePlayerMsg();
	int verify();
	int pack(ByteBuf& buf, bool bPack);
	void dump();

	playerId_t srcId;
	std::string msg;
};

/*! \brief
 */
class MessageMove : public Message
{
public:
	MessageMove();
	int verify();
	int pack(ByteBuf& buf, bool bPack);
	void dump();

	int id;
	//moveable_t move;
};

/*! \brief
 */
class MessageMoving : public Message
{
public:
	MessageMoving();
	int verify();
	int pack(ByteBuf& buf, bool bPack);
	void dump();

	int id;
	int event;
	int magnitude;
	float longitude;
	float latitude;
};

/*! \brief
 */
class MessagePlotInfo : public Message
{
public:
	MessagePlotInfo();
	int verify();
	int pack(ByteBuf& buf, bool bPack);
	void dump();

	int nPlot;
	int nPlotX;
	int nPlotY;
};

enum {
	TERRAIN_DIRT=0,
	TERRAIN_GRASS,
	TERRAIN_STORE,
	TERRAIN_RIVER,
	MAX_TERRAIN
};

/*! \brief
 */
class MessagePlot : public Message
{
public:
	MessagePlot();
	int verify();
	int pack(ByteBuf& buf, bool bPack);
	void dump();

	int plotId;
	playerId_t owner;
	int terrain;
	bool mountain;
	ResourceType resOutfit;
	int resProduced;
	int resPotential[MAX_RESOURCE];
	float speedResistance;
};

/*! \brief
 */
class MessageDevel : public Message
{
public:
	MessageDevel();
	int verify();
	int pack(ByteBuf& buf, bool bPack);
	void dump();

	int timeo;
	int outfitting[MAX_RESOURCE];
};

typedef enum _auctionMode
{
	AUCTION_MODE_NONE=0,
	AUCTION_MODE_STATUS,
	AUCTION_MODE_ROLE,
	AUCTION_MODE_TRADE,
	MAX_AUCTION_MODE
} AuctionMode;


/*! \brief
 */
class MessageAuction : public Message
{
public:
	MessageAuction();
	int verify();
	int pack(ByteBuf& buf, bool bPack);
	void dump();

	int res;
	int mode;
	int timeout; //!< Time left for current auction mode.
	bool playerBuying[MAX_PLAYER];
	int playerBid[MAX_PLAYER];
};

/*! \brief
 */
class MessageStandings : public Message
{
public:
	MessageStandings();
	int verify();
	int pack(ByteBuf& buf, bool bPack);
	void dump();

	std::vector<playerId_t> rank;
};

#endif

