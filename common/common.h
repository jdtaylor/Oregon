
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#ifndef COMMON_H
#define COMMON_H

#include <stdlib.h>
#include "mutil/mmath.h"
#include "mutil/log.h"

using namespace mutil;

#ifndef FALSE
enum { FALSE=0, TRUE=1 };
#endif

#ifndef NULL
#define NULL 0
#endif

#ifndef ASSERT
#include <assert.h>
#define ASSERT(test) assert(test)
#endif

/*!
 * Microsoft doesn't like these functions for some reason.
 */
#ifdef WIN32
	#define snprintf _snprintf
	#define vsnprintf _vsnprintf
#endif

#define MALLOC(s) malloc(s)
#define FREE(b) free(b)
#define REALLOC(b,s) realloc(b,s)
#define CALLOC(num,s) calloc(num,s)

#endif

