#!/bin/sh
set +x
`make clean`
rm -f oregon_client
rm -f oregon_server
rm -f CMakeCache.txt
rm -f cmake_install.cmake
rm -f client/ai/*.[oa]
rm -f client/sdl/*.[oa]
rm -f server/basic/*.[oa]
rm -rf CMakeFiles
rm -rf mutil/CMakeFiles
rm -rf mutil/cmake_install.cmake
rm -rf common/CMakeFiles
rm -rf common/cmake_install.cmake
rm -rf client/CMakeFiles
rm -rf client/cmake_install.cmake
rm -rf server/CMakeFiles
rm -rf server/cmake_install.cmake
rm -rf client/ai/CMakeFiles
rm -rf client/ai/cmake_install.cmake
rm -rf client/sdl/CMakeFiles
rm -rf client/sdl/cmake_install.cmake
rm -rf server/basic/CMakeFiles
rm -rf server/basic/cmake_install.cmake

