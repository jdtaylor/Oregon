
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#ifndef CLIENT_H
#define CLIENT_H

#include "IClient.h"
#include "State.h"
#include "ClientPhase.h"
#include "ClientTransit.h"
#include "oregon_defs.h"

/*! \brief Enumerates the different front-ends that the client can take.
 */
typedef enum _ClientFrontend
{
	CLIENT_FRONTEND_SDL=0,
	CLIENT_FRONTEND_AI,
	MAX_CLIENT_FRONTEND
} ClientFrontend;


/*! \brief Implements the client interface.
 */
class Client : public IClient
{
public:
	Client(ClientFrontend fe);
	virtual ~Client();
	int setServer(const char *host, short port);
	int execute();
	int dispatch(Message& msg);
	State& getState() { return m_state; }

protected:
	int OnMsgEvent(MessageEvent& event);
	int OnMsgPlayer(MessagePlayer& player);

	State m_state;
	ClientTransit m_server; // handles message passing to/from the server
	ClientPhase *m_phase[MAX_PHASE];

	int Server_start();
	int Server_advancePhase();
	int Server_OnMsgEvent(MessageEvent& event);
	int Server_OnPlayerSelect(MessageEvent& event);

private:
	Client();
	Client& operator=(const Client&);
};

#endif

