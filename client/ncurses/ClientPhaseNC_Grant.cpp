
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2007  James Douglas Taylor
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: "James D. Taylor" <james.d.taylor@gmail.com>
 */

#include <ncurses.h>
#include "ClientPhaseNC_Grant.h"


ClientPhaseNC_Grant::ClientPhaseNC_Grant() : ClientPhaseNC()
{
}

ClientPhaseNC_Grant::~ClientPhaseNC_Grant()
{
	destroy();
}

int ClientPhaseNC_Grant::execute()
{
	return ClientPhaseNC::execute();
}

int ClientPhaseNC_Grant::create()
{
	if(ClientPhaseNC::create() < 0)
		return -1;
	if(m_pState->sys == NULL) {
		LOG_MSG(LOG_LOW,"ClientPhaseNC_Grant::create - cargoSys is NULL");
		destroy();
		return -1;
	}
	makeBoardBG();

	//strncat(status, "[Grant]", sizeof(status) - strlen(status));

//	if(getState()->playerInfo->nPlayer <= 0) {
//		LOG_MSG(LOG_LOW,"ClientPhaseNC_Grant::create - err, no players");
//		destroy();
//		return -1;
//	}
	return 1;
}

int ClientPhaseNC_Grant::destroy()
{
	ClientPhaseNC::destroy();
	return 1;
}

int ClientPhaseNC_Grant::show()
{
	if(ClientPhaseNC::show() < 0) 
		return -1;
	doRefresh();
	return 1;
}

int ClientPhaseNC_Grant::hide()
{
	ClientPhaseNC::hide();
	return 1;
}

int ClientPhaseNC_Grant::doFrame(int tic)
{
	if(!m_bRefresh)
		return 1;
	printBoard();
	return 1;
}

int ClientPhaseNC_Grant::OnCargoEvent(cargoMoleEvent *evnt)
{
	int rtrn = ClientPhaseNC::OnCargoEvent(evnt);

	if(evnt->code == CODE_MOLE_PLOT_OFFER) {
		doRefresh();
	}
	return rtrn;
}

int ClientPhaseNC_Grant::OnCargoPlot(cargoMolePlot *pCargo)
{
	ClientPhaseNC::OnCargoPlot(pCargo);
	doRefresh();
	return 1;
}



