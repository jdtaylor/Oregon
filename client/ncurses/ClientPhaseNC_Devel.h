
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2007  James Douglas Taylor
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: "James D. Taylor" <james.d.taylor@gmail.com>
 */

#ifndef CLIENT_PHASE_NC_DEVEL_H
#define CLIENT_PHASE_NC_DEVEL_H


#include "ClientPhaseNC.h"


class ClientPhaseNC_Devel : public ClientPhaseNC
{
public:
    ClientPhaseNC_Devel();
    ~ClientPhaseNC_Devel();
	int execute();
    int create();
    int destroy();
    int show();
    int hide();
    int doFrame(int frame);
protected:
	int printStore();
	int printOutback();
	int printCountdown();

	int OnChar(int c);
	int OnCargoEvent(cargoMoleEvent *evnt);
	int OnCargoPlot(cargoMolePlot *pCargo);
	int OnCargoDevel(cargoMoleDevel *pCargo);
	int OnCargoMove(cargoMoleMove *pCargo);
private:
	int execPlayerSelect(int playerId);
	int execMoveDir(int playerId, int dir);

	int getOutbackPos(vect3f vPos, int &x, int &y);
	int getStorePos(vect3f vPos, int &x, int &y);

	int OnStoreAction(cargoMoleEvent *evnt);
	long storeOutfitTime[STORE_NUM]; // begin time of an outfitting

	int posPrevX;
	int posPrevY;
	int posCurX;
	int posCurY;
};

#endif

