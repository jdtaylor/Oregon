
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2007  James Douglas Taylor
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: "James D. Taylor" <james.d.taylor@gmail.com>
 */


#include <ncurses.h>
#include "mole_state.h"
#include "ClientPhaseNC_Devel.h"


ClientPhaseNC_Devel::ClientPhaseNC_Devel() : ClientPhaseNC()
{
	posPrevX = -1;
	posPrevY = -1;
	posCurX = -1;
	posCurY = -1;
}

ClientPhaseNC_Devel::~ClientPhaseNC_Devel()
{
	destroy();
}

int ClientPhaseNC_Devel::execute()
{
	return ClientPhaseNC::execute();
}

int ClientPhaseNC_Devel::create()
{
	int playerId;
	char tmp[BOARD_WIDTH] = "";
	cargoMolePlayer *pPlayer;

	memset(storeOutfitTime, 0, sizeof(long) * STORE_NUM);

	m_statusTime = timerCurrent();
	strncpy(m_strStatus, "[Devel]", sizeof(m_strStatus));

	playerId = m_pState->sys->curPlayer;
	if(playerId >= 0) {
		pPlayer = statePlayerGet(m_pState, playerId);
		if(pPlayer != NULL) {
			snprintf(tmp, sizeof(tmp), " %s's turn.", pPlayer->name);
			strncat(m_strStatus, tmp, sizeof(m_strStatus)-strlen(m_strStatus));
		}
	}

	doRefresh(5050);

	if(ClientPhaseNC::create() < 0)
		return -1;
	return 1;
}

int ClientPhaseNC_Devel::destroy()
{
	ClientPhaseNC::destroy();
	return 1;
}

int ClientPhaseNC_Devel::show()
{
	if(ClientPhaseNC::show() < 0)
		return -1;
	doRefresh();
	return 1;
}

int ClientPhaseNC_Devel::hide()
{
	ClientPhaseNC::hide();
	return 1;
}

int ClientPhaseNC_Devel::doFrame(int tic)
{
	moveable_t moveable;

	if(!m_bRefresh)
		return 1;

	playerPosGet(TODO_PLAYER, &moveable);
	int plotId = statePosToPlot(m_pState, moveable.vPos);
	
	if((int)m_pState->sys->curPlayer == playerIdGet(TODO_PLAYER))
	{
		if(plotId == (int)m_pState->store->plot)
			printStore();
		else
			printOutback();
	}
	else
		printOutback();

	printCountdown();

	return ClientPhaseNC::doFrame(tic);
}


int ClientPhaseNC_Devel::getOutbackPos(vect3f vPos, int &x, int &y)
{
	x = (int)(vPos[0] * BOARD_WIDTH); //m_width);
	y = (int)(vPos[1] * BOARD_HEIGHT); //m_height);
	return 1;
}

int ClientPhaseNC_Devel::getStorePos(vect3f vPos, int &x, int &y)
{
	x = (int)(vPos[0] * BOARD_WIDTH); //m_width);
	y = (int)(vPos[1] * (BOARD_HEIGHT - 4)) + 1; //m_height);
	return 1;
}

int ClientPhaseNC_Devel::printStore()
{
	int i;
	int x, y;
	cargoMolePlayer *pPlayer;
	moveable_t moveable;
	vect3f vStorePos;

	color_set(1, NULL);

	i = 0;
#define S(line) move(i++,0); addnstr(line, BOARD_WIDTH)
// 1..64
S("                                                                ");
S("    +----------+   +----------+   +---------+   +---------+     ");
S("    | Crystite |   | Smithore |   | Energy  |   |  Food   |     ");
S("    |          |   |          |   |         |   |         |     ");
S("    +--+   +---+   +---+   +--+   +--+   +--+   +--+   +--+     ");
S("       |   |           |   |         |   |         |   |        ");
S("[======#   #===========#   #=========#   #=========#   #=====]  ");
S("                                                                ");
S("                                                                ");
S("                                                                ");
S("                                                                ");
S("[===#---#======#--#======] [====+---------=]   [=----------+=]  ");
S("    |   |      |  |      | |    |          ]   [           |    ");
S("  +-+   +-+  +-+  +-+  +-} {-+  |                          |    ");
S("  |       |  |      |  |     |  |                          |    ");
S("  |       |  |      |  |     |  |                          |    ");
S("  |       |  |      |  | Pub |  |                          |    ");
S("  |       |  | Land |  +-----+  |                          |    ");
S("  | Assay |  +------+           |                          |    ");
S("  +-------+                     |                          |    ");
S("                                |        M U L E S         |    ");
S("                                +--------------------------+    ");
S("                                                                ");
	while(i < BOARD_HEIGHT) {
		S("                                                                ");
	}
	//
	// draw player
	pPlayer = playerGet(TODO_PLAYER);
	if(pPlayer == NULL)
		return 0;

	playerPosGet(TODO_PLAYER, &moveable);
	if(statePosToStorePos(m_pState, moveable.vPos, vStorePos) <= 0)
		return 0;
	getStorePos(vStorePos, x, y);

	if(x != posCurX || y != posCurY) {
		posPrevX = posCurX;
		posPrevY = posCurY;
		posCurX = x;
		posCurY = y;
	}

	if(pPlayer->flags & MOLE_PLAYER_OUTFIT)
	{
		switch(statePosToStore(m_pState, moveable.vPos))
		{
			case STORE_CRYSTITE:
				move(y-1, x);
				addnstr("C", 1);
				break;
			case STORE_SMITHORE:
				break;
			case STORE_ENERGY:
				break;
			case STORE_FOOD:
				break;
			case STORE_ASSAY:
				break;
			case STORE_LAND:
				break;
			case STORE_PUB:
				break;
			case STORE_MULE:
				break;
			case -1:
				// doesn't match any stores
				break;
		}
	}
	else
	{
		// draw mule
		if(pPlayer->muleRes >= 0 && posPrevX >= 0 && posPrevY >= 0) {
			move(posPrevY, posPrevX);
			addch(resToChar(pPlayer->muleRes));
		}

//		VLOG_MSG(LOG_DUMP,"POS: %d,%d", x, y);
		move(y, x);
		addnstr("@", 1);
	}
	return 1;
}

int ClientPhaseNC_Devel::printOutback()
{
	int x, y;
	cargoMolePlayer *pPlayer;
	moveable_t moveable;

	printBoard();
	//
	// draw player
	pPlayer = playerGet(TODO_PLAYER);
	if(pPlayer == NULL)
		return 0;

	playerPosGet(TODO_PLAYER, &moveable);
	getOutbackPos(moveable.vPos, x, y);

	if(x != posCurX || y != posCurY) {
		posPrevX = posCurX;
		posPrevY = posCurY;
		posCurX = x;
		posCurY = y;
	}
	// draw mule
	if(pPlayer->muleRes >= 0 && posPrevX >= 0 && posPrevY >= 0) {
		move(posPrevY, posPrevX);
		addch(resToChar(pPlayer->muleRes));
	}
	//VLOG_MSG(LOG_DUMP,"POS: %d,%d", x, y);
	move(y, x);
	addnstr("@", 1);
	return 1;
}

int ClientPhaseNC_Devel::printCountdown()
{
	int i;
	int y;
	cargoMolePlayer *pPlayer;

	if((pPlayer = playerGet(m_pState->sys->curPlayer)) == NULL)
		return 0;
	
	y = pPlayer->countdown;
	y = (BOARD_HEIGHT-1) -
		(int)((float)y / MAX_MOLE_DEVEL_TIME * BOARD_HEIGHT * 0.6f);

	for(i = 0; i < y; i++) {
		move(i, BOARD_WIDTH);
		addnstr(" ", 1);
	}
	for(i = y; i <= BOARD_HEIGHT; i++) {
		move(i, BOARD_WIDTH);
		addnstr("#", 1);
	}
	return 1;
}

int ClientPhaseNC_Devel::OnCargoEvent(cargoMoleEvent *evnt)
{
	int rtrn = ClientPhaseNC::OnCargoEvent(evnt);

	switch(evnt->code)
	{
		case CODE_MOLE_STORE_IN:
		case CODE_MOLE_STORE_OUT:
			return OnStoreAction(evnt);
		case CODE_MOLE_PLAYER_COUNTDOWN:
			doRefresh();
			break;
	}
	return rtrn;
}

int ClientPhaseNC_Devel::OnStoreAction(cargoMoleEvent *evnt)
{
	//unsigned int playerId = evnt->data1;
	unsigned int storeId = evnt->data2;

	if(storeId >= STORE_NUM) {
		VLOG_MSG(LOG_LOW,"ClientPhaseNC_Devel::OnStoreAction - bad storeId: %d", storeId);
		return -1;
	}
	if(evnt->code == CODE_MOLE_STORE_IN)
	{
		storeOutfitTime[storeId] = timerCurrent();
	}
	else if(evnt->code == CODE_MOLE_STORE_OUT)
	{
	}
	return 1;
}


int ClientPhaseNC_Devel::OnCargoPlot(cargoMolePlot *pCargo)
{
	ClientPhaseNC::OnCargoPlot(pCargo);
	doRefresh();
	return 1;
}

int ClientPhaseNC_Devel::OnCargoDevel(cargoMoleDevel *pCargo)
{
	ClientPhaseNC::OnCargoDevel(pCargo);

	doRefresh();
	return 1;
}

int ClientPhaseNC_Devel::OnCargoMove(cargoMoleMove *pCargo)
{
	ClientPhaseNC::OnCargoMove(pCargo);

	VLOG_MSG(LOG_DUMP,"OnCargoMove - vPos[%f,%f,%f]",
			pCargo->data.vPos[0],
			pCargo->data.vPos[1],
			pCargo->data.vPos[2]);
	doRefresh();
	return 1;
}

int ClientPhaseNC_Devel::OnChar(int c)
{
	return ClientPhaseNC::OnChar(c);
}

int ClientPhaseNC_Devel::execPlayerSelect(int playerId)
{
	return ClientPhaseNC::execPlayerSelect(playerId);
}

int ClientPhaseNC_Devel::execMoveDir(int playerId, int dir)
{
	vect3f vDir;
	vect3f vTest;
	moveable_t move;
	cargoMolePlayer *pPlayer;

	pPlayer = playerGet(playerId);
	if(pPlayer == NULL)
		return 0;
	if(pPlayer->flags & MOLE_PLAYER_OUTFIT)
		return 0; // frozen in a store

	if(playerPosGet(playerId, &move) <= 0)
		return 0;

	VECTINIT3F(vDir, 0.0f);
	switch(dir)
	{
		case MOVE_DIR_LEFT: vDir[0] = -1.0f; break;
		case MOVE_DIR_DOWN: vDir[1] = 1.0f; break;
		case MOVE_DIR_UP: vDir[1] = -1.0f; break;
		case MOVE_DIR_RIGHT: vDir[0] = 1.0f; break;
	}

	VECTCOPY3F(vTest, move.vPos);

	int plotId = statePosToPlot(m_pState, move.vPos);
	
	if(plotId == (int)m_pState->store->plot) {
		vTest[0] += (vDir[0] / m_pState->plotInfo->nPlotX) / BOARD_WIDTH;
		vTest[1] += (vDir[1] / m_pState->plotInfo->nPlotY) / BOARD_HEIGHT;
	}
	else {
		vTest[0] += vDir[0] / BOARD_WIDTH;
		vTest[1] += vDir[1] / BOARD_HEIGHT;
	}

	/*
	move.vVel[0] = MAX_VEL * vDir[0];
	move.vVel[1] = MAX_VEL * vDir[1];
	move.vVel[2] = MAX_VEL * vDir[2];
	move.time = timerCurrent();
	playerPosSet(TODO_PLAYER, &move);
	playerPosShip(TODO_PLAYER);
	*/

	// TODO: adjust for store
	vDir[0] /= BOARD_WIDTH;
	vDir[1] /= BOARD_HEIGHT;

	VECTINIT3F(move.vVel, 0.0f);
	VECTCOPY3F(move.vPos, vTest);
	move.time += (mUint32)(sqrtf(vDir[0]*vDir[0] + vDir[1]*vDir[1]) / MAX_VEL);
	playerPosSet(TODO_PLAYER, &move);
	playerPosShip(TODO_PLAYER);
	return 1;
}


