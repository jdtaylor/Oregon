
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2007  James Douglas Taylor
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: "James D. Taylor" <james.d.taylor@gmail.com>
 */


#include "ClientPhaseNC_Multi.h"


ClientPhaseNC_MultiPlayer::ClientPhaseNC_MultiPlayer() : ClientPhaseNC()
{
}

ClientPhaseNC_MultiPlayer::~ClientPhaseNC_MultiPlayer()
{
	destroy();
}

int ClientPhaseNC_MultiPlayer::create()
{
	if(ClientPhaseNC::create() < 0)
		return -1;
	return 1;
}

int ClientPhaseNC_MultiPlayer::destroy()
{
	ClientPhaseNC::destroy();
	return 1;
}

int ClientPhaseNC_MultiPlayer::show()
{
	if(ClientPhaseNC::show() < 0)
		return -1;
	return 1;
}

int ClientPhaseNC_MultiPlayer::hide()
{
	ClientPhaseNC::hide();
	return 1;
}

int ClientPhaseNC_MultiPlayer::doFrame(int tic)
{
	return 1;
}


