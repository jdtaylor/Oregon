
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2007  James Douglas Taylor
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: "James D. Taylor" <james.d.taylor@gmail.com>
 */


#include <string.h>
#include <ncurses.h>
#include "metric/mchar.h"
#include "ClientPhaseNC_Alien.h"



ClientPhaseNC_AlienSelect::ClientPhaseNC_AlienSelect() : ClientPhaseNC()
{
}

ClientPhaseNC_AlienSelect::~ClientPhaseNC_AlienSelect()
{
	destroy();
}

int ClientPhaseNC_AlienSelect::execute()
{
	return ClientPhaseNC::execute();
}

int ClientPhaseNC_AlienSelect::create()
{
	if(ClientPhaseNC::create() < 0)
		return -1;
	return 1;
}

int ClientPhaseNC_AlienSelect::destroy()
{
	return 1;
}

int ClientPhaseNC_AlienSelect::show()
{
	//playerCreateNew(); /* tell the server we need a new player */
	setFrameDelay(25); /* 40 fps */
	return 1;
}

int ClientPhaseNC_AlienSelect::hide()
{
	ClientPhaseNC::hide();
	return 1;
}

int ClientPhaseNC_AlienSelect::execPlayerSelect(int localId)
{
	cargoMolePlayer p;
	mUint8 chewey[] = "chuman";

//	if(subState == SUB_ALIEN_HUMAN)
//	{
		// get the player who selected, set it's alien type,
		// and send it to the server.
		if(playerLocalGet(localId, &p) < 0) {
			cargoMolePlayerInit(&p);
			p.flags |= MOLE_PLAYER_CREATE;
			unicode_cpy(p.name, chewey, M_UTF8, MAX_MOLE_PLAYER_NAME);
		}
		//p.alienType = A_HUMAN;
		//playerStart(&p);
		playerLocalSet(localId, &p);
		playerShip(localId);

		ClientPhaseNC::execPlayerSelect(localId);
//	}
	return 1;
}

int ClientPhaseNC_AlienSelect::doFrame(int frame)
{
	if(!m_bRefresh)
		return 1;
	return 1;
}



