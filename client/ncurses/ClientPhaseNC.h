
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2007  James Douglas Taylor
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: "James D. Taylor" <james.d.taylor@gmail.com>
 */

#ifndef CLIENT_PHASE_NC_H
#define CLIENT_PHASE_NC_H

#include "ClinetPhase.h"
//#include "metric/timer.h"


enum {
	MOVE_DIR_DOWN = 0,
	MOVE_DIR_UP,
	MOVE_DIR_LEFT,
	MOVE_DIR_RIGHT
};


class ClientPhaseNC : public ClientPhase
{
public:
	ClientPhaseNC();
	virtual ~ClientPhaseNC();

	virtual int execute();
	int screenOpen();
	int screenClose();
	int screenResize(int w, int h);
	int preFrame();
	int postFrame();

	virtual int create();
	virtual int destroy();
	virtual int show();
	virtual int hide();
	virtual int doFrame(int frame);

protected:
	int makeBoardBG();
	int printBoard();

	static int m_width;
	static int m_height;
	static char m_boardBG[BOARD_HEIGHT][BOARD_WIDTH];
	static char m_strStatus[BOARD_WIDTH];
	static long m_statusTime;

	static int m_bMsgMode;
	static int m_nMsgChar;
	static cargoMolePlayerMsg m_cargoMsg;

	virtual int OnChar(int c);
	virtual int execMoveDir(int playerId, int dir);
	char resToChar(int res);
//	virtual int execPlayerDir(int localId, 

	virtual int OnCargoPlayerMsg(cargoMolePlayerMsg *pCargo);
private:
	int placeRandom(int plotId, char c, int n);
	int getScreenSize();
};

#endif


