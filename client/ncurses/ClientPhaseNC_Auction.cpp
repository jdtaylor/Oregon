
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2007  James Douglas Taylor
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: "James D. Taylor" <james.d.taylor@gmail.com>
 */


#include <ncurses.h>
#include "ClientPhaseNC_Auction.h"

//[31,2]

static char g_auctionBG[BOARD_HEIGHT][BOARD_WIDTH] = {
"                            Status #                          ",
"                                                              ",
"    [ ]     .         .         .         .         .     [ ] ",
"                                                              ",
"                                                           __ ",
"                                                          /  \\",
"            = - - - - - - - - - - - - - - - - - - - =     |_#|",
"                                                              ",
"                                                              ",
"                                                              ",
"                                                              ",
"                                                              ",
"                                                              ",
"                                                              ",
"                                                              ",
"                                                              ",
"                                                              ",
"                                                              ",
"    __                                                        ",
"   /  \\                                                       ",
"   |_#|     =- - - - - - - - - - - - - - - - - - - -=         ",
"                                                              ",
"                                                              ",
"            .         .         .         .         .         ",
"    Money:                                             :Money ",
"    Units:                                             :Units ",
};

#define STATUS_TOP_Y 0
#define STATUS_BOT_Y 26

//! x offset of player column
#define PCOL_LEFT 12
#define PCOL_RIGHT 52
//! y offset of player column
#define PCOL_TOP 6
#define PCOL_BOT 20

//! x offset of first player column
#define PCOL_WIDTH 10
//! y height of player columns
#define PCOL_HEIGHT 14

#define PCOL_TOT_WIDTH (PCOL_RIGHT - PCOL_LEFT + 1)

#define MONEY_Y 24
#define UNITS_Y 25


ClientPhaseNC_Auction::ClientPhaseNC_Auction() : ClientPhaseNC()
{
}

ClientPhaseNC_Auction::~ClientPhaseNC_Auction()
{
	destroy();
}

int ClientPhaseNC_Auction::execute()
{
	ClientPhaseNC::execute();

	int c = getch();
	if(c == ERR)
		return 0;

	switch(c)
	{
		case '\022':
			doRefresh();
			break;
		case ' ':
			// tell the server that we want the currently selected
			// plot of land
			shipPlayerEvent(TODO_PLAYER, CODE_MOLE_PLAYER_SELECT);
			doRefresh();
			break;
	}
	return 1;
}

int ClientPhaseNC_Auction::create()
{
	if(ClientPhaseNC::create() < 0)
		return -1;
	return 1;
}

int ClientPhaseNC_Auction::destroy()
{
	ClientPhaseNC::destroy();
	return 1;
}

int ClientPhaseNC_Auction::show()
{
	if(ClientPhaseNC::show() < 0)
		return -1;
	doRefresh();
	maxTimeout = 10; // TODO
	return 1;
}

int ClientPhaseNC_Auction::hide()
{
	ClientPhaseNC::hide();
	return 1;
}

int ClientPhaseNC_Auction::handleTruck(moleTruck *pTruck, int *pStatus)
{
	if(pTruck->type == CARGO_MOLE_AUCTION)
		doRefresh();
	if(pTruck->type == CARGO_MOLE_SYS)
		doRefresh();

	return ClientPhaseNC::handleTruck(pTruck, pStatus);
}

int ClientPhaseNC_Auction::OnCargoEvent(cargoMoleEvent *evnt)
{
	int rtrn = ClientPhaseNC::OnCargoEvent(evnt);

	if(evnt->code == CODE_MOLE_COUNTDOWN)
		doRefresh();
	if(evnt->code == CODE_MOLE_AUCTION_PRICE)
		doRefresh();
	return rtrn;
}

int ClientPhaseNC_Auction::OnCargoPlayer(cargoMolePlayer *pPlayer)
{
	ClientPhaseNC::OnCargoPlayer(pPlayer);
	doRefresh();
	return 1;
}

int ClientPhaseNC_Auction::OnCargoPlot(cargoMolePlot *pPlot)
{
	ClientPhaseNC::OnCargoPlot(pPlot);
	doRefresh();
	return 1;
}

int ClientPhaseNC_Auction::doFrame(int tic)
{
	unsigned int i;
	char buf[BOARD_WIDTH];
	char reschar;
	cargoMolePlayer *pPlayer;
	int res = m_pState->auction->res;

	if(!m_bRefresh)
		return 1;
	pPlayer = statePlayerGet(m_pState, playerIdGet(TODO_PLAYER));

	erase();
	color_set(1, NULL);

	for(i = 0; i < BOARD_HEIGHT; i++) {
		move(i, 0);
		addnstr(g_auctionBG[i], BOARD_WIDTH);
	}
	switch(res)
	{
	case RESOURCE_FOOD:
		reschar = 'F';
		strcpy(buf, "   Food   ");
		break;
	case RESOURCE_ENERGY:
		reschar = 'E';
		strcpy(buf, "  Energy  ");
		break;
	case RESOURCE_ORE:
		reschar = 'O';
		strcpy(buf, " Smithore ");
		break;
	case RESOURCE_CRYSTITE:
		reschar = 'C';
		strcpy(buf, " Crystite ");
		break;
	default:
		reschar = ' ';
		strcpy(buf, "          ");
		break;
	}
	move(1, (BOARD_WIDTH - strlen(buf)) / 2); addnstr(buf, 10);
	move(2, 5); addch(reschar);
	move(2, 59); addch(reschar);
	//
	// which round (month) it is.
	snprintf(buf, sizeof(buf), "%02d", m_pState->sys->round);
	move(0,36); addnstr(buf, 2);

	snprintf(buf, sizeof(buf), "%3d", m_pState->store->resSell[res]);
	move(PCOL_TOP, 54); addnstr(buf, 3);

	snprintf(buf, sizeof(buf), "%3d", m_pState->store->resBuy[res]);
	move(PCOL_BOT, 8); addnstr(buf, 3);

	switch(m_pState->auction->mode)
	{
		case AUCTION_MODE_PREV:
		case AUCTION_MODE_USE:
		case AUCTION_MODE_SPOIL:
		case AUCTION_MODE_PROD:
			doStatus();
			break;
		case AUCTION_MODE_BUYSELL:
			doBuySell();
			break;
		case AUCTION_MODE_BARTER:
			doBarter();
			break;
	}
	return 1;
}

int ClientPhaseNC_Auction::doStatus()
{
	int i;
	int ip;
	int x, y;
	float fScale;
	unsigned int val;
	char buf[BOARD_WIDTH];
	cargoMolePlayer *pPlayer;
	int res = m_pState->auction->res;

	ip = 0;
	while((pPlayer = statePlayerIncr(m_pState, &ip)) != NULL)
	{
		x = pPlayer->id * PCOL_WIDTH + PCOL_LEFT;

		color_set((short)(pPlayer->id % 5) + 2, NULL);

		switch(m_pState->auction->mode)
		{
		case AUCTION_MODE_PREV:
			val = pPlayer->prev[res];
			break;
		case AUCTION_MODE_USE:
			val = pPlayer->prev[res] - pPlayer->use[res];
			break;
		case AUCTION_MODE_SPOIL:
			val = pPlayer->prev[res] - pPlayer->use[res] - pPlayer->spoil[res];
			break;
		case AUCTION_MODE_PROD:
			val = playerResCur(pPlayer, res);
			break;
		}
		snprintf(buf, sizeof(buf), "%9d", pPlayer->money);
		move(MONEY_Y, x); addnstr(buf, 9);
		snprintf(buf, sizeof(buf), "%9d", val);
		move(UNITS_Y, x); addnstr(buf, 9);
		//
		// resource need line
		if(pPlayer->need[res]) {
			fScale = scaleRes(res, pPlayer->need[res]);
			y = (int)(fScale * PCOL_HEIGHT);
			if(y) {
				y = PCOL_BOT - y;
				move(y, x+3); addch('_');
				move(y, x+4); addch('_');
				move(y, x+5); addch('_');
				move(y, x+6); addch('_');
				move(y, x+7); addch('_');
			}
		}
		//
		// draw scale representing previous rsource amount
		fScale = scaleRes(res, val);
		i = PCOL_BOT - (int)(fScale * PCOL_HEIGHT);
		while(i < PCOL_BOT)
		{
			move(i, x+5);
			addch('#');
			i++;
		}
		move(PCOL_BOT+2, x+3); addnstr("/|=|\\", 5);
		move(PCOL_BOT+3, x+3); addnstr("+-0-+", 5);
	}
	switch(m_pState->auction->mode)
	{
	case AUCTION_MODE_PREV:
		move(STATUS_BOT_Y, PCOL_LEFT +
				(PCOL_TOT_WIDTH - strlen("PREVIOUS AMOUNT")) / 2);
		addstr("PREVIOUS AMOUNT");
		break;
	case AUCTION_MODE_USE:
		move(STATUS_BOT_Y, PCOL_LEFT +
				(PCOL_TOT_WIDTH - strlen("USAGE")) / 2);
		addstr("USAGE");
		break;
	case AUCTION_MODE_SPOIL:
		move(STATUS_BOT_Y, PCOL_LEFT +
				(PCOL_TOT_WIDTH - strlen("SPOILAGE")) / 2);
		addstr("SPOILAGE");
		break;
	case AUCTION_MODE_PROD:
		move(STATUS_BOT_Y, PCOL_LEFT +
				(PCOL_TOT_WIDTH - strlen("PRODUCTION")) / 2);
		addstr("PRODUCTION");
		break;
	}

	return 1;
}

int ClientPhaseNC_Auction::doBuySell()
{
	int i;
	int x, y;
	int ip;
	char buf[BOARD_WIDTH];
	cargoMolePlayer *pPlayer;
	int res = m_pState->auction->res;

	ip = 0;
	while((pPlayer = statePlayerIncr(m_pState, &ip)) != NULL)
	{
		x = pPlayer->id * PCOL_WIDTH + PCOL_LEFT;

		color_set((short)(pPlayer->id % 5) + 2, NULL);

		snprintf(buf, sizeof(buf), "%9d", pPlayer->money);
		move(MONEY_Y, x); addnstr(buf, 9);
		snprintf(buf, sizeof(buf), "%9d", playerResCur(pPlayer, res));
		move(UNITS_Y, x); addnstr(buf, 9);
		if(pPlayer->flags & MOLE_PLAYER_SELL)
		{
			move(PCOL_TOP-3, x+3); addnstr("+-0-+", 5);
			move(PCOL_TOP-2, x+3); addnstr("\\|=|/", 5);
		}
		else
		{
			move(PCOL_BOT+2, x+3); addnstr("/|=|\\", 5);
			move(PCOL_BOT+3, x+3); addnstr("+-0-+", 5);
		}


		y = m_pState->sys->countdown;
		y = PCOL_BOT - (int)((float)y / maxTimeout * PCOL_HEIGHT * 0.8f);

		for(i = y; i <= PCOL_BOT; i++) {
			move(i, PCOL_RIGHT+5);
			addnstr("##", 3);
		}
	}
	return 1;
}

int ClientPhaseNC_Auction::doBarter()
{
	int i;
	int ip;
	int x, y;
	float f;
	char buf[BOARD_WIDTH];
	cargoMolePlayer *pPlayer;
	int res = m_pState->auction->res;
	unsigned int priceMin, priceMax;

	priceMin = m_pState->store->resBuy[res];
	priceMax = m_pState->store->resSell[res];

	ip = 0;
	while((pPlayer = statePlayerIncr(m_pState, &ip)) != NULL)
	{
		x = pPlayer->id * PCOL_WIDTH + PCOL_LEFT;

		color_set((short)(pPlayer->id % 5) + 2, NULL);

		if(pPlayer->resPrice < 0)
		{
			if(pPlayer->flags & MOLE_PLAYER_SELL)
				y = PCOL_TOP - 2;
			else
				y = PCOL_BOT + 2;
		}
		else
		{
			f = (float)pPlayer->resPrice - priceMin;
			f /= (float)(priceMax - priceMin + 1);
			if(f < 0.0f)
				f = 0.0f;
			if(f > 1.0f)
				f = 1.0f;
			f *= PCOL_HEIGHT;
			y = PCOL_BOT - (int)f;
		}
		if(pPlayer->flags & MOLE_PLAYER_SELL)
		{
			move(y-1, x+3); addnstr("+-0-+", 5);
			move(y, x+3); addnstr("\\|=|/", 5);
		}
		else
		{
			move(y, x+3); addnstr("/|=|\\", 5);
			move(y+1, x+3); addnstr("+-0-+", 5);
		}

		if(pPlayer->resPrice >= 0) {
			snprintf(buf, sizeof(buf), "%9d", pPlayer->resPrice);
			move(MONEY_Y, x); addnstr(buf, 9);
		}
		snprintf(buf, sizeof(buf), "%9d", playerResCur(pPlayer, res));
		move(UNITS_Y, x); addnstr(buf, 9);

		y = m_pState->sys->countdown;
		y = PCOL_BOT - (int)((float)y / maxTimeout * PCOL_HEIGHT * 0.8f);

		for(i = y; i <= PCOL_BOT; i++) {
			move(i, PCOL_RIGHT+5);
			addnstr("##", 3);
		}
	}
	return 1;
}

float ClientPhaseNC_Auction::scaleRes(int res, float val)
{
	float scale;
	// TODO: I want a steeper curve
	scale = sqrtf(val) / sqrtf(g_playerResMax[res]);
	if(scale > 1.0f)
		return 1.0f;
	return scale;
}


