
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2007  James Douglas Taylor
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: "James D. Taylor" <james.d.taylor@gmail.com>
 */


#include <signal.h>
#include <ncurses.h>
#include <sys/ioctl.h>
#include <time.h>

#include "metric/common.h"
#include "metric/timer.h"
#include "ClientPhaseNC_Base.h"


#define PLOT_WIDTH 7
#define PLOT_HEIGHT 5
#define PLOT_NX 9
#define PLOT_NY 5


static int g_bResize;
static void winch_handler(int notused)
{
	g_bResize = TRUE;  // ClientPhaseNC_Io.h
}

int ClientPhaseNC::m_bMsgMode = FALSE;
int ClientPhaseNC::m_nMsgChar = 0;
cargoMolePlayerMsg ClientPhaseNC::m_cargoMsg;

int ClientPhaseNC::m_width;
int ClientPhaseNC::m_height;
char ClientPhaseNC::m_boardBG[BOARD_HEIGHT][BOARD_WIDTH];
char ClientPhaseNC::m_strStatus[BOARD_WIDTH];
long ClientPhaseNC::m_statusTime = 0;


ClientPhaseNC::ClientPhaseNC() : ClientPhase()
{
	cargoMolePlayerMsgInit(&m_cargoMsg);
}

ClientPhaseNC::~ClientPhaseNC()
{
	destroy();
}

int ClientPhaseNC::execute()
{
	ClientPhase::execute();

	int c = getch();
	if(c == ERR)
		return 0;
	OnChar(c);
	return 1;
}

int ClientPhaseNC::OnChar(int c)
{
	if(m_bMsgMode && c < 128)
	{
		// constructing a text message/command
		if(c == KEY_ENTER || m_nMsgChar >= MOLE_PLAYER_MSG_MAX-1)
		{
			// send message to server
			m_cargoMsg.srcId = playerIdGet(TODO_PLAYER);
			shipTruck(m_cargoMsg.srcId, (moleTruck*)&m_cargoMsg);

			m_nMsgChar = 0;
			m_bMsgMode = FALSE;
			return 1;
		}
		m_cargoMsg.msg[m_nMsgChar] = c;
		m_cargoMsg.msg[m_nMsgChar+1] = '\0';
		m_nMsgChar++;
		return 1;
	}
	switch(c)
	{
		case '\022':
			doRefresh();
			break;
		case ' ':
			execPlayerSelect(TODO_PLAYER);
			//doRefresh();
			break;
		case KEY_DOWN:
		case 'j':
			return execMoveDir(TODO_PLAYER, MOVE_DIR_DOWN);
		case KEY_UP:
		case 'k':
			return execMoveDir(TODO_PLAYER, MOVE_DIR_UP);
		case KEY_LEFT:
		case 'h':
			return execMoveDir(TODO_PLAYER, MOVE_DIR_LEFT);
		case KEY_RIGHT:
		case 'l':
			return execMoveDir(TODO_PLAYER, MOVE_DIR_RIGHT);
		case 't':
			m_bMsgMode = TRUE;
			break;
		case KEY_EXIT:
			if(m_bMsgMode) {
				m_bMsgMode = FALSE;
				break;
			}
		case 'Q':
		case 'q':
		//case 27: // WTF?! I thought this was ESC, KEY_UP tirggers..
			exitApp();
			break;
	}
	return 1;
}


int ClientPhaseNC::screenOpen()
{
	WINDOW *pWnd;
	signal(SIGWINCH, winch_handler);

	pWnd = initscr();
	start_color();
	nonl();
	noecho();
	cbreak();
	nodelay(pWnd, TRUE); // set stdin to non-blocking
	curs_set(0); // hide cursor
	keypad(pWnd, TRUE); // enable keypad function keys

	/*
	 * [d ] COLOR_RED = (680, 0, 0)
	 * [d ] COLOR_GREEN = (0, 680, 0)
	 * [d ] COLOR_BLUE = (0, 0, 680)
	 * [d ] COLOR_YELLOW = (680, 680, 0)
	 * [d ] COLOR_MAGENTA = (680, 0, 680)
	 * [d ] COLOR_CYAN = (0, 680, 680)
	 *
	 *
	VLOG_MSG(LOG_DUMP,"can_change_color - %d", can_change_color);
	[d ] can_change_color - 1073998416
	
	* below routines fail. (don't effect the colors)

	init_color(COLOR_RED,	200, 0, 0);
	init_color(COLOR_GREEN,	0, 100, 0);
	init_color(COLOR_BLUE,	0, 0, 200);
	init_color(COLOR_YELLOW,0, 0, 0);
	init_color(COLOR_MAGENTA,200, 0, 200);
	init_color(COLOR_CYAN,	0, 200, 200);
	 */

	init_pair(1, COLOR_WHITE, COLOR_BLACK);
	init_pair(2, COLOR_RED, COLOR_BLACK);
	init_pair(3, COLOR_GREEN, COLOR_BLACK);
	init_pair(4, COLOR_BLUE, COLOR_BLACK);
	init_pair(5, COLOR_YELLOW, COLOR_BLACK);
	init_pair(6, COLOR_MAGENTA, COLOR_BLACK);

	return 1;
}

int ClientPhaseNC::screenClose()
{
	endwin();
	return 1;
}

int ClientPhaseNC::screenResize(int w, int h)
{
	resizeterm(h, w);
	clear();
	g_bResize = FALSE;
	return 1;
}

int ClientPhaseNC::getScreenSize()
{
	struct winsize winSize;
	m_width = 80;
	m_height = 32;

	if(ioctl(2, TIOCGWINSZ, &winSize) == 0)
	{
		m_width = winSize.ws_col;
		m_height = winSize.ws_row;
		if(m_width <= 0)
			m_width = 1;
		if(m_height <= 0)
			m_height = 1;
	}
	return 1;
}

int ClientPhaseNC::preFrame()
{
	if(g_bResize) {
		getScreenSize();
		screenResize(m_width, m_height);
		doRefresh();
	}
	return 1;
}

int ClientPhaseNC::postFrame()
{
	if(m_bRefresh)
		refresh(); // this is ncurses call
	m_bRefresh = FALSE;
	return 1;
}


int ClientPhaseNC::create()
{
	if(ClientPhase::create() < 0)
		return -1;
	return 1;
}

int ClientPhaseNC::destroy()
{
	ClientPhase::destroy();
	return 1;
}

int ClientPhaseNC::show()
{
	if(ClientPhase::show() < 0)
		return -1;
	return 1;
}

int ClientPhaseNC::hide()
{
	ClientPhase::hide();
	return 1;
}

int ClientPhaseNC::doFrame(int tic)
{
	if(timerCurrent() < m_statusTime + 5000)
	{
		move(BOARD_HEIGHT-1, 2);
		addstr(m_strStatus);
	}
	//
	// print status at bottom of board
//	for(i = 0; i < BOARD_WIDTH; i++) {
//		move(BOARD_HEIGHT-1, i);
//		addch(' '); // clear whole status bar
//	}
	return 1;
}

int ClientPhaseNC::OnCargoPlayerMsg(cargoMolePlayerMsg *pCargo)
{
	if(pCargo->srcId != -1)
	{
		cargoMolePlayer *pPlayer = statePlayerGet(m_pState, pCargo->srcId);
		if(pPlayer == NULL)
			return 0;
		strncpy(m_strStatus, (char*)pPlayer->name, sizeof(m_strStatus));
	}
	else
		strncpy(m_strStatus, "<server>", sizeof(m_strStatus));

	strncat(m_strStatus, ": ", sizeof(m_strStatus) - strlen(m_strStatus) - 1);
	strncat(m_strStatus, (char*)pCargo->msg,
			sizeof(m_strStatus) - strlen(m_strStatus) - 1);

	m_statusTime = timerCurrent();
	doRefresh();

	return ClientPhase::OnCargoPlayerMsg(pCargo);
}

int ClientPhaseNC::placeRandom(int plotId, char c, int n)
{
	int i;
	int iX = (int)((plotId % PLOT_NX) * PLOT_WIDTH);
	int iY = (int)((plotId / PLOT_NX) * PLOT_HEIGHT);
	int randX, randY;

	for(i = 0; i < n; i++)
	{
		randX = iX + (int)(float(PLOT_WIDTH) * rand() / RAND_MAX);
		randY = iY + (int)(float(PLOT_HEIGHT) * rand() / RAND_MAX);
		ASSERT(randX >= 0 || randX < BOARD_WIDTH);
		ASSERT(randY >= 0 || randY < BOARD_HEIGHT);

		if(m_boardBG[randY][randX] != ' ' && m_boardBG[randY][randX] != '\0')
			continue; // don't overwrite
		m_boardBG[randY][randX] = c;
	}
	return 1;
}

int ClientPhaseNC::makeBoardBG()
{
	int i, j;
	cargoMolePlot *pPlot;

	srand(time(NULL));

	i = 0;
#define S(line) strcpy(m_boardBG[i++], line)
	S("|------|------|------|------|------|------|------|------|------|");
	S("|                                                              |");
	S("|                                                              |");
	S("|                                                              |");
	S("|                                                              |");
	S("+-                                                            -+");
	S("|                                                              |");
	S("|                                                              |");
	S("|                                                              |");
	S("|                                                              |");
	S("+-                          #------#                          -+");
	S("|                           |@__|##|                           |");
	S("|                                                              |");
	S("|                            __                                |");
	S("|                           |!!|#HH|                           |");
	S("+-                          #------#                          -+");
	S("|                                                              |");
	S("|                                                              |");
	S("|                                                              |");
	S("|                                                              |");
	S("+-                                                            -+");
	S("|                                                              |");
	S("|                                                              |");
	S("|                                                              |");
	S("|                                                              |");
	S("[                                                              ]");
	//S("|------|------|------|------|------|------|------|------|------|");

	unsigned int res, resMax;

	for(i = 0; i < (int)m_pState->plotInfo->nPlot; i++)
	{
		pPlot = statePlotGet(m_pState, i);
		res = 0;
		resMax = 0;
		for(j = 0; j < RESOURCE_NUM; j++) {
			if(pPlot->resPotential[j] > resMax) {
				resMax = pPlot->resPotential[j];
				res = j;
			}
		}
		switch(res)
		{
			case RESOURCE_FOOD:
				if(resMax >= 3)
					placeRandom(i, '$', resMax+8);
				break;
			//case RESOURCE_ENERGY:
			//	placeRandom(m_pState, m_boardBG, i, '.', resMax);
			//	break;
			case RESOURCE_ORE:
				placeRandom(i, '^', resMax);
				break;
		}
	}
	return 1;
}


int ClientPhaseNC::printBoard()
{
	int i, j;
	int x, y;
	int iPlot;
	char c;
	cargoMolePlot *pPlot;

	color_set(1, NULL);

//	for(i = 0; i < BOARD_HEIGHT; i++) {
//		move(i, 0);
//		addnstr(m_boardBG[i], BOARD_WIDTH);
		//
		// code where background color denotes player ownership:
		//if(i % 5 == 0) {
		//	move(i, 0);
		//	addnstr(m_boardBG[i], BOARD_WIDTH);
		//}
		//for(j = 0; j < BOARD_WIDTH; j += 7)
		//	mvaddch(i, j, m_boardBG[i][j]);
//	}
	//
	// draw owned plot data
	//
	for(iPlot = 0; iPlot < (int)m_pState->plotInfo->nPlot; iPlot++)
	{
		pPlot = statePlotGet(m_pState, iPlot);

		x = (int)((iPlot % PLOT_NX) * PLOT_WIDTH);
		y = (int)((iPlot / PLOT_NX) * PLOT_HEIGHT);

		color_set(1, NULL);

		for(i = y; i <= y+PLOT_HEIGHT; i++)
			for(j = x; j <= x+PLOT_WIDTH; j++)
				mvaddch(i, j, m_boardBG[i][j]);

		if(pPlot->owner < 0)
			continue; // no owner
		color_set((short)(pPlot->owner % 5)+2, NULL);

		//
		// border
		mvaddch(y, x+0, '+');
		mvaddch(y, x+1, '-');
		mvaddch(y, x+2, '-');
		mvaddch(y, x+3, '-');
		mvaddch(y, x+4, '-');
		mvaddch(y, x+5, '-');
		mvaddch(y, x+6, '+');

		mvaddch(y+1, x+0, '|');
		mvaddch(y+1, x+6, '|');
		mvaddch(y+2, x+0, '|');
		mvaddch(y+2, x+6, '|');
		mvaddch(y+3, x+0, '|');
		mvaddch(y+3, x+6, '|');

		mvaddch(y+4, x+0, '+');
		mvaddch(y+4, x+1, '-');
		mvaddch(y+4, x+2, '-');
		mvaddch(y+4, x+3, '-');
		mvaddch(y+4, x+4, '-');
		mvaddch(y+4, x+5, '-');
		mvaddch(y+4, x+6, '+');

		//
		// playerId top-left
		//move(y+2, x+2); addch((char)(pPlot->owner + '0'));
		//
		// plot's product info
		c = '\0';
		switch(pPlot->res) {
			case RESOURCE_FOOD:
				c = 'F';
				break;
			case RESOURCE_ENERGY:
				c = 'E';
				break;
			case RESOURCE_ORE:
				c = 'O';
				break;
			case RESOURCE_CRYSTITE:
				c = 'C';
				break;
			default:
				break;
		}
		if(c != '\0') {
			move(y+2, x+1); addch(c);
			move(y+3, x+1); addch((char)(pPlot->resPotential[pPlot->res]+'0'));
			//move(y+4, x+5); addch((char)(pPlot->resProduced + '0'));
			if(m_pState->sys->mode == MOLE_MODE_PROD) {
				// producing
				if(pPlot->resProduced > 0) { move(y+3, x+5); addch('.'); }
				if(pPlot->resProduced > 1) { move(y+3, x+4); addch('.'); }
				if(pPlot->resProduced > 2) { move(y+2, x+5); addch('.'); }
				if(pPlot->resProduced > 3) { move(y+2, x+4); addch('.'); }
				if(pPlot->resProduced > 4) { move(y+1, x+5); addch('.'); }
				if(pPlot->resProduced > 5) { move(y+1, x+4); addch('.'); }
			}
		}
	}
	color_set(1, NULL);
	//
	// redraw board border
	for(y = PLOT_NY/2*PLOT_HEIGHT; y <= (PLOT_NY/2+1)*PLOT_HEIGHT; y++)
		for(x = PLOT_NX/2*PLOT_WIDTH; x <= (PLOT_NX/2+1)*PLOT_WIDTH; x++)
			mvaddch(y, x, m_boardBG[y][x]);

	for(i = 0; i < BOARD_HEIGHT; i++)
	{
		mvaddch(i, 0, m_boardBG[i][0]);
		mvaddch(i, BOARD_WIDTH-1, m_boardBG[i][BOARD_WIDTH-1]);
	}
	move(0, 0);
	addnstr(m_boardBG[0], BOARD_WIDTH);
	move(BOARD_HEIGHT-1, 0);
	addnstr(m_boardBG[BOARD_HEIGHT-1], BOARD_WIDTH);
	//
	// draw currently hilighted plot
	//
	while(42)
	{
		if(m_pState->hilitPlot < 0)
			break;
		if(m_pState->hilitPlot >= (int)m_pState->plotInfo->nPlot)
			break;
		x = (int)((m_pState->hilitPlot % PLOT_NX) * PLOT_WIDTH);
		y = (int)((m_pState->hilitPlot / PLOT_NX) * PLOT_HEIGHT);

		for(i = x; i <= x+PLOT_WIDTH; i++) {
			move(y, i); addch('#');
			move(y+PLOT_HEIGHT, i); addch('#');
		}
		for(i = y; i < y+PLOT_HEIGHT; i++) {
			move(i, x); addch('#');
			move(i, x+PLOT_WIDTH); addch('#');
		}
		break;
	}
	return 1;
}

static char g_resToChar[RESOURCE_NUM] = { '?', 'F', 'E', 'O', 'C' };
	
char ClientPhaseNC::resToChar(int res)
{
	if(res < 0 || res >= RESOURCE_NUM)
		return '?';
	return g_resToChar[res];
}

int ClientPhaseNC::execMoveDir(int playerId, int dir)
{
	return 1;
}


/*
 * F: food
 * E: energy
 * O: Ore
 * C: Crystite
 *
	S("+------+------+------+------+------+------+------+------+------+");
	S("|      |      |      |      |      |      |      |      |      |");
	S("|                                                              |");
	S("|                                                              |");
	S("|                                                              |");
	S("|                                                              |");
	S("+-                                                            -+");
	S("|                                                              |");
	S("|                                                              |");
	S("|                                                              |");
	S("|                                                              |");
	S("|                                                              |");
	S("+-                          #------#                          -+");
	S("|                           |@__|##|                           |");
	S("|                           |      |                           |");
	S("|                                                              |");
	S("|                           |__    |                           |");
	S("|                           |!!|#HH|                           |");
	S("+-                          #------#                          -+");
	S("|                                          +----+ +----+       |");
	S("|                                          |F @@| |O   |       |");
	S("|                                          |6 @@| |1   |       |");
	S("|                                          |  @@| |    |       |");
	S("|                                          +----+ +----+       |");
	S("+-                                                            -+");
	S("|                                                              |");
	S("|                                                              |");
	S("|                                                              |");
	S("|                                                              |");
	S("|      |      |      |      |      |      |      |      |      |");
	S("+------+------+------+------+------+------+------+------+------+");

	~!@#$%^&*()`1234567890-qwertyuiop[]\';lkjhgfsdsazxcvbnm,./
*/
