
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2007  James Douglas Taylor
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: "James D. Taylor" <james.d.taylor@gmail.com>
 */


#include <ncurses.h>
#include "ClientPhaseNC_Prod.h"


ClientPhaseNC_Prod::ClientPhaseNC_Prod() : ClientPhaseNC()
{
}

ClientPhaseNC_Prod::~ClientPhaseNC_Prod()
{
	destroy();
}

int ClientPhaseNC_Prod::execute()
{
	ClientPhaseNC::execute();

	int c = getch();
	if(c == ERR)
		return 0;

	switch(c)
	{
		case '\022':
			doRefresh();
			break;
		case ' ':
			// tell the server that we want the currently selected
			// plot of land
			//playerShipEvent(TODO_PLAYER, CODE_MOLE_PLAYER_SELECT);
			doRefresh();
			break;
	}
	return 1;
}

int ClientPhaseNC_Prod::create()
{
	if(ClientPhaseNC::create() < 0)
		return -1;

//	strncat(status, "[Prod]", sizeof(status) - strlen(status));
	return 1;
}

int ClientPhaseNC_Prod::destroy()
{
	ClientPhaseNC::destroy();
	return 1;
}

int ClientPhaseNC_Prod::show()
{
	if(ClientPhaseNC::show() < 0)
		return -1;
	doRefresh();
	return 1;
}

int ClientPhaseNC_Prod::hide()
{
	ClientPhaseNC::hide();
	return 1;
}

int ClientPhaseNC_Prod::doFrame(int tic)
{
	if(!m_bRefresh)
		return 1;
	m_bRefresh = FALSE;

	printBoard();
	return 1;
}

int ClientPhaseNC_Prod::OnCargoEvent(cargoMoleEvent *evnt)
{
	int rtrn = ClientPhaseNC::OnCargoEvent(evnt);
	return rtrn;
}

int ClientPhaseNC_Prod::OnCargoPlot(cargoMolePlot *pCargo)
{
	ClientPhaseNC::OnCargoPlot(pCargo);
	doRefresh();
	return 1;
}


