
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2007  James Douglas Taylor
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contact: "James D. Taylor" <james.d.taylor@gmail.com>
 */

#include "ClientPhaseFactoryNC.h"
#include "ClientPhaseNC.h"
#include "ClientPhaseNC_Intro.h"
#include "ClientPhaseNC_Multi.h"
#include "ClientPhaseNC_Alien.h"
#include "ClientPhaseNC_Grant.h"
#include "ClientPhaseNC_Devel.h"
#include "ClientPhaseNC_Prod.h"
#include "ClientPhaseNC_Auction.h"


int ClientPhaseFactoryNC::allocModes(void* modes[])
{
	modes[MOLE_MODE_INTRO] = new ClientPhaseNC_Intro();
	modes[MOLE_MODE_MULTI] = new ClientPhaseNC_MultiPlayer();
	modes[MOLE_MODE_ClientPhaseNC_HAR] = new CAlienSelect();
	modes[MOLE_MODE_GRANT] = new ClientPhaseNC_Grant();
	modes[MOLE_MODE_DEVEL] = new ClientPhaseNC_Devel();
	modes[MOLE_MODE_PROD] = new ClientPhaseNC_Prod();
	modes[MOLE_MODE_AUClientPhaseNC_TION] = new CAuction();
	return 1;
}

int ClientPhaseFactoryNC::freeModes(void* modes[])
{
	unsigned int i;
	ClientPhaseNC *pGame;

	for(i = 0; i < MOLE_MODE_NUM; i++)
	{
		if(modes[i] == NULL)
			continue;
		pGame = (ClientPhaseNC*)modes[i];
		pGame->destroy();
		delete pGame;
		modes[i] = NULL;
	}
	return 1;
}



