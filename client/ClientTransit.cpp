
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include "SDL/SDL_net.h"
#include "ClientTransit.h"
#include "common.h"
#include "RemoteHost.h"
#include "ByteBuf.h"
#include "MessageFactory.h"

using namespace std;


class ClientTransitData
{
public:
	RemoteHost m_server;
	SDLNet_SocketSet m_sockSet;
};

/*
 */

ClientTransit::ClientTransit()
{
	SDLNet_Init();
	m_data = new ClientTransitData();
	m_data->m_sockSet = SDLNet_AllocSocketSet(1);
}

ClientTransit::~ClientTransit()
{
	delete m_data;
	SDLNet_Quit();
}


int ClientTransit::connect(const char *host, short port)
{
	if(m_data->m_server.connect(host, port) <= 0)
		return -1;
	m_data->m_server.setAdd(m_data->m_sockSet);
	return 1;
}

int ClientTransit::close()
{
	return m_data->m_server.close();
}

int ClientTransit::send(Message& msg)
{
	return m_data->m_server.send(msg);
}

vector<Message*> ClientTransit::poll()
{
	int i;
	vector<Message*> msgs;

	i = SDLNet_CheckSockets(m_data->m_sockSet, 0); // non-blocking
	if(i < 0) {
		// server closed connection
		throw RemoteCloseException();
	}
	else if(i == 0)
		return msgs; // no sockets ready to read

	if(!m_data->m_server.ready())
		return msgs; // no data ready to read.. don't think this will occur

	return m_data->m_server.get();
}


