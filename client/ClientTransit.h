
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#ifndef CLIENT_TRANSIT_H
#define CLIENT_TRANSIT_H

#include <string>
#include <vector>
#include "common/Message.h"

/// \brief Forward declaration of hidden data in ClientTransit class
class ClientTransitData;

/*! \brief Takes care of the transmission of messages on client side.
 *
 * Used by the client to abstract the message passing implementation.
 * It hides its data structures in the implementation file, and allocates
 * it in its constructor.
 */
class ClientTransit
{
public:
	ClientTransit();
	~ClientTransit();

	int connect(const char *host, short port);
	int close();

	int send(Message& msg);
	std::vector<Message*> poll();
protected:
	ClientTransitData *m_data;
};

#endif
