/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 *
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *              Reza Assadi <gassadi@uh.edu>
 *              Danny Bousleiman <dan_man89@hotmail.com>
 *              Jeffrey Jardiolin <jjardiolin@uh.edu>
 *              John Knieper <jknieper@gmail.com>
 *              Mike Mack <mikemackjr@gmail.com>
 *              Rajiv Shah <brainix@gmail.com>
 *              James D. Taylor <james.d.taylor@gmail.com>
 *              Roland Vassallo <rcvassallo@gmail.com>
 */

#ifndef GFX_H
#define GFX_H

#include "SDL.h"
#include "SDL_image.h"
#include "commonSDL.h"

class GFX {
public:
	GFX();

	void setScreen(SDL_Surface *pScreen) { this->pScreen = pScreen; }
	void setFont(SDL_Surface *pFontmap, int size)
	{
		this->pFont = pFontmap;
		this->fontsize = size;
	}

	int drawFont(const char* text, int x, int y);
	int drawFont(int number, int x, int y);
	int drawFontRightJustify(const char* text, int x, int y);
	int drawFontRightJustify(int number, int x, int y);
	int drawFontCentered(const char*, int y);
	int drawRect(SDL_Rect* dest, int size, Uint32 color);
	int drawFilledRect(SDL_Rect* dest, Uint32 color);
	int drawDottedLine(int x1, int y1, int x2, int y2, int width,
						int dotlen, int dotspace, Uint32 color);

protected:
	SDL_Surface *pScreen;
	SDL_Surface *pFont;
	int fontsize;
};

#endif

