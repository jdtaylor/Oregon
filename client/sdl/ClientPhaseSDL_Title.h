
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#ifndef CLIENT_PHASE_SDL_TITLE_H
#define CLIENT_PHASE_SDL_TITLE_H

#include "ClientPhaseSDL.h"

class ClientPhaseSDL_Title : public ClientPhaseSDL
{
public:
	ClientPhaseSDL_Title(IClient& client);

	int open();
	int close();
	int execute();
	int doFrame();
	int msgHandler(Message& msg);
	int OnEvent(SDL_Event& event);

protected:
	SDL_Surface *m_titleScreen;
	SDL_Surface *m_tileChar[4];
	SDL_Surface *m_userinputfont;
	SDL_Surface *m_instructionfont;
	SDL_Surface *m_errorfont;
	bool serverfield; //true if server field is selected to type in
	bool namefield; //true if name field is selected to type in

	int OnMouseButtonDown(SDL_Event& event);
	int OnKeyDown(SDL_Event& event);

	int OnConnect();
};

#endif
