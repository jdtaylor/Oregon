
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */


#include "commonSDL.h"
#include "common.h"


SDL_Surface* CreateSurface(Uint32 flags, int width, int height)
{
	SDL_Surface *surface;
	Uint32 rmask, gmask, bmask, amask;

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
	rmask = 0xff000000;
	gmask = 0x00ff0000;
	bmask = 0x0000ff00;
	amask = 0x000000ff;
#else
	rmask = 0x000000ff;
	gmask = 0x0000ff00;
	bmask = 0x00ff0000;
	amask = 0xff000000;
#endif

	surface = SDL_CreateRGBSurface(flags, width, height, 32,
			rmask, gmask, bmask, amask);
	if(surface == NULL) {
		VLOG_ERR("CreateSurface failed: %s\n", SDL_GetError());
		return NULL;
	}
	return surface;
}

bool operator==(const SDL_Event& e1, const SDL_Event& e2)
{
	if(e1.type != e2.type)
		return false;

	switch(e1.type)
	{
	case SDL_KEYDOWN:
	case SDL_KEYUP:
		if(e1.key.keysym.sym != e2.key.keysym.sym)
			return false;
		break;
	case SDL_MOUSEBUTTONDOWN:
	case SDL_MOUSEBUTTONUP:
		//
		// TODO: this should be looking for which mouse button,
		// not where the cursor is located...
		//
		if(e1.button.x != e2.button.x)
			return false;
		if(e1.button.y != e2.button.y)
			return false;
		break;
	default:
		return false;
	}
	return true;
}


