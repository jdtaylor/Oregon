
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; ifnot, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include "ClientPhaseSDL_Title.h"
#include "common.h"
#include "GFX.h"

using namespace std;


ClientPhaseSDL_Title::ClientPhaseSDL_Title(IClient& client)
	: ClientPhaseSDL(client)
{
	m_titleScreen = IMG_Load(TITLE_SCREEN);
	if(!m_titleScreen) {
		ERROR << "Failed to load: " << TITLE_SCREEN << endl;
		return;
	}
	m_tileChar[0] = IMG_Load(CHARACTER1);
	if(!m_tileChar[0]) {
		ERROR << "Failed to load: " << CHARACTER1 << endl;
		return;
	}
	m_tileChar[1] = IMG_Load(CHARACTER2);
	if(!m_tileChar[1]) {
		ERROR << "Failed to load: " << CHARACTER2 << endl;
		return;
	}
	m_tileChar[2] = IMG_Load(CHARACTER3);
	if(!m_tileChar[2]) {
		ERROR << "Failed to load: " << CHARACTER3 << endl;
		return;
	}
    m_instructionfont = IMG_Load("./data/gfx/fonts/10w.png");
	if(!m_instructionfont) {
		ERROR << "Failed to load: data/gfx/fonts/10w.png" << endl;
		return;
	}
    m_userinputfont = IMG_Load("./data/gfx/fonts/24b.png");
	if(!m_userinputfont) {
		ERROR << "Failed to load: data/gfx/fonts/24b.png" << endl;
		return;
	}
    m_errorfont = IMG_Load("./data/gfx/fonts/10r.png");
	if(!m_errorfont) {
		ERROR << "Failed to load: data/gfx/fonts/10r.png" << endl;
		return;
	}
	serverfield = false;
	namefield = false;
}

int ClientPhaseSDL_Title::open()
{
	SDL_Rect dest;
	MessagePlayer *player = getCurPlayer();

	SDL_BlitSurface(m_titleScreen, NULL, screen, NULL);
	dest.x = 340;
	dest.y = 441;
	SDL_BlitSurface(m_tileChar[0], NULL, screen, &dest);

	data->gfx.setFont(m_userinputfont, 24);
	data->gfx.drawFont(player->server.data(), 330, 334);
	data->gfx.drawFont(player->name.data(), 330, 393);
	dest.x = 0;
	dest.y = 580;
	dest.h = 95;
	dest.w = 798;
	Uint32 color = SDL_MapRGB(screen->format, 0, 0, 0);
	data->gfx.drawFilledRect(&dest, color);  // erase black part

	data->gfx.setFont(m_instructionfont, 10);
	data->gfx.drawFont("instructions for title screen:", 10, 580);
	data->gfx.drawFont("*click on [server] and enter ip address", 20, 595);
	data->gfx.drawFont("*click on [player name] and enter your name", 20, 610);
	data->gfx.drawFont("*select character by clicking on left and right arrows",
			20, 625);
	data->gfx.drawFont("*click connect to begin", 20, 640);

	dest.x = 0;
	dest.y = 0;
	dest.w = SCREEN_WIDTH;
	dest.h = SCREEN_HEIGHT;
	SDL_UpdateRects(screen, 1, &dest);
	//SDL_Flip(screen);

	return ClientPhaseSDL::open();
}

int ClientPhaseSDL_Title::close()
{
	return ClientPhaseSDL::close();
}

int ClientPhaseSDL_Title::execute()
{
	return ClientPhaseSDL::execute();
}

int ClientPhaseSDL_Title::doFrame()
{
	return ClientPhaseSDL::doFrame();
}

int ClientPhaseSDL_Title::msgHandler(Message& msg)
{
	return ClientPhaseSDL::msgHandler(msg);
}

int ClientPhaseSDL_Title::OnEvent(SDL_Event& event)
{
	int id;
	bool bStart;
	InputType inputType;

	inputType = data->m_input.get(event, id, bStart);
	if(inputType == IO_QUIT)
		return 0;

	switch(event.type)
	{
	case SDL_MOUSEBUTTONDOWN:
		OnMouseButtonDown(event);
		break;
	case SDL_KEYDOWN:
		OnKeyDown(event);
		break;
	default:
		break;
	}
	return 1;
}

int ClientPhaseSDL_Title::OnMouseButtonDown(SDL_Event& event)
{
	MessagePlayer *player = getCurPlayer();
	Uint32 color = SDL_MapRGB(screen->format, 255, 0, 0);
	int x,y;
	SDL_Rect dest;

	if(SDL_BUTTON(SDL_GetMouseState(&x,&y)) != SDL_BUTTON_LEFT)
		return 1; // only interested in right button clicks

	serverfield = false;
	namefield = false;

	data->gfx.setFont(m_userinputfont, 24);

	if(x > 374 && y > 496 && x < 388 && y < 510)
	{
		// next char button
		dest.x = 340;
		dest.y = 441;
		dest.w = 50;
		dest.h = 50;
		SDL_BlitSurface(m_titleScreen, &dest, screen, &dest);
		player->character++;
		if(player->character == 3)
			player->character = 0;
		SDL_BlitSurface(m_tileChar[player->character], NULL, screen, &dest);
		SDL_UpdateRects(screen, 1, &dest);
	}
	if(x > 343 && y > 496 && x < 356 && y < 510)
	{
		// prev char button
		dest.x = 340;
		dest.y = 441;
		dest.w = 50;
		dest.h = 50;
		SDL_BlitSurface(m_titleScreen, &dest, screen, &dest);
		player->character--;
		if(player->character == -1)
			player->character = 2;
		SDL_BlitSurface(m_tileChar[player->character], NULL, screen, &dest);
		SDL_UpdateRects(screen, 1, &dest);
	}
	if(x > 202 && y > 330 && x < 694 && y < 359)
	{
		// server typing field selected
		serverfield = true;
		dest.x = 198;
		dest.y = 330;
		dest.h = 30;
		dest.w = 495;
		data->gfx.drawRect(&dest, 2, color); // highlight box
		dest.x = 108;
		dest.y = 389;
		dest.h = 30;
		dest.w = 464;
		SDL_BlitSurface(m_titleScreen, &dest, screen, &dest); // erase old box
		data->gfx.drawFont(player->name.data(), 330, 393);
		dest.x = 100;
		dest.y = 330;
		dest.h = 100;
		dest.w = 606;
		SDL_UpdateRects(screen, 1, &dest);
	}
	else if(x > 112 && y > 389 && x < 570 && y < 418)
	{
		// player name typing field
		namefield = true;
		dest.x = 108;
		dest.y = 389;
		dest.h = 30;
		dest.w = 464;
		data->gfx.drawRect(&dest, 2, color); // highlight box
		dest.x = 198;
		dest.y = 330;
		dest.h = 30;
		dest.w = 495;
		SDL_BlitSurface(m_titleScreen, &dest, screen, &dest); // erase old box
		data->gfx.drawFont(player->server.data(), 330, 334);
		dest.x = 100;
		dest.y = 330;
		dest.h = 100;
		dest.w = 606;
		SDL_UpdateRects(screen, 1, &dest);
	}
	else
	{
		namefield = false;
		serverfield = false;
		dest.x = 100; // erase any left over boxes when clicked out
		dest.y = 330;
		dest.h = 100;
		dest.w = 606;
		SDL_BlitSurface(m_titleScreen, &dest, screen, &dest); // erase old boxes
		data->gfx.drawFont(player->server.data(), 330, 334);
		data->gfx.drawFont(player->name.data(), 330, 393);
		SDL_UpdateRects(screen, 1, &dest);
	}
	if(x > 637 && y > 517 && x < 790 && y < 563)
	{
		// connect button
		OnConnect();
	}
	//SDL_Flip(screen);
	return 1;
}

int ClientPhaseSDL_Title::OnKeyDown(SDL_Event& event)
{
	MessagePlayer *player = getCurPlayer();
	SDL_Rect dest;

	data->gfx.setFont(m_userinputfont, 24);

	if(serverfield)
	{
		dest.x = 330;
		dest.y = 334;
		dest.h = 24;
		dest.w = 360;
		if(event.key.keysym.sym == SDLK_BACKSPACE &&
			player->server.length() > 0)
		{
			 // sets new string with last char gone
			player->server = player->server.substr(0,player->server.length()-1);
			// erase old text
			SDL_BlitSurface(m_titleScreen, &dest, screen, &dest);
			data->gfx.drawFont(player->server.data(), 330, 334);
		}
		else if(event.key.keysym.sym >= 46 &&
				event.key.keysym.sym <= 122 && player->server.length() < 15)
		{
			// adds character to string
			player->server += (char)event.key.keysym.sym;
			data->gfx.drawFont(player->server.data(), 330, 334);
		}
		SDL_UpdateRects(screen, 1, &dest);
	}

	if(namefield)
	{
		dest.x = 330;
		dest.y = 393;
		dest.h = 24;
		dest.w = 240;
		if(event.key.keysym.sym == SDLK_BACKSPACE && player->name.length() > 0)
		{
			player->name = player->name.substr(0,player->name.length()-1);
			//erase old text
			SDL_BlitSurface(m_titleScreen, &dest, screen, &dest);
			data->gfx.drawFont(player->name.data(), 330, 393);
		}
		else if(event.key.keysym.sym >= 46 &&
				event.key.keysym.sym <= 122 && player->name.length() < 10)
		{
			//adds character to string
			player->name += (char)event.key.keysym.sym;
			data->gfx.drawFont(player->name.data(), 330, 393);
		}
		SDL_UpdateRects(screen, 1, &dest);
	}
	return 1;
}

int ClientPhaseSDL_Title::OnConnect()
{
	MessagePlayer *player = getCurPlayer();
	if(!player->name.length() || !player->server.length())
		return 0;

	//Load character image
	char* timg = new char[50];
	snprintf(timg, 49, "./data/gfx/sprites/%d%d_player_small.png",
			player->character +1, player->playerId +1);
	data->playerimg[player->playerId] = IMG_Load(timg);
	if(!data->playerimg[player->playerId]) {
		ERROR << "Failed to load: " << timg << endl;
		delete[] timg;
		return -1;
	}
	delete[] timg;
	//
	// send a PLAYER_SELECT message to the server
	//
	MessageEvent event;
	event.code = MSG_CODE_PLAYER_SELECT;
	m_client.dispatch(event);
	return 1;
}

