
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include <algorithm>
#include "ClientPhaseSDL_Auction.h"
#include "common.h"
#include "State.h"

using namespace std;


//! \brief How many ms between trades between two players
#define TRADE_INTERVAL 1000

#define STATUS_DURATION 2000
#define STATUS_INTERVAL 200

#define ROLE_DURATION 5000
#define ROLE_INTERVAL 50

#define TRADING_DURATION 10000
#define TRADING_INTERVAL 10

#define AUCTION_STORE "data/gfx/auction_store.png"


const char g_statusModeName[MAX_RES_STATUS][16] = {
	"Previous",
	"Usage",
	"Spoilage",
	"Production",
	"Surplus",
	""
};


ClientPhaseSDL_Auction::ClientPhaseSDL_Auction(IClient& client)
	: ClientPhaseSDL(client)
{
	m_auctionScreen = IMG_Load(AUCTION_SCREEN);
	if(!m_auctionScreen) {
		ERROR << "Failed to load: " << AUCTION_SCREEN << endl;
		return;
	}
	m_auctionStore = IMG_Load(AUCTION_STORE);
	if(!m_auctionStore) {
		ERROR << "Failed to load: " << AUCTION_STORE << endl;
		return;
	}
	m_instructionfont = IMG_Load("./data/gfx/fonts/10w.png");
	if(!m_instructionfont) {
		ERROR << "File failed to load: " << "./data/gfx/fonts/10w.png" << endl;
		return;
	}
	m_resourcefont = IMG_Load("./data/gfx/fonts/24br.png");
	if(!m_resourcefont) {
		ERROR << "File failed to load: " << "./data/gfx/fonts/24br.png" << endl;
		return;
	}
	m_timeOfLastTrade = 0;
	m_bResStatusInit = true;

	//
	// make sprite locations dependent on screen dimensions
	//
	m_tradeArea.h = 275;
	m_tradeArea.x = SCREEN_WIDTH / 4;
	m_tradeArea.y = (SCREEN_HEIGHT - m_tradeArea.h) / 2;
	m_tradeArea.w = SCREEN_WIDTH - (m_tradeArea.x * 2);
	m_pxPad = 10;
	m_countArea.w = 20;
	m_countArea.x = SCREEN_WIDTH - m_pxPad*2 - m_countArea.w;
	m_countArea.y = m_tradeArea.y + m_pxPad*2;
	m_countArea.h = m_tradeArea.h - m_pxPad*2;
}

void ClientPhaseSDL_Auction::advanceResource()
{
	switch(m_state.auction->res)
	{
	case RES_FOOD:
	case RES_ENERGY:
	case RES_ORE:
	case RES_OIL:
		m_state.auction->res++;
	default:
		break;
	}
}

void ClientPhaseSDL_Auction::advanceMode()
{
	MessageEvent event;

	switch(m_state.auction->mode)
	{
	case AUCTION_MODE_NONE:
		m_state.auction->mode = AUCTION_MODE_STATUS;
		doResStatusInit();
		break;
	case AUCTION_MODE_STATUS:
		m_state.auction->mode = AUCTION_MODE_ROLE;
		doPlayerRoleInit();
		break;
	case AUCTION_MODE_ROLE:
		m_state.auction->mode = AUCTION_MODE_TRADE;
		doTradingInit();
		break;
	case AUCTION_MODE_TRADE:
		//
		// trading mode is over, advance to next resource
		//
		advanceResource();
		//
		// if we were already on the last resource, then the acution is over
		//
		if(m_state.auction->res >= MAX_RESOURCE) {
			event.code = MSG_CODE_PLAYER_SELECT;
			m_client.dispatch(event);
			return; // don't redraw auction if this is the last reasource 
		}
		m_state.auction->mode = AUCTION_MODE_STATUS;
		doResStatusInit();
		break;
	default:
		break;
	}
}

int ClientPhaseSDL_Auction::open()
{
	int i;
	//
	// Calculate what the store will set its price for each resource
	// to.  This includes any mark-ups.  results stored in:
	// m_state.store->resBuy[]
	// m_state.store->resSell[]
	//
	m_state.calcStorePrice();

	//
	// Initialize pixel locations for each player
	//
	m_pxPlayerWidth = data->playerimg[0]->w / CHARACTER_ANIM;
	m_pxPlayerHeight = data->playerimg[0]->h;
	int pxPlayersSpacing = (m_tradeArea.w - m_pxPlayerWidth - m_pxPad*8)
							/ (m_state.sys->nPlayer-1);
	for(i = 0; i < MAX_PLAYER; i++)
	{
		m_pxPlayerX[i] = m_tradeArea.x + m_pxPad*4 + i * pxPlayersSpacing;
	}

	m_pxBidsY = m_tradeArea.y + m_tradeArea.h + m_pxPlayerHeight + m_pxPad*2;
	m_pxMoneyY = m_pxBidsY + 24 + m_pxPad;
	m_pxUnitsY = m_pxMoneyY + 24 + 5; // font size 24 + padding

	m_state.auction->res = RES_FOOD;
	m_state.auction->mode = AUCTION_MODE_NONE;
	advanceMode();

	return ClientPhaseSDL::open();
}

int ClientPhaseSDL_Auction::close()
{
	return ClientPhaseSDL::close();
}

int ClientPhaseSDL_Auction::execute()
{
	switch(m_state.auction->mode)
	{
	case AUCTION_MODE_STATUS:
		doResStatus();
		break;
	case AUCTION_MODE_ROLE:
		doPlayerRole();
		break;
	case AUCTION_MODE_TRADE:
		doTrading();
		break;
	default:
		break;
	}
	return ClientPhaseSDL::execute();
}

int ClientPhaseSDL_Auction::doFrame()
{
	return ClientPhaseSDL::doFrame();
}

int ClientPhaseSDL_Auction::msgHandler(Message& msg)
{
	return ClientPhaseSDL::msgHandler(msg);
}

int ClientPhaseSDL_Auction::OnEvent(SDL_Event& event)
{
	InputType inputType;
	int playerId;
	bool bStart;
	int arrowKey = -1;
	//
	// resort to mutable input class to translate SDL events into
	// game events.
	//
	inputType = data->m_input.get(event, playerId, bStart);
	switch(inputType)
	{
	case IO_SELECT:
		OnSelect(playerId);
		break;
	case IO_UP:
		arrowKey = ARROW_UP;
		break;
	case IO_DOWN:
		arrowKey = ARROW_DOWN;
		break;
	case IO_QUIT:
		return 0;
	default:
		// ignore
		break;
	}
	if(arrowKey >= 0 && arrowKey < MAX_ARROW)
	{
		if(bStart)
			data->m_bArrowKey[playerId][arrowKey] = true;
		else
			data->m_bArrowKey[playerId][arrowKey] = false;
	}
	return 1;
}

int ClientPhaseSDL_Auction::OnSelect(int playerId)
{
	return 1;
}

void ClientPhaseSDL_Auction::drawCommon()
{
	unsigned int i;
	int res = m_state.auction->res;
	SDL_Rect dest;

	// draw common background of auction
	SDL_BlitSurface(m_auctionScreen, NULL, screen, NULL);

	data->gfx.setFont(m_resourcefont, 24);

	char title[64];
	snprintf(title, sizeof(title), "Status #%d", m_state.sys->round);
	data->gfx.drawFontCentered(title, 15);
	snprintf(title, sizeof(title), "%s", g_resType[res].name.c_str());
	data->gfx.drawFontCentered(title, 42);

	dest.x = m_pxPad*4;
	dest.y = m_pxPad*4;
	dest.h = data->m_pPlotRes[res]->h;
	dest.w = data->m_pPlotRes[res]->w;
	SDL_BlitSurface(data->m_pPlotRes[res], NULL, screen, &dest);
	dest.x = SCREEN_WIDTH - m_pxPad*4 - dest.w;
	SDL_BlitSurface(data->m_pPlotRes[res], NULL, screen, &dest);

	for(i = 0; i < MAX_PLAYER; i++)
	{
		MessagePlayer *pPlayer = m_state.player[i];

		if(m_state.auction->playerBuying[i] == false)
			pPlayer->pos_y = m_tradeArea.y - m_pxPlayerHeight - m_pxPad;
		else
			pPlayer->pos_y = m_tradeArea.y + m_tradeArea.h + m_pxPad;
		pPlayer->pos_x = m_pxPlayerX[i];
		pPlayer->direction = 2;

		draw_player(pPlayer, data->playerimg[i]);
	}

	dest.x = m_tradeArea.x - m_auctionStore->w - m_pxPad;
	dest.y = m_tradeArea.y + m_tradeArea.h - m_auctionStore->h;
	dest.w = m_auctionStore->w;
	dest.h = m_auctionStore->h;
	SDL_BlitSurface(m_auctionStore, NULL, screen, &dest);
	data->gfx.drawFont(m_state.store->resBuy[res], dest.x,
						dest.y + m_auctionStore->h + m_pxPad);

	if(m_state.store->res[res])
	{
		//
		// only draw the selling store if it has this resource for sale
		//
		dest.x = m_tradeArea.x + m_tradeArea.w + m_pxPad;
		dest.y = m_tradeArea.y - m_auctionStore->h;
		SDL_BlitSurface(m_auctionStore, NULL, screen, &dest);
		data->gfx.drawFont(m_state.store->resSell[res], dest.x,
						dest.y - m_pxPad - 24);
	}

	data->gfx.drawFont("MONEY:", m_tradeArea.x - 144 - m_pxPad, m_pxMoneyY);
	data->gfx.drawFont("UNITS:", m_tradeArea.x - 144 - m_pxPad, m_pxUnitsY);
	data->gfx.drawFont(":MONEY", m_tradeArea.x + m_tradeArea.w + m_pxPad,
						m_pxMoneyY);
	data->gfx.drawFont(":UNITS", m_tradeArea.x + m_tradeArea.w + m_pxPad,
						m_pxUnitsY);

	for(i = 0; i < m_state.sys->nPlayer; i++)
	{
		data->gfx.setFont(data->playersmallfont[i], 10);
		data->gfx.drawFontRightJustify(m_state.player[i]->money,
						m_pxPlayerX[i] + m_pxPlayerWidth + 10,
						m_pxMoneyY + 10);
	}
}

void ClientPhaseSDL_Auction::drawCountdown(int duration)
{
	// normalized time for this duration
	float f = (float)(m_timeLast - m_timeStarted) / duration;
	int pxHeight = (int)(f * m_countArea.h);
	//
	// erase top most portion of the countdown bar
	//
	SDL_Rect dest = m_countArea;
	dest.h = pxHeight;
	SDL_BlitSurface(m_auctionScreen, &dest, screen, &dest);
	SDL_UpdateRects(screen, 1, &dest);
}

void ClientPhaseSDL_Auction::drawPlayerBid(MessagePlayer *pPlayer)
{
	SDL_Rect dest;
	int i = pPlayer->playerId;

	//
	// erase previous bid value with background
	//
	dest.x = m_pxPlayerX[i] - 48;
	dest.y = m_pxBidsY;
	dest.w = 96;
	dest.h = 24;
	SDL_BlitSurface(m_auctionScreen, &dest, screen, &dest);

	if(m_state.auction->playerBid[i] > 0)
	{
		data->gfx.setFont(data->playerbigfont[i], 24);
		data->gfx.drawFontRightJustify(m_state.auction->playerBid[i],
						m_pxPlayerX[i] + m_pxPlayerWidth + 10,
						m_pxBidsY);
	}

	SDL_UpdateRects(screen, 1, &dest);
}

void ClientPhaseSDL_Auction::drawPlayerStats(MessagePlayer *pPlayer)
{
	SDL_Rect dest;
	int res = m_state.auction->res;
	int i = pPlayer->playerId;

	//
	// erase previous bid value with background
	//
	dest.x = m_pxPlayerX[i] - 48;
	dest.y = m_pxMoneyY;
	dest.w = 96;
	dest.h = 34 + m_pxPad*2;
	SDL_BlitSurface(m_auctionScreen, &dest, screen, &dest);

	data->gfx.setFont(data->playersmallfont[i], 10);
	data->gfx.drawFontRightJustify(pPlayer->money,
					m_pxPlayerX[i] + m_pxPlayerWidth + 10,
					m_pxMoneyY + 10);

	data->gfx.setFont(data->playerbigfont[i], 24);
	data->gfx.drawFontRightJustify(pPlayer->resCur(res),
					m_pxPlayerX[i] + m_pxPlayerWidth + 10,
					m_pxUnitsY);

	SDL_UpdateRects(screen, 1, &dest);
}

void ClientPhaseSDL_Auction::doResStatusInit()
{
	unsigned int i;
	for(i = 0; i < m_state.sys->nPlayer; i++)
		m_state.auction->playerBuying[i] = true;

	drawCommon();
	//
	// update entire screen
	//
	SDL_UpdateRect(screen, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	//
	// start by showing the user their previous resource amount
	//
	m_resStatusMode = RES_STATUS_PREV;
	//
	// start counting time for bar-graph animations
	//
	timeReset();
}


void ClientPhaseSDL_Auction::advanceResStatus()
{
	int res = m_state.auction->res;

	switch(m_resStatusMode)
	{
	case RES_STATUS_PREV:
	case RES_STATUS_USE:
	case RES_STATUS_SPOIL:
	case RES_STATUS_PROD:
	case RES_STATUS_SURPLUS:

		m_resStatusMode++;

		//
		// couldn't get the 'need' value to show properly, so I'm
		// hacking it to skip the surplus mode...
		//
		if(m_resStatusMode == RES_STATUS_SURPLUS)
			m_resStatusMode = RES_STATUS_STORE; // skip surplus mode

		if(res == RES_ORE || res == RES_OIL)
		{
			// Oil and ore don't get "used" or spoil, so skip these modes
			if(m_resStatusMode == RES_STATUS_USE)
				m_resStatusMode = RES_STATUS_PROD;
			if(m_resStatusMode == RES_STATUS_SURPLUS)
				m_resStatusMode = RES_STATUS_STORE; // skip surplus mode
		}
		timeReset();
		m_bResStatusInit = true; // tells it to draw text/icons
		break;
	case RES_STATUS_STORE:
		m_resStatusMode = RES_STATUS_PREV;
		m_state.updatePlayerResource(res); // modify prev[] with resCur()
		advanceMode();
		break;
	default:
		break;
	}
}

void ClientPhaseSDL_Auction::doResStatus()
{
	unsigned int i;
	int res = m_state.auction->res;
	SDL_Rect dest;

	if(!timePassedInterval(STATUS_INTERVAL))
		return;
	if(timePassedDuration(STATUS_DURATION)) {
		advanceResStatus();
		return; // bar-graph animation complete
	}
	if(!m_bResStatusInit)
		return; // no animation

	// erase any text with original background
	dest.x = 0;
	dest.y = SCREEN_HEIGHT - 24 - 30;
	dest.w = SCREEN_WIDTH;
	dest.h = 24;
	SDL_BlitSurface(m_auctionScreen, &dest, screen, &dest);

	data->gfx.setFont(m_resourcefont, 24);
	if(m_resStatusMode == RES_STATUS_STORE)
	{
		char str[64];
		snprintf(str,sizeof(str),"Store has %d units",m_state.store->res[res]);
		data->gfx.drawFontCentered(str, SCREEN_HEIGHT - 24 - 30);
	}
	else
		data->gfx.drawFontCentered(g_statusModeName[m_resStatusMode],
									SCREEN_HEIGHT - 24 - 30);
	SDL_UpdateRects(screen, 1, &dest);

	//
	// erase previous unit values with background
	//
	dest.x = m_tradeArea.x;
	dest.y = m_pxUnitsY;
	dest.w = m_tradeArea.w;
	dest.h = 24;
	SDL_BlitSurface(m_auctionScreen, &dest, screen, &dest);

	bool bDrawNeedLines = false;

	for(i = 0; i < m_state.sys->nPlayer; i++)
	{
		MessagePlayer *pPlayer = m_state.player[i];

		switch(m_resStatusMode)
		{
		case RES_STATUS_PREV:
			m_statusBarCur[i] = pPlayer->prev[res];
			break;
		case RES_STATUS_USE:
			m_statusBarCur[i] -= pPlayer->use[res];
			break;
		case RES_STATUS_SPOIL:
			m_statusBarCur[i] -= pPlayer->spoil[res];
			break;
		case RES_STATUS_PROD:
			m_statusBarCur[i] += pPlayer->prod[res];
			break;
		case RES_STATUS_SURPLUS:
			//
			// The 'need' lines must be drawn, and the difference in
			// resource amount from the need value whould be printed.
			//
			m_statusBarCur[i] = pPlayer->resCur(res);
			bDrawNeedLines = true;
			break;
		case RES_STATUS_STORE:
			// already printed out how much res the store has above
			m_statusBarCur[i] = pPlayer->resCur(res);
			return;
		default:
			return;
		}

		data->gfx.setFont(data->playerbigfont[i], 24);
		data->gfx.drawFontRightJustify(m_statusBarCur[i],
						m_pxPlayerX[i] + m_pxPlayerWidth + 10,
						m_pxUnitsY);
	}
	//
	// draw unit values
	//
	SDL_UpdateRects(screen, 1, &dest);

	if(m_resStatusMode >= RES_STATUS_PREV && m_resStatusMode <= RES_STATUS_PROD)
	{
		SDL_Rect rect[MAX_PLAYER];
		Uint32 color = SDL_MapRGB(screen->format, 200, 200, 200);
		Uint32 dot_color = SDL_MapRGB(screen->format, 125, 125, 125);

		for(i = 0; i < m_state.sys->nPlayer; i++)
		{
			MessagePlayer *pPlayer = m_state.player[i];

			//
			// normalize to trading area
			//
			float f = (float)m_statusBarCur[i] / g_playerResMax[res];
			if(f > 1.0f)
				f = 1.0f;
			int pxHeight = (int)(f * m_tradeArea.h);
			int y = m_tradeArea.y + m_tradeArea.h - pxHeight;
			//
			// erase current bar graph
			//
			rect[i].x = m_pxPlayerX[i];
			rect[i].y = m_tradeArea.y;
			rect[i].w = m_pxPlayerWidth;
			rect[i].h = m_tradeArea.h;
			SDL_BlitSurface(m_auctionScreen, &rect[i], screen, &rect[i]);
			//
			// draw new bar graph height
			//
			dest.x = m_pxPlayerX[i];
			dest.y = y;
			dest.w = m_pxPlayerWidth;
			dest.h = pxHeight;
			data->gfx.drawFilledRect(&dest, color);
			//
			// draw dotted line at the pPlayer->need[] height
			//
			if(bDrawNeedLines)
			{
				f = (float)pPlayer->need[res] / g_playerResMax[res];
				if(f > 1.0f)
					f = 1.0f;
				pxHeight = (int)(f * m_tradeArea.h);
				y = m_tradeArea.y + m_tradeArea.h - pxHeight;

				rect[i].x = m_pxPlayerX[i] - m_pxPad;
				rect[i].w = m_pxPlayerWidth + m_pxPad*2;
				data->gfx.drawDottedLine(rect[i].x, y, rect[i].x + rect[i].w, y,
										2, 8, 2, dot_color);
			}
		}

		SDL_UpdateRects(screen, m_state.sys->nPlayer, rect);
	}

	m_bResStatusInit = false;
}

void ClientPhaseSDL_Auction::doPlayerRoleInit()
{
	drawCommon();
	//
	// draw a full countdown bar on the right
	//
	Uint32 color = SDL_MapRGB(screen->format, 200, 200, 200);
	data->gfx.drawFilledRect(&m_countArea, color);
	//SDL_UpdateRects(screen, 1, &m_countArea);

	//
	// write "BUY" below each of the players
	//
	unsigned int i;
	for(i = 0; i < m_state.sys->nPlayer; i++)
	{
		drawPlayerStats(m_state.player[i]);

		m_state.auction->playerBuying[i] = true;
		data->gfx.setFont(data->playerbigfont[i], 24);
		data->gfx.drawFont("BUY", m_pxPlayerX[i] - 24,
				m_tradeArea.y + m_tradeArea.h + m_pxPlayerHeight + 2*m_pxPad);
	}
	//
	// update entire screen
	//
	SDL_UpdateRect(screen, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
	//
	// start the countdown
	//
	timeReset();
}

void ClientPhaseSDL_Auction::doPlayerRole()
{
	SDL_Rect dest;

	if(!timePassedInterval(ROLE_INTERVAL))
		return;
	if(timePassedDuration(ROLE_DURATION)) {
		advanceMode();
		return; // bar-graph animation complete
	}
	//
	// check for player up/down, and update sprites
	//
	unsigned int i, j;
	for(j = 0; j < m_state.sys->nPlayer; j++)
	for(i = 0; i < MAX_ARROW; i++)
	{
		if(!data->m_bArrowKey[j][i])
			continue;
		MessagePlayer *pPlayer = m_state.player[j];
		//
		// this arrow key is depressed.. awww
		//
		switch(i) // only interested in up/down events
		{
		case ARROW_UP:
			if(m_state.auction->playerBuying[j] == false)
				break; // already selling
			m_state.auction->playerBuying[j] = false;
			//
			// erase buyer, and blit at seller position
			//
			dest.x = m_pxPlayerX[j] - 24; // erase the "BUY" too
			dest.y = m_tradeArea.y + m_tradeArea.h + m_pxPad;
			dest.w = strlen("BUY") * 24; // should be > than playerWidth
			dest.h = m_pxPlayerHeight + 24 + m_pxPad;
			SDL_BlitSurface(m_auctionScreen, &dest, screen, &dest);
			SDL_UpdateRects(screen, 1, &dest);

			pPlayer->pos_x = m_pxPlayerX[j];
			pPlayer->pos_y = m_tradeArea.y - m_pxPlayerHeight - m_pxPad;
			pPlayer->direction = 2;
			draw_player(pPlayer, data->playerimg[j]);

			dest.x = m_pxPlayerX[j] - 36;
			dest.y = m_tradeArea.y - m_pxPlayerHeight - 24 - 2*m_pxPad;
			dest.w = strlen("SELL") * 24;
			dest.h = 24;
			data->gfx.setFont(data->playerbigfont[j], 24);
			data->gfx.drawFont("SELL", dest.x, dest.y);
			SDL_UpdateRects(screen, 1, &dest);
			break;
		case ARROW_DOWN:
			if(m_state.auction->playerBuying[j])
				break; // already buying
			m_state.auction->playerBuying[j] = true;
			//
			// erase seller, and blit at seller position
			//
			dest.x = m_pxPlayerX[j] - 36; // erase the "SELL" too
			dest.y = m_tradeArea.y - m_pxPlayerHeight - 24 - 2*m_pxPad;
			dest.w = strlen("SELL") * 24; // should be > than playerWidth
			dest.h = m_pxPlayerHeight + 24 + 2*m_pxPad;
			SDL_BlitSurface(m_auctionScreen, &dest, screen, &dest);
			SDL_UpdateRects(screen, 1, &dest);

			pPlayer->pos_x = m_pxPlayerX[j];
			pPlayer->pos_y = m_tradeArea.y + m_tradeArea.h + m_pxPad;
			pPlayer->direction = 2;
			draw_player(pPlayer, data->playerimg[j]);

			dest.x = m_pxPlayerX[j] - 24;
			dest.y = m_tradeArea.y + m_tradeArea.h + m_pxPlayerHeight+2*m_pxPad;
			dest.w = strlen("BUY") * 24;
			dest.h = 24;
			data->gfx.setFont(data->playerbigfont[j], 24);
			data->gfx.drawFont("BUY", dest.x, dest.y);
			SDL_UpdateRects(screen, 1, &dest);
			break;
		default:
			break;
		}
	}
	//
	// draw the countdown bar on the right
	//
	drawCountdown(ROLE_DURATION);
}

int ClientPhaseSDL_Auction::height2bid(int y)
{
	int res = m_state.auction->res;
	//
	// use inverse normalized vertical position to determine bid price
	//
	float f = 1.0f - ((float)(y - m_tradeArea.y) / m_tradeArea.h);
	int bid = m_state.store->resBuy[res];
	bid += (int)(f * (m_state.store->resSell[res]-m_state.store->resBuy[res]));
	return bid;
}

void ClientPhaseSDL_Auction::calcBids(MessagePlayer *pPlayer)
{
	int i = pPlayer->playerId;
	int y;

	if(m_state.auction->playerBuying[i])
	{
		y = pPlayer->pos_y; // using top of sprite

		if(y > m_tradeArea.y + m_tradeArea.h) {
			m_state.auction->playerBid[i] = -1;
			return; // player not bidding
		}
		if(y == m_tradeArea.y) {
			//
			// special case if the store is out of units, player can
			// bid higher and higher
			//
			return; // don't modify the bid value
		}
	}
	else // selling
	{
		y = pPlayer->pos_y + m_pxPlayerHeight; // using bottom of sprite

		if(y < m_tradeArea.y) {
			m_state.auction->playerBid[i] = -1;
			return; // player not bidding
		}
	}
	m_state.auction->playerBid[i] = height2bid(y);
}

void ClientPhaseSDL_Auction::updateBidders(MessagePlayer *pPlayer)
{
	unsigned int i;
	SDL_Rect dest;

	//
	// calculate each players bid based on their vertical location
	//
	calcBids(pPlayer);

	int oldMinBuyer = m_minBuyer;
	int oldMaxSeller = m_maxSeller;

	m_minBuyer = MAX_INT;
	m_maxSeller = MIN_INT;

	for(i = 0; i < m_state.sys->nPlayer; i++)
	{
		MessagePlayer *pPlayer = m_state.player[i];

		if(m_state.auction->playerBid[i] < 0)
			continue; // player isn't bidding yet

		if(m_state.auction->playerBuying[i])
		{
			if(pPlayer->pos_y < m_minBuyer)
				m_minBuyer = pPlayer->pos_y;
		}
		else // selling
		{
			if(pPlayer->pos_y + m_pxPlayerHeight > m_maxSeller)
				m_maxSeller = pPlayer->pos_y + m_pxPlayerHeight;
		}
	}

	//
	// erase highest buyer line
	//
	dest = m_tradeArea;
	dest.y = oldMinBuyer-2;
	dest.h = 2;
	SDL_BlitSurface(m_auctionScreen, &dest, screen, &dest);

	if(m_minBuyer != MAX_INT && m_minBuyer < m_tradeArea.y + m_tradeArea.h)
	{
		//
		// draw new highest buyer line
		//
		Uint32 color = SDL_MapRGB(screen->format, 100, 100, 100);
		data->gfx.drawDottedLine(dest.x, m_minBuyer-2, dest.x+dest.w,
								m_minBuyer-2, 2, 8, 2, color);
	}
	SDL_UpdateRects(screen, 1, &dest);

	//
	// erase highest buyer line
	//
	dest = m_tradeArea;
	dest.y = oldMaxSeller;
	dest.h = 2;
	SDL_BlitSurface(m_auctionScreen, &dest, screen, &dest);

	if(m_maxSeller != MIN_INT && m_maxSeller > m_tradeArea.y)
	{
		//
		// draw new highest buyer line
		//
		Uint32 color = SDL_MapRGB(screen->format, 100, 100, 100);
		data->gfx.drawDottedLine(dest.x, m_maxSeller, dest.x+dest.w,
								m_maxSeller, 2, 8, 2, color);
	}
	SDL_UpdateRects(screen, 1, &dest);
	

	if(m_minBuyer == MAX_INT && m_maxSeller == MIN_INT)
		return; // no bidding is occuring

	//
	// add new players to the trading queues if they are min/max
	// TODO: this doesn't seem SDL-specific.. move to base class
	//
	deque<MessagePlayer*>::iterator iter;

	for(i = 0; i < m_state.sys->nPlayer; i++)
	{
		MessagePlayer *p = m_state.player[i];

		if(m_state.auction->playerBid[i] < 0)
			continue; // player isn't bidding yet

		if(m_state.auction->playerBuying[i])
		{
			if(p->pos_y == m_minBuyer)
			{
				iter = find(highest_buyer.begin(), highest_buyer.end(), p);
				if(iter == highest_buyer.end())
					highest_buyer.push_back(p);
			}
		}
		else // selling
		{
			if(p->pos_y + m_pxPlayerHeight == m_maxSeller)
			{
				iter = find(lowest_seller.begin(), lowest_seller.end(), p);
				if(iter == lowest_seller.end())
					lowest_seller.push_back(p);
			}
		}
	}
	//
	// remove any players from trading queues if they aren't min/max
	//
	iter = lowest_seller.begin();
	for(; iter != lowest_seller.end(); ++iter)
	{
		MessagePlayer *p = *iter;

		if(p->pos_y + m_pxPlayerHeight < m_maxSeller) {
			iter = lowest_seller.erase(iter);
			if(iter == lowest_seller.end())
				break;
		}
	}
	iter = highest_buyer.begin();
	for(; iter != highest_buyer.end(); ++iter)
	{
		MessagePlayer *p = *iter;

		if(p->pos_y > m_minBuyer) {
			iter = highest_buyer.erase(iter);
			if(iter == highest_buyer.end())
				break;
		}
	}

}

void ClientPhaseSDL_Auction::doTradingInit()
{
	drawCommon();
	//
	// draw a full countdown bar on the right
	//
	Uint32 color = SDL_MapRGB(screen->format, 200, 200, 200);
	data->gfx.drawFilledRect(&m_countArea, color);
	//
	// update entire screen
	//
	SDL_UpdateRect(screen, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);

	//
	// clear all trading queues of players
	//
	lowest_seller.clear();
	highest_buyer.clear();

	m_minBuyer = SCREEN_HEIGHT;
	m_maxSeller = 0;

	unsigned int i;
	for(i = 0; i < m_state.sys->nPlayer; i++) {
		m_state.auction->playerBid[i] = -1; // not bidding yet
		drawPlayerStats(m_state.player[i]);
	}

	//
	// start the countdown
	//
	timeReset();
}

void ClientPhaseSDL_Auction::doTrading()
{
	int res = m_state.auction->res;
	
	if(!timePassedInterval(TRADING_INTERVAL))
		return;
	if(timePassedDuration(TRADING_DURATION)) {
		advanceMode(); // TODO: pause for all player buttons
		return; // bar-graph animation complete
	}
	//
	// draw the countdown bar on the right
	//
	drawCountdown(TRADING_DURATION);

	//
	// check for player up/down, and update sprites
	//
	unsigned int i, j;
	for(j = 0; j < m_state.sys->nPlayer; j++)
	for(i = 0; i < MAX_ARROW; i++)
	{
		if(!data->m_bArrowKey[j][i])
			continue;
		MessagePlayer *pPlayer = m_state.player[j];
		bool bBuying = m_state.auction->playerBuying[j];
		int delta;
		int new_y;
		int bid;
		bool bIgnoreMovement = false; // true if buyer is above trading area

		switch(i) // only interested in up/down events
		{
		case ARROW_UP:
		case ARROW_DOWN:
			if(i == ARROW_UP)
				delta = -1; // reverse direction
			else
				delta = 1;

			new_y = pPlayer->pos_y + delta;

			bid = height2bid(new_y);
			if(bid < 0)
				bid = 0;

			if(bBuying)
			{
				if(new_y > m_tradeArea.y + m_tradeArea.h + m_pxPad)
					break;
				if(pPlayer->money < (unsigned int)bid)
					break; // not enough money to make this bid
				if(lowest_seller.size()) {
					if(new_y < lowest_seller.front()->pos_y + m_pxPlayerHeight)
						break;
				}
				if(new_y < m_tradeArea.y)
				{
					if(m_state.store->res[res] > 0)
						break;
					//
					// store is out of the resource. we have to keep
					// increasing the buyer's bid price
					//
					bIgnoreMovement = true;
				}
			}
			else
			{
				if(new_y < m_tradeArea.y - m_pxPlayerHeight - m_pxPad)
					break;
				if(highest_buyer.size()) {
					if(new_y + m_pxPlayerHeight > highest_buyer.front()->pos_y)
						break;
				}
				if(new_y + m_pxPlayerHeight > m_tradeArea.y + m_tradeArea.h)
					break;
				if(pPlayer->resCur(res) == 0)
					break; // seller ran out of resource
			}

			erase_player(pPlayer, m_auctionScreen);
			if(!bIgnoreMovement)
				pPlayer->pos_y = new_y;
			draw_player(pPlayer, data->playerimg[j]);

			//
			// this player might be in the position to trade.
			// Add/Remove player from trading queues if necessary
			//
			updateBidders(pPlayer);
			if(!bIgnoreMovement)
			{
				//
				// this only happens when the buyer is climbing off
				// the top of the trading area and the store is dry.
				//
				// TODO: this messes up the whole queue system, which
				// is pixel based.  The queue is going to be out of synch
				// when players are climbing above the trading area..
				//
				m_state.auction->playerBid[pPlayer->playerId] = bid;
			}
			drawPlayerBid(pPlayer); // draws the numeric monetary value
			break;
		default:
			break;
		}
	}

	Uint32 dT = m_timeLast - m_timeOfLastTrade;
	if(dT < TRADE_INTERVAL)
		return; // not time to trade yet
	m_timeOfLastTrade = m_timeLast;

	//
	// PERFORM ANY TRADING FOR ALL PLAYERS
	//

	MessagePlayer *buyer = NULL;
	MessagePlayer *seller = NULL;
	if(highest_buyer.size())
		buyer = highest_buyer.front();
	if(lowest_seller.size())
		seller = lowest_seller.front();

	if(buyer == NULL && seller == NULL)
		return; // no trading activity

	unsigned int bid;
	if(buyer != NULL)
		bid = m_state.auction->playerBid[buyer->playerId];
	else
		bid = m_state.auction->playerBid[seller->playerId];

	if(seller != NULL && seller->resCur(res) <= 0) {
		//
		// seller has run dry.. put it back at the starting line.
		//
		erase_player(seller, m_auctionScreen);
		seller->pos_y = m_tradeArea.y - m_pxPlayerHeight - m_pxPad;
		draw_player(seller, data->playerimg[seller->playerId]);
		updateBidders(seller); // this should deque the seller
		drawPlayerBid(seller);
	}
	if(buyer != NULL && buyer->money < bid)
	{
		//
		// buyer can't afford this bid.. put it back at starting line.
		//
		erase_player(buyer, m_auctionScreen);
		buyer->pos_y = m_tradeArea.y + m_tradeArea.h + m_pxPad;
		draw_player(buyer, data->playerimg[buyer->playerId]);
		updateBidders(buyer); // this should deque the seller
		drawPlayerBid(buyer);
	}
	//
	// perform buy from the store if there is a buyer at that position
	//
	while(buyer != NULL && buyer->pos_y == m_tradeArea.y)
	{
		if(m_state.store->res[res] == 0)
			break; // store has none of this resource

		buyer->money -= m_state.store->resSell[res];
		m_state.store->res[res]--;
		buyer->prev[res]++;

		updateBidders(buyer);
		drawPlayerStats(buyer);
		return; // only trade with store, not other players
	}
	//
	// perform sell to the store if there is a seller at that position
	//
	while(seller != NULL && seller->pos_y+m_pxPlayerHeight ==
							m_tradeArea.y + m_tradeArea.h)
	{
		seller->money += m_state.store->resBuy[res];
		m_state.store->res[res]++;
		seller->prev[res]--;

		updateBidders(seller);
		drawPlayerStats(seller);
		return; // only trade with store, not other players
	}

	//
	// perform any trades if two players are on the same bid price,
	// and the trading time interval is right.
	//
	while(seller != NULL && buyer != NULL)
	{

		if(seller->pos_y + m_pxPlayerHeight != buyer->pos_y)
			break; // not agreeing on price to trade at
		//
		// perform trade, if possible
		//
		seller->money += bid;
		buyer->money -= bid;
		seller->prev[res]--;
		buyer->prev[res]++;

		updateBidders(buyer);
		updateBidders(seller);
		drawPlayerStats(buyer);
		drawPlayerStats(seller);

		break; // hacked use of control loop for access to 'break'
	}

}

void ClientPhaseSDL_Auction::timeReset()
{
	m_timeStarted = SDL_GetTicks(); // start the new animation
	m_timeElapsed = 0;
	m_timeLast = m_timeStarted;
}

bool ClientPhaseSDL_Auction::timePassedInterval(Uint32 interval)
{
	Uint32 t = SDL_GetTicks();

	m_timeElapsed += t - m_timeLast;
	m_timeLast = t;

	if(m_timeElapsed < interval)
		return false; // not time for frame redraw

	m_timeElapsed -= interval;
	return true;
}

bool ClientPhaseSDL_Auction::timePassedDuration(Uint32 duration)
{
	if(m_timeLast - m_timeStarted > duration)
		return true;
	return false;
}

