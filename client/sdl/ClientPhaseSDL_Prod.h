
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#ifndef CLIENT_PHASE_SDL_PROD_H
#define CLIENT_PHASE_SDL_PROD_H

#include "ClientPhaseSDL.h"

class ClientPhaseSDL_Prod : public ClientPhaseSDL
{
public:
	ClientPhaseSDL_Prod(IClient& client);

	int open();
	int close();
	int execute();
	int doFrame();
	int msgHandler(Message& msg);

	int OnEvent(SDL_Event& event);
	Uint32 OnTimerEvent(Uint32 interval);
protected:
	int OnSelect(int playerId);
	int OnEndProduction();
	int updateProduction();

    SDL_Surface *m_instructionfont;
    SDL_Surface *m_pProd;
	SDL_TimerID m_timerID;
	Uint32 m_timeElapsed;
	
	int m_plotProduced[PLOT_NX][PLOT_NY];
	int m_nDot; // number of resource dots
	int m_iDot; // current count of resource dots
	float m_nDotPerTimer;
	float m_nDotRender;
};

#endif

