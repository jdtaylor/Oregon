
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#ifndef CLIENT_PHASE_SDL_AUCTION_H
#define CLIENT_PHASE_SDL_AUCTION_H

#include <queue>
#include "ClientPhaseSDL.h"
#include "Input.h"
#include "Messages.h"

//!
//	\brief Resource Status Mode
//
//	These are the modes the game cycles through with bar graphs at the
//	beginning of each resource type.
//
typedef enum _resStatusMode
{
	RES_STATUS_PREV = 0,
	RES_STATUS_USE,
	RES_STATUS_SPOIL,
	RES_STATUS_PROD,
	RES_STATUS_SURPLUS, //!< how much over or under "need" value
	RES_STATUS_STORE, //!< eg. "Store has 10 units"
	MAX_RES_STATUS
} resStatusMode;

extern const char g_statusModeName[MAX_RES_STATUS][16];


class ClientPhaseSDL_Auction : public ClientPhaseSDL
{
public:
	ClientPhaseSDL_Auction(IClient& client);

	int open();
	int close();
	int execute();
	int doFrame();
	int initialize();
	int calculate_resources();
	int msgHandler(Message& msg);
	int OnEvent(SDL_Event& event);
	
protected:
	int OnSelect(int playerId);

	void advanceResource();
	void advanceMode();
	void advanceResStatus();

	void drawCommon(); //!< blits things common to all modes in auction
	void drawCountdown(int duration); //!< updates the timer bar on right
	void drawPlayerBid(MessagePlayer *pPlayer);
	void drawPlayerStats(MessagePlayer *pPlayer);

	//
	// These 'Init' methods are called once when setting up the mode.
	// Their counterparts are called repetitively from execute()
	//
	void doResStatusInit();
	void doResStatus(); //!< drawing for the resource status mode
	void doPlayerRoleInit();
	void doPlayerRole(); //!< drawing for the player buy/sell mode
	void doTradingInit();
	void doTrading(); //!< drawing for the trading mode

	bool m_bResStatusInit;
	int m_resStatusMode; //!< what status (bar-graph) is showing
	Uint32 m_timeStarted; //!< share single variable for start of timers
	Uint32 m_timeElapsed; //!< accumulates time intervals for frame redraws
	Uint32 m_timeLast; //!< time of last ms  accumulation
	Uint32 m_timeOfLastTrade;
	void timeReset();
	bool timePassedInterval(Uint32 interval);
	bool timePassedDuration(Uint32 duration);

	SDL_Surface *m_instructionfont;
	SDL_Surface *m_resourcefont;
	SDL_Surface *m_auctionScreen;
	SDL_Surface *m_auctionStore;

	int height2bid(int y); //!< turns verticle pixel location into bid value
	void calcBids(MessagePlayer *pPlayer);
	 //!< synchronizes the players with the trading queues
	void updateBidders(MessagePlayer *pPlayer);
	std::deque<MessagePlayer*> lowest_seller;
	std::deque<MessagePlayer*> highest_buyer;
	int m_minBuyer; //!< minimum pixel location of all buyers
	int m_maxSeller; //!< maximum pixel location of all sellers

	SDL_Rect m_tradeArea; //!< bounding pixel locations of the trading area
	SDL_Rect m_countArea; //!< the countdown bar on the right
	int m_pxPlayerWidth;
	int m_pxPlayerHeight;
	int m_pxPlayerX[MAX_PLAYER];
	int m_pxBidsY;
	int m_pxMoneyY;
	int m_pxUnitsY;
	int m_pxPad;

	int m_statusBarCur[MAX_PLAYER]; //!< current height of bar-graph
};

#endif

