
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#ifndef CLIENT_PHASE_SDL_H
#define CLIENT_PHASE_SDL_H

#include "SDL.h"
#include "SDL_image.h"
#include "ClientPhase.h"
#include "SharedData.h"


#define SCREEN_WIDTH 798
#define SCREEN_HEIGHT 675

#define PROG_ICON "./data/gfx/icon.png"

#define TITLE_SCREEN "./data/gfx/title.png"
#define AUCTION_SCREEN "./data/gfx/auction.png"
#define CHARACTER1 "./data/gfx/sprites/1_player_big.png"
#define CHARACTER2 "./data/gfx/sprites/2_player_big.png"
#define CHARACTER3 "./data/gfx/sprites/3_player_big.png"
#define CHARACTER_ANIM 8

#define TILE_DIRT "./data/gfx/map/basic/dirt.png"
#define TILE_GRASS "./data/gfx/map/basic/grass.png"
#define TILE_WATER "./data/gfx/map/basic/water.png"
#define TILE_STORE "./data/gfx/map/basic/store.png"
#define TILE_MOUNT "./data/gfx/map/basic/mountain.png"


class ClientPhaseSDL : public ClientPhase
{
public:
	ClientPhaseSDL(IClient& client);
	virtual ~ClientPhaseSDL();

	void setScreen(SDL_Surface *pScreen) { screen = pScreen; }
	void setSharedData(SharedData *pData) { data = pData; }

	virtual int open();
	virtual int close();
	virtual int execute();
	virtual int doFrame();
	virtual int msgHandler(Message& msg);

	virtual int OnEvent(SDL_Event& event);

protected:
	int get_plot(MessagePlayer *pPlayer, int plots[4][2]);
    int draw_player(MessagePlayer *pPlayer, SDL_Surface *surface);
    int erase_player(MessagePlayer *pPlayer, SDL_Surface *surface);
	int draw_mule(MessagePlayer *pPlayer, SDL_Surface *muleimage);

	SharedData *data;
	SDL_Surface *screen;
	int UpdatePlayerInfoBar();
};

#endif

