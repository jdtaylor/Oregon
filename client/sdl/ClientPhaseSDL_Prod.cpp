
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackyr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include <stdlib.h>
#include <time.h>
#include "ClientPhaseSDL_Prod.h"
#include "common.h"

using namespace std;

//
// set production phase to last for 10 seconds
//
#define PROD_TIME 10000
//
// time increment to call the dot drawing routine
//
#define PROD_INCR 500

static Uint32 _OnTimerEvent(Uint32 interval, void* param)
{
	ClientPhaseSDL_Prod* pSelf = (ClientPhaseSDL_Prod*)param;
	return pSelf->OnTimerEvent(interval);
}

ClientPhaseSDL_Prod::ClientPhaseSDL_Prod(IClient& client)
	: ClientPhaseSDL(client)
{
    m_instructionfont = IMG_Load("./data/gfx/fonts/10w.png");
	if(!m_instructionfont) {
		ERROR << "Failed to load: data/gfx/fonts/10w.png" << endl;
		return;
	}
	m_timerID = NULL;
}

int ClientPhaseSDL_Prod::open()
{
	SDL_Rect dest;

	data->gfx.setFont(m_instructionfont, 10);
	data->gfx.drawFont("Please Wait For Production To Complete", 10, 580);

	if(data != NULL)
		data->blitMap(screen);
	dest.x = 0;
	dest.y = 0;
	dest.w = SCREEN_WIDTH;
	dest.h = SCREEN_HEIGHT;
	SDL_UpdateRects(screen, 1, &dest);

	//
	// calculate the prod[], use[], spoil[], but leave prev[] alone
	// because the auction needs the value to display to the user.
	//
	m_state.calcProduction();

	//
	// reset the "dots" to all be zero.  as the timer goes, the values in
	// m_plotProduced will increment until they reach m_state.plotProduced.
	//
	memset(m_plotProduced, 0, sizeof(m_plotProduced));
	//
	// find out how many dots to render per timer event
	//
	m_nDot = 0;
	int x, y;
	for(x = 0; x < PLOT_NX; x++)
	for(y = 0; y < PLOT_NY; y++)
		m_nDot += m_state.plotProduced[x][y];

	m_nDotPerTimer = (float)m_nDot / (PROD_TIME / PROD_INCR);
	m_nDotRender = 0.0f;
	m_iDot = 0;

	m_timeElapsed = 0;
	m_timerID = SDL_AddTimer(PROD_INCR, _OnTimerEvent, this); // start the timer

	return ClientPhaseSDL::open();	
}

int ClientPhaseSDL_Prod::close()
{
	return ClientPhaseSDL::close();
}

int ClientPhaseSDL_Prod::execute()
{
	return ClientPhaseSDL::execute();
}

int ClientPhaseSDL_Prod::doFrame()
{
	return ClientPhaseSDL::doFrame();	
}

int ClientPhaseSDL_Prod::msgHandler(Message& msg)
{
	return ClientPhaseSDL::msgHandler(msg);	
}

int ClientPhaseSDL_Prod::OnEvent(SDL_Event& event)
{
	InputType inputType;
	int playerId;
	bool bStart;
	//
	// resort to mutable input class to translate SDL events into
	// game events.
	//
	inputType = data->m_input.get(event, playerId, bStart);
	switch(inputType)
	{
	case IO_SELECT:
		OnSelect(playerId);
		break;
	case IO_QUIT:
		return 0;
	default:
		// ignore
		break;
	}
	return 1;
}

int ClientPhaseSDL_Prod::OnSelect(int playerId)
{
	if(m_timerID != NULL)
	{
		//
		// Select while timer is still running.
		// don't switch phases just yet.
		//
		OnEndProduction();
	}
	else
	{
		//
		// select to advance the phase
		//
		MessageEvent event;
		event.code = MSG_CODE_PLAYER_SELECT;
		m_client.dispatch(event);
	}
	return 1;
}

int ClientPhaseSDL_Prod::OnEndProduction()
{
	//
	// finish drawing all the resource dots
	//
	while(updateProduction() > 0);

	//
	// inform the user to switch to next phase
	//
	SDL_Rect dest;
	dest.x = 0;
	dest.y = SCREEN_HEIGHT-95; //580;
	dest.h = 95;
	dest.w = 798;
	Uint32 color = SDL_MapRGB(screen->format, 0, 0, 0);
	SDL_FillRect(screen, &dest, color);  //erase screen

	data->gfx.setFont(m_instructionfont, 10);
	data->gfx.drawFont("Press All Player Buttons to Go On", 10, 580);

	SDL_UpdateRects(screen, 1, &dest);

	SDL_RemoveTimer(m_timerID); // Delete timer, if exits
	m_timerID = NULL;
	return 1;
}

Uint32 ClientPhaseSDL_Prod::OnTimerEvent(Uint32 interval)
{
	m_timeElapsed += interval;

	if(m_timeElapsed >= PROD_TIME || updateProduction() <= 0)
	{
		OnEndProduction();
		return 0; // stops timer
	}

	return interval; // keep timer going
}

int ClientPhaseSDL_Prod::updateProduction()
{
	unsigned int prand; // rand_r() state
	int x, y;

	m_nDotRender += m_nDotPerTimer;
	
	while(42)
	{
		if(m_iDot >= m_nDot)
			return 0;
		if(m_nDotRender < 1.0f)
			break; // not enough time left to render more dots

		x = RAND(&prand, 0, PLOT_NX-1);
		y = RAND(&prand, 0, PLOT_NY-1);

		MessagePlot *pPlot = m_state.plot[x][y];
		if(pPlot->resOutfit == RES_NONE)
			continue;
		if(pPlot->owner < 0 || pPlot->owner >= MAX_PLAYER)
			continue;
		MessagePlayer *pPlayer = m_state.getPlayer(pPlot->owner);
		if(pPlayer == NULL)
			continue;
		if(m_state.plotProduced[x][y] <= 0)
			continue;
		if(m_plotProduced[x][y] >= m_state.plotProduced[x][y])
			continue; // already rendered all the resource dots

		int posx = x * 114 + 10;
		int posy = (y+1) * 114 - 18;
		int offx = m_plotProduced[x][y] % 2 * 10;
		int offy = m_plotProduced[x][y] / 2 * 10;

		m_plotProduced[x][y]++;
		m_iDot++;

		SDL_Rect dest;
		dest.x = posx + offx;
		dest.y = posy - offy;
		dest.w = 8;
		dest.h = 8;
		Uint32 clr = SDL_MapRGB(screen->format, 255, 128, 64);

		data->gfx.drawFilledRect(&dest, clr);
		SDL_UpdateRects(screen, 1, &dest);

		m_nDotRender -= 1.0f;
	}

	return 1;
}



