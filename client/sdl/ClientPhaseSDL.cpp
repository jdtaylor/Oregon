
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include "ClientPhaseSDL.h"
#include "common.h"


ClientPhaseSDL::ClientPhaseSDL(IClient& client)
	: ClientPhase(client)
{
	screen = NULL;
	data = NULL;
}

ClientPhaseSDL::~ClientPhaseSDL()
{
}


int ClientPhaseSDL::open()
{
	//
	// make sure any player movements are seized
	//
	int i, j;
	for(j = 0; j < MAX_PLAYER; j++)
	for(i = 0; i < MAX_ARROW; i++)
		data->m_bArrowKey[j][i] = false;

	//
	// flush the event queue of previous phase events
	//
	SDL_Event event;
	while(SDL_PollEvent(&event));

	return 1;
}

int ClientPhaseSDL::close()
{
	SDL_Rect dest;
	Uint32 color;

	dest.x = 0;
	dest.y = 570;
	dest.h = 105;
	dest.w = 798;
	color = SDL_MapRGB(screen->format, 0, 0, 0);
	SDL_FillRect(screen, &dest, color);  //erase screen
	SDL_UpdateRects(screen, 1, &dest);
	return 1;
}

int ClientPhaseSDL::execute()
{
	SDL_Event event;

	while(SDL_PollEvent(&event))
	{
		switch(event.type)
		{
			case SDL_QUIT:
				return 0; // exit app
			default:
				break;
		}
		if(OnEvent(event) <= 0) // calls subclass method in vtable
			return 0;
	}
	return 1;
}

int ClientPhaseSDL::doFrame()
{
	return 1;
}

int ClientPhaseSDL::msgHandler(Message& msg)
{
	return 1;
}

int ClientPhaseSDL::OnEvent(SDL_Event& event)
{
	return 1;
}

int ClientPhaseSDL::UpdatePlayerInfoBar()
{
	SDL_Rect dest;
	MessagePlayer *pPlayer = getCurPlayer();
	Uint32 color = SDL_MapRGB(screen->format, 0, 0, 0);

	dest.x = 0;
	dest.y = SCREEN_HEIGHT - 12;
	dest.w = SCREEN_WIDTH;
	dest.h = 10;
	data->gfx.drawFilledRect(&dest, color);
	char* playerinfo = (char*)malloc(100);
	snprintf(playerinfo, 99, "turn: %s  Money: %d  Food: %d  Energy: %d  Ore: %d  Oil: %d", pPlayer->name.data(), pPlayer->money, pPlayer->prev[1], pPlayer->prev[2], pPlayer->prev[3], pPlayer->prev[4]);

	data->gfx.setFont(data->playersmallfont[pPlayer->playerId], 10);
	data->gfx.drawFontRightJustify(playerinfo, SCREEN_WIDTH - 5, SCREEN_HEIGHT - 12);

	free(playerinfo);
	SDL_UpdateRects(screen, 1, &dest);
	return 1;
}

int ClientPhaseSDL::get_plot(MessagePlayer *pPlayer, int plots[4][2])
{
	int x;

	if(pPlayer == NULL)
		return -1;

	for(x = 0; x < 5; x++) {
		plots[x][0] = -1;
		plots[x][1] = -1;
	}

	plots[0][0] = pPlayer->pos_x / 114;
	plots[0][1] = pPlayer->pos_y / 114;

	x = 1;

	if(pPlayer->pos_x % 114 > 94) {
		if(plots[0][0] != 6) {
			plots[x][0] = plots[0][0] + 1;
			plots[x][1] = plots[0][1];
			x++;
		}
	}

	if(pPlayer->pos_y % 114 > 94)
	{
		if(plots[0][1] != (4))
		{
			plots[x][0] = plots[0][0];
			plots[x][1] = plots[0][1] + 1;
			x++;
		}
	}

	if(pPlayer->pos_x % 114 > 94 && pPlayer->pos_y % 114 > 94)
	{
		if(plots[0][0] != 6 && plots[0][1] != 4)
		{
			plots[x][0] = plots[0][0] + 1;
			plots[x][1] = plots[0][1] + 1;
			x++;
		}
	}
	return x;
}

int ClientPhaseSDL::draw_player(MessagePlayer *pPlayer, SDL_Surface *playerimage)
{
	SDL_Rect dest;
	SDL_Rect playerdest;
	int pid = pPlayer->playerId;

	data->footstepcount[pid]++;
	playerdest.x = 2*(pPlayer->direction * 22) + (data->footstep[pid] * 22);
	playerdest.y = 0;
	playerdest.w = 22;
	playerdest.h = 22;
	dest.x = pPlayer->pos_x;
	dest.y = pPlayer->pos_y;
	dest.w = 22;
	dest.h = 22;
	SDL_BlitSurface(playerimage, &playerdest, screen, &dest);
	SDL_UpdateRects(screen, 1, &dest);
	if(data->footstepcount[pid] == 8)
	{
		data->footstepcount[pid] = 0;
		data->footstep[pid] = 1 - data->footstep[pid];
	}
	return 1;
}

int ClientPhaseSDL::erase_player(MessagePlayer *pPlayer, SDL_Surface *surface)
{
	SDL_Rect dest;
	dest.x = pPlayer->pos_x+1;
	dest.y = pPlayer->pos_y+1;
	dest.w = 20;
	dest.h = 20;
	SDL_BlitSurface(surface, &dest, screen, &dest);
	SDL_UpdateRects(screen, 1, &dest);
	return 1;
}

int ClientPhaseSDL::draw_mule(MessagePlayer *pPlayer, SDL_Surface *muleimage)
{
	SDL_Rect dest;
	SDL_Rect muledest;
	int pid = pPlayer->playerId;

	//	pPlayer->footstepcount++;
	muledest.x = 2*(pPlayer->direction * 22) + (data->footstep[pid] * 22);
	muledest.y = 0;
	muledest.w = 22;
	muledest.h = 22;
	dest.x = pPlayer->pos_x + 13;
	dest.y = pPlayer->pos_y + 13;
	dest.w = 22;
	dest.h = 22;
	SDL_BlitSurface(muleimage, &muledest, screen, &dest);
	SDL_UpdateRects(screen, 1, &dest);
	return 1;
}

