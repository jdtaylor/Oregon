
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include "common.h"
#include "ClientPhaseFactorySDL.h"
#include "ClientPhaseSDL_Title.h"
#include "ClientPhaseSDL_Grant.h"
#include "ClientPhaseSDL_Devel.h"
#include "ClientPhaseSDL_Prod.h"
#include "ClientPhaseSDL_Auction.h"
#include "ClientPhaseSDL_Standings.h"
#include "SharedData.h"

using namespace std;

ClientPhaseFactorySDL::ClientPhaseFactorySDL(IClient& client)
	: ClientPhaseFactory(client)
{
}

int ClientPhaseFactorySDL::create(ClientPhase* phase[MAX_PHASE])
{
	unsigned int i;
	SDL_Surface *pScreen;

	//
	// this is important because it calls SDL_Init and
	// SDL_SetVideoMode.  Some of the phase constructors rely on these
	// to have been called in advance.
	//
	// plus it opens the screen.. :)
	//
	pScreen = openScreen();
	if(pScreen == NULL) {
		ERROR << "Failed to open screen" << endl;
		return -1;
	}

	for(i = 0; i < MAX_PHASE; i++)
		phase[i] = NULL;

	phase[PHASE_TITLE] = new ClientPhaseSDL_Title(m_client);
	phase[PHASE_GRANT] = new ClientPhaseSDL_Grant(m_client);
	phase[PHASE_DEVEL] = new ClientPhaseSDL_Devel(m_client);
	phase[PHASE_PROD] = new ClientPhaseSDL_Prod(m_client);
	phase[PHASE_AUCTION] = new ClientPhaseSDL_Auction(m_client);
	phase[PHASE_STANDINGS] = new ClientPhaseSDL_Standings(m_client);

    SharedData *pSharedData = new SharedData(m_client);
	pSharedData->gfx.setScreen(pScreen);

	for(i = 0; i < MAX_PHASE; i++) {
		if(phase[i] != NULL) {
			((ClientPhaseSDL*)phase[i])->setScreen(pScreen);
			((ClientPhaseSDL*)phase[i])->setSharedData(pSharedData);
		}
	}

	return 1;
}

SDL_Surface* ClientPhaseFactorySDL::openScreen()
{
	const SDL_VideoInfo *vidInfo = NULL;
	Uint8 bpp = 24;
	Uint16 wd = SCREEN_WIDTH;
	Uint16 ht = SCREEN_HEIGHT;
	Uint32 flags = 0;
	SDL_Surface *pScreen;

	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER | SDL_INIT_NOPARACHUTE) < 0) {
		ERROR << "SDL_Init: " << SDL_GetError() << endl;
		return NULL;
	}
	vidInfo = SDL_GetVideoInfo();
	if(vidInfo <= 0) {
		ERROR << "SDL_GetVideoInfo: " << SDL_GetError() << endl;
		return NULL;
	}
	bpp = vidInfo->vfmt->BitsPerPixel;

	//flags |= SDL_HWPALETTE;
	//flags |= SDL_RESIZABLE;

	/*
	if(vidInfo->hw_available)
		flags |= SDL_HWSURFACE | SDL_DOUBLEBUF;
	else
		flags |= SDL_SWSURFACE;

	if(vidInfo->blit_hw)
		flags |= SDL_HWACCEL | SDL_DOUBLEBUF;
	*/

	//flags |= SDL_HWSURFACE | SDL_DOUBLEBUF;
	
	//
	// For fuck-sake, don't turn this flag on without stable closing
	// of the screen.. like in atexit() or something.
	//
	//flags |= SDL_FULLSCREEN;


	//
	// This will actually open the window
	//
	pScreen = SDL_SetVideoMode(wd, ht, bpp, flags);
	if(pScreen == NULL) {
		ERROR << "SDL_SetVideoMode: " << SDL_GetError() << endl;
		return 0;
	}
	SDL_WM_SetCaption(OREGON_TITLE, NULL);
	SDL_WM_SetIcon(IMG_Load(PROG_ICON),NULL);

	//SDL_EnableKeyRepeat(1, 1);
	return pScreen;
}

