
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include "ClientPhaseSDL_Grant.h"
#include "common.h"

using namespace std;


ClientPhaseSDL_Grant::ClientPhaseSDL_Grant(IClient& client)
	: ClientPhaseSDL(client)
{
	m_instructionfont = IMG_Load("./data/gfx/fonts/10w.png");
	if(!m_instructionfont) {
		ERROR << "Failed to load: data/gfx/fonts/10w.png" << endl;
		return;
	}
	m_houseimg = IMG_Load("./data/gfx/sprites/house.png");
	if(!m_houseimg) {
		ERROR << "Failed to load: data/gfx/sprites/house.png" << endl;
		return;
	}
}

int ClientPhaseSDL_Grant::open()
{
	SDL_Rect dest;
	MessagePlayer *pPlayer = getCurPlayer();

	pPlayer->direction = 2;
	pPlayer->pos_x = 389; //player starting position
	pPlayer->pos_y = 275; //player starting position
	if(data != NULL)
		data->blitMap(screen);

	dest.x = 0;
	dest.y = 580;
	dest.h = 95;
	dest.w = 798;
	Uint32 color = SDL_MapRGB(screen->format, 0, 0, 0);
	SDL_FillRect(screen, &dest, color);  //erase instructions
	SDL_UpdateRects(screen, 1, &dest);

	data->gfx.setFont(m_instructionfont, 10);

	if(m_state.getPlotsAvailable() <= 0) //if there is no land available
	{
		data->gfx.drawFont("Warning:", 10, 580);
		data->gfx.drawFont("*There are no more plots available, press the [space bar] key", 20, 595);
		data->gfx.drawFont(" to continue to the development phase", 20, 610);
	}
	else
	{
        //draw player starting position
		draw_player(pPlayer, data->playerimg[pPlayer->playerId]);
		data->gfx.drawFont("instructions for land grant:", 10, 580);
		data->gfx.drawFont("*Select a plot by moving on to it and pressing the [space bar] key", 20, 595);
		data->gfx.drawFont("*or Move on the store and press the [space bar] key to skip this round", 20, 625);
		char* plotcost = new char[50];
		snprintf(plotcost, 49, "*A plot of land costs $%d", PLOT_VALUE);
		data->gfx.drawFont(plotcost, 20, 610);
		delete[] plotcost;
		UpdatePlayerInfoBar();
	}
	dest.x = 0;
	dest.y = 0;
	dest.w = SCREEN_WIDTH;
	dest.h = SCREEN_HEIGHT;
	SDL_UpdateRects(screen, 1, &dest); //update ENTIRE screen

	return ClientPhaseSDL::open();
}

int ClientPhaseSDL_Grant::close()
{
	return ClientPhaseSDL::close();
}

int ClientPhaseSDL_Grant::execute()
{
	int i;
	int playerId = getCurPlayer()->playerId;
	//
	// restrict arrow keys to just the keypad
	//
	for(i = ARROW_UP; i <= ARROW_RIGHT; i++)
		if(data->m_bArrowKey[playerId][i])
			break;
	if(i <= ARROW_RIGHT)
		OnMove();

	return ClientPhaseSDL::execute();
}

int ClientPhaseSDL_Grant::doFrame()
{
	return ClientPhaseSDL::doFrame();
}

int ClientPhaseSDL_Grant::msgHandler(Message& msg)
{
	return ClientPhaseSDL::msgHandler(msg);
}

int ClientPhaseSDL_Grant::OnEvent(SDL_Event& event)
{
	InputType inputType;
	int playerId = getCurPlayer()->playerId;
	int id;
	bool bStart;
	int arrowKey = -1;
	//
	// resort to mutable input class to translate SDL events into
	// game events.
	//
	inputType = data->m_input.get(event, id, bStart);
	switch(inputType)
	{
	case IO_SELECT:
		OnSelect();
		break;
	case IO_UP:
		arrowKey = ARROW_UP;
		break;
	case IO_DOWN:
		arrowKey = ARROW_DOWN;
		break;
	case IO_LEFT:
		arrowKey = ARROW_LEFT;
		break;
	case IO_RIGHT:
		arrowKey = ARROW_RIGHT;
		break;
	case IO_QUIT:
		return 0;
	default:
		// ignore
		break;
	}
	if(arrowKey >= 0 && arrowKey < MAX_ARROW)
	{
		if(bStart)
			data->m_bArrowKey[playerId][arrowKey] = true;
		else
			data->m_bArrowKey[playerId][arrowKey] = false;
	}
	return 1;
}

int ClientPhaseSDL_Grant::OnMove()
{
	SDL_Rect dest;
	MessagePlayer *pPlayer = getCurPlayer();
	int i = pPlayer->playerId;
	dest.x = pPlayer->pos_x;
	dest.y = pPlayer->pos_y;
	dest.w = 22;
	dest.h = 22;
	//erase old player
	SDL_BlitSurface(data->m_pLand, &dest, screen, &dest);

	if(m_state.getPlotsAvailable() <= 0)
		return 1; // no more plots available, eh
	//
	// Increment player positions based on whether the directional key
	// is being depressed.
	//
	if(data->m_bArrowKey[i][ARROW_UP]) {
		pPlayer->direction = 0;
		if(pPlayer->pos_y > 0)
			pPlayer->pos_y--;
	}
	if(data->m_bArrowKey[i][ARROW_DOWN]) {
		pPlayer->direction = 2;
		if(pPlayer->pos_y < (114 * PLOT_NY - 21))
			pPlayer->pos_y++;
	}
	if(data->m_bArrowKey[i][ARROW_LEFT]) {
		pPlayer->direction = 3;
		if(pPlayer->pos_x > 0)
			pPlayer->pos_x--;
	}
	if(data->m_bArrowKey[i][ARROW_RIGHT]) {
		pPlayer->direction = 1;
		if(pPlayer->pos_x < (114 * PLOT_NX - 21))
			pPlayer->pos_x++;
	}
	//draw new player
	draw_player(pPlayer, data->playerimg[i]);
	SDL_Delay(10); //after player moves, delay to slow movement
	//SDL_Flip(screen);
	return 1;
}

int ClientPhaseSDL_Grant::OnSelect()
{
	SDL_Rect dest;
	Uint32 clr = 0;
	int nPlot;    // Number of plots returned by the get_plot function
	int plots[4][2]; // array with all the plots the player is on
	MessagePlayer *pPlayer = getCurPlayer();

	nPlot = get_plot(pPlayer, plots);

	//A very crude select plot box..
	if(m_state.getPlotsAvailable() <= 0) // If there are no plots available
	{
		while (m_state.sys->phase == PHASE_GRANT)
		{
			MessageEvent event;
			event.code = MSG_CODE_PLAYER_SELECT;
			m_client.dispatch(event);
		} //end while loop to keep going thru players until @ devel stage
		return 1; // shouldn't be reached
	} 

	//Player on store, skips turn, also includes river surrounding store...
	if(pPlayer->pos_x >= STORE_X * 114 &&
	   pPlayer->pos_x <= STORE_X * 114 + 114 &&
	   pPlayer->pos_y >= STORE_Y * 114 &&
	   pPlayer->pos_y <= STORE_Y * 114 + 114)
	{
		MessageEvent event;
		event.code = MSG_CODE_PLAYER_SELECT;
		m_client.dispatch(event);
		return 1;
	}
	
	if(pPlayer->money < PLOT_VALUE)
	{
		//tell the user why he cannot buy the land
		SDL_Rect warntext;
		Uint32 color = SDL_MapRGB(screen->format, 0, 0, 0);
		warntext.x = SCREEN_WIDTH - 405;
		warntext.y = SCREEN_HEIGHT - 24;
		warntext.w = 405;
		warntext.h = 11;
		SDL_FillRect(screen, &dest, color); //erase old warning if there
		data->gfx.setFont(data->playersmallfont[pPlayer->playerId], 10);
		data->gfx.drawFontRightJustify("Not enough money to buy land!",
				SCREEN_WIDTH - 5, SCREEN_HEIGHT - 24);
		SDL_UpdateRects(screen,1,&warntext);
		SDL_Delay(2000); // hack to wait
		erase_player(pPlayer, data->m_pLand);
		MessageEvent event;
		event.code = MSG_CODE_PLAYER_SELECT;
		m_client.dispatch(event);
		return 1;
	}
	if(nPlot != 1)
		return 1; // player isn't on one single plot.. do nothing

	MessagePlot *pPlot = m_state.plot[plots[0][0]][plots[0][1]]; 

	if(pPlot->owner >= 0)
		return 1; // plot is already owned by someone.. do nothing

	pPlayer->money -= PLOT_VALUE;
	UpdatePlayerInfoBar(); //show new money

	pPlot->owner = pPlayer->playerId;
	pPlayer->nPlotOwned++;

	switch(pPlayer->playerId) {
	case 0:
		clr = SDL_MapRGB(screen->format, 255, 0, 0); //red
		break;
	case 1:
		clr = SDL_MapRGB(screen->format, 240, 0, 255); //pink
		break;
	case 2:
		clr = SDL_MapRGB(screen->format, 255, 246, 0); //yellow
		break;
	case 3:
		clr = SDL_MapRGB(screen->format, 0, 252, 255); //light_blue
		break;
	}
	// temp erase player from screen
	erase_player(pPlayer, data->m_pLand);
	
	dest.x = plots[0][0]*114 + 10;
	dest.y = plots[0][1]*114 + 10;
	dest.w = 40;
	dest.h = 40;
	SDL_BlitSurface(m_houseimg, NULL, screen, &dest); //put house symbol
	dest.x = plots[0][0]*114;
	dest.y = plots[0][1]*114;
	dest.w = 114;
	dest.h = 114;
	data->gfx.drawRect(&dest, 4, clr); //highlight box on screen
	// write current screen to background
	SDL_BlitSurface(screen, &dest, data->m_pLand, &dest);
	SDL_UpdateRects(screen, 1, &dest); //show updated screen
	//
	// when ready to go to development stage:
	// send a PLAYER_SELECT message to the server
	//
	MessageEvent event;
	event.code = MSG_CODE_PLAYER_SELECT;
	m_client.dispatch(event);
	return 1;
}


