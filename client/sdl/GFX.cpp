/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 *
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *              Reza Assadi <gassadi@uh.edu>
 *              Danny Bousleiman <dan_man89@hotmail.com>
 *              Jeffrey Jardiolin <jjardiolin@uh.edu>
 *              John Knieper <jknieper@gmail.com>
 *              Mike Mack <mikemackjr@gmail.com>
 *              Rajiv Shah <brainix@gmail.com>
 *              James D. Taylor <james.d.taylor@gmail.com>
 *              Roland Vassallo <rcvassallo@gmail.com>
 */

#include "GFX.h"
#include "common.h"

using namespace std;

GFX::GFX() {
	pScreen = NULL;
	pFont = NULL;
}

/*
 * GFX.drawFont() will put text at (x,y)
 * 		You will have to use SDL_UpdateRects() after calling drawFont
 * 		to make text show.
 * text: the text to be written
 * int x, int y: x,y position of top left corner of starting text
 */
int GFX::drawFont(const char* text, int x, int y)
{
	int i;
	SDL_Rect source;
	SDL_Rect dest;
	int charasint;
	for(i = 0; text[i] != '\0'; i++)
	{
		charasint = (int)text[i];
		if(charasint > 90)
			charasint -= 58; // offset so character 91 is now 33 in font map
		//make UPPERcase and lowercase chars have same charasint value
		else if(charasint < 90 && charasint > 64)
			charasint -= 26;
		else
			charasint -= 32; //offset so character 32 is now 0 in font map

		source.x = fontsize*(charasint/10); //get column in fontmap
		source.y = fontsize*(charasint%10); //get row in fontmap
		source.w = fontsize;
		source.h = fontsize;
		//where this current character will be placed on surface
		dest.x = x+fontsize*i;
		dest.y = y;
		dest.w = fontsize;
		dest.h = fontsize;
		SDL_BlitSurface(pFont, &source, pScreen, &dest);
	}
	return 1;
}

//For sending just numbers to be drawn
int GFX::drawFont(int number, int x, int y)
{
	char text[50];
	snprintf(text, 49, "%d", number);
	drawFont(text, x, y);
	return 1;
}

/*
 * int x, int y: x,y position of top right corner of where the text will end
 */
int GFX::drawFontRightJustify(const char* text, int x, int y)
{
	int i;
	SDL_Rect source;
	SDL_Rect dest;
	int charasint;
	int counter = 0;

	for(; text[counter] != '\0'; counter++);

	for(i = 0; i < counter; i++)
	{
		charasint = (int)text[i];
		if(charasint > 90)
			charasint -= 58; // offset so character 91 is now 33 in font map
		//make UPPERcase and lowercase chars have same charasint value
		else if(charasint < 90 && charasint > 64)
			charasint -= 26;
		else
			charasint -= 32; //offset so character 32 is now 0 in font map

		source.x = fontsize*(charasint/10); //get column in fontmap
		source.y = fontsize*(charasint%10); //get row in fontmap
		source.w = fontsize;
		source.h = fontsize;
		//where this current character will be placed on surface
		dest.x = x - fontsize*(counter - i);
		dest.y = y;
		dest.w = fontsize;
		dest.h = fontsize;
		SDL_BlitSurface(pFont, &source, pScreen, &dest);
	}
	return 1;
}

int GFX::drawFontRightJustify(int number, int x, int y)
{
	char text[50];
	snprintf(text, 49, "%d", number);
	drawFontRightJustify(text, x, y);
	return 1;
}

int GFX::drawFontCentered(const char* text, int y)
{
	int len = strlen(text);
	int npx = len * fontsize;
	int x = (pScreen->w - npx) / 2;
	return drawFont(text, x, y);
}

/*
 * GFX.drawRect() will draw an open rectangle from (x,y) with
 * 		width w and heigth h.  You will have to use SDL_UpdateRects()
 * 		after calling drawRect to make the box show.
 * rect: rect.x, rect.y, rect.w, rect.h must be specified
 * size: size (thickness) of box line in pixels
 * color: SDL_MapRGB(m_pScreen->format, int red, int green, int blue)
 */
int GFX::drawRect(SDL_Rect* rect, int size, Uint32 color)
{
	SDL_Surface *dst = pScreen;
	SDL_Rect dest;
	//top line of box begin
	dest.x = rect->x;
	dest.y = rect->y;
	dest.h = size;
	dest.w = rect->w - size;
	SDL_FillRect(dst, &dest, color); //top line end
	//bottom line of box begin
	dest.x = rect->x;
	dest.y = rect->y + rect->h - size;
	dest.h = size;
	dest.w = rect->w - size;
	SDL_FillRect(dst, &dest, color); //bottom line end
	//left line of box begin
	dest.x = rect->x;
	dest.y = rect->y;
	dest.h = rect->h;
	dest.w = size;
	SDL_FillRect(dst, &dest, color); //left line end
	//right line of box begin
	dest.x = rect->x + rect->w - size;
	dest.y = rect->y;
	dest.h = rect->h;
	dest.w = size;
	SDL_FillRect(dst, &dest, color); //right line! end
	return 1;
}

int GFX::drawFilledRect(SDL_Rect* dest, Uint32 color)
{
	SDL_FillRect(pScreen, dest, color); //draw a filled in box
	return 1;
}

int GFX::drawDottedLine(int x1, int y1, int x2, int y2, int width,
						int dotlen, int dotspace, Uint32 color)
{
	int i;
	SDL_Surface *dst = pScreen;
	SDL_Rect dest;

	if(x1 - x2 != 0)
	{
		//line is horizontal
		i = x1;
		while(i <= x2)
		{
			dest.x = i;
			dest.w = dotlen;
			dest.y = y1;
			dest.h = width;
			SDL_FillRect(dst, &dest, color);
			i += dotlen + dotspace;
		}
		dest.x = x2 - dotlen;
		dest.w = dotlen;
		dest.y = y1;
		dest.h = width;
        SDL_FillRect(dst, &dest, color);
	}
	if(y1 - y2 != 0)
	{
		//line is vertical
		i = y1;
		while(i <= y2)
		{
			dest.x = x1;
			dest.w = width;
			dest.y = i;
			dest.h = dotlen;
			SDL_FillRect(dst, &dest, color);
			i += dotlen + dotspace;
		}
		dest.x = x1;
		dest.w = width;
		dest.y = y2-dotlen;
		dest.h = dotlen;
        SDL_FillRect(dst, &dest, color);
	}
	return 1;
}

