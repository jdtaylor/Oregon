
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include "SharedData.h"
#include "ClientPhaseSDL.h"
#include "common.h"
#include "commonSDL.h"

using namespace std;


SharedData::SharedData(IClient& client)
	: m_client(client)
{
	State& state = client.getState();

	pTileDirt = IMG_Load(TILE_DIRT);
	if(!pTileDirt) {
		ERROR << "Failed to load: " << TILE_DIRT << endl;
		return;
	}
	pTileWater = IMG_Load(TILE_WATER);
	if(!pTileWater) {
		ERROR << "Failed to load: " << TILE_WATER << endl;
		return;
	}
	pTileGrass = IMG_Load(TILE_GRASS);
	if(!pTileGrass) {
		ERROR << "Failed to load: " << TILE_GRASS << endl;
		return;
	}
	pTileStore = IMG_Load(TILE_STORE);
	if(!pTileStore) {
		ERROR << "Failed to load: " << TILE_STORE << endl;
		return;
	}
	pTileMountain = IMG_Load(TILE_MOUNT);
	if(!pTileMountain) {
		ERROR << "Failed to load: " << TILE_MOUNT << endl;
		return;
	}

	m_pLand = CreateSurface(SDL_SWSURFACE, SCREEN_WIDTH, SCREEN_HEIGHT);
	if(m_pLand == NULL) {
		ERROR << "Failed to create background surface" << endl;
		return;
	}

	SDL_Rect dest;
	int i, j;

	for(i = 0; i < PLOT_NY; i++)
	{
		for(j = 0; j < PLOT_NX; j++)
		{
			dest.x = 114 * j;
			dest.y = 114 * i;
			dest.w = 114;
			dest.h = 114;

			switch(state.plot[j][i]->terrain)
			{
			case TERRAIN_DIRT:
				SDL_BlitSurface(pTileDirt, NULL, m_pLand, &dest);
				break;
			case TERRAIN_GRASS:
				SDL_BlitSurface(pTileGrass, NULL, m_pLand, &dest);
				break;
			case TERRAIN_RIVER:
				SDL_BlitSurface(pTileWater, NULL, m_pLand, &dest);
				break;
			case TERRAIN_STORE:
				SDL_BlitSurface(pTileStore, NULL, m_pLand, &dest);
				break;
			default:
				break;
			}

			if(state.plot[j][i]->mountain)
				SDL_BlitSurface(pTileMountain, NULL, m_pLand, &dest);
		}
	}
	SDL_DisplayFormat(m_pLand);

	//
	// The resource icon that shows up at the bottom
	// left of the plot after outfitting it.
	//
	for(i = 0; i < MAX_RESOURCE; i++)
	{
		switch(i)
		{
		case RES_FOOD:
			m_pPlotRes[i] = IMG_Load("./data/gfx/sprites/food.png");
			break;
		case RES_ENERGY:
			m_pPlotRes[i] = IMG_Load("./data/gfx/sprites/energy.png");
			break;
		case RES_ORE:
			m_pPlotRes[i] = IMG_Load("./data/gfx/sprites/ore.png");
			break;
		case RES_OIL:
			m_pPlotRes[i] = IMG_Load("./data/gfx/sprites/oil.png");
			break;
		default:
			m_pPlotRes[i] = NULL;
			break;
		}
	}
	m_pPlotHouse = IMG_Load("./data/gfx/sprites/house.png");

//	m_pPlayer = NULL;

	string strSmallFont, strBigFont;

	for(i = 0; i < MAX_PLAYER; i++)
	{
		switch(i)
		{
		case 0:
			strSmallFont = "./data/gfx/fonts/10r.png"; 
			strBigFont = "./data/gfx/fonts/24r.png";
			break;
		case 1:
			strSmallFont = "./data/gfx/fonts/10p.png"; 
			strBigFont = "./data/gfx/fonts/24p.png";
			break;
		case 2:
			strSmallFont = "./data/gfx/fonts/10y.png"; 
			strBigFont = "./data/gfx/fonts/24y.png";
			break;
		case 3:
			strSmallFont = "./data/gfx/fonts/10lb.png"; 
			strBigFont = "./data/gfx/fonts/24lb.png";
			break;
		}

		playersmallfont[i] = IMG_Load(strSmallFont.c_str());
		if(!playersmallfont[i]) {
			ERROR << "Failed to load: " << strSmallFont.c_str() << endl;
			return;
		}
		playerbigfont[i] = IMG_Load(strBigFont.c_str());
		if(!playerbigfont[i]) {
			ERROR << "Failed to load: " << strBigFont.c_str() << endl;
			return;
		}

		footstep[i] = 0;
		footstepcount[i] = 0;
	}

	for(j = 0; j < MAX_PLAYER; j++)
	for(i = 0; i < MAX_ARROW; i++)
		m_bArrowKey[j][i] = false; // arrow key not depressed.  yay
}

SharedData::~SharedData()
{
//	SDL_FreeSurface(m_pPlayer);
	SDL_FreeSurface(m_pLand);
}

void SharedData::blitMap(SDL_Surface *pSurface)
{
	SDL_Rect dest;

	dest.x = 0;
	dest.y = 0;
	dest.w = SCREEN_WIDTH;
	dest.h = SCREEN_HEIGHT;
	SDL_BlitSurface(m_pLand, &dest, pSurface, &dest);
	SDL_UpdateRects(pSurface, 1, &dest);
	//SDL_Flip(m_pScreen);
}

