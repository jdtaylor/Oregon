
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include <ctime>
#include "ClientPhaseSDL_Devel.h"
#include "common.h"
#include "Resource.h"
#include "State.h"

using namespace std;


ClientPhaseSDL_Devel::ClientPhaseSDL_Devel(IClient& client)
	: ClientPhaseSDL(client)
{
	m_instructionfont = IMG_Load("./data/gfx/fonts/10w.png");
	if(!m_instructionfont) {
		ERROR << "File failed to load: " << "data/gfx/fonts/10w.png" << endl;
		return;
	} 
	m_screenfont = IMG_Load("./data/gfx/fonts/10b.png");
	if(!m_instructionfont) {
		ERROR << "File failed to load: " << "data/gfx/fonts/10b.png" << endl;
		return;
	}
	m_mulesprite = IMG_Load("./data/gfx/sprites/donkey.png");
	if(!m_mulesprite) {
		ERROR << "File failed to load: data/gfx/sprites/donkey.png" << endl;
		return;
	}
	m_inside_store = IMG_Load("data/gfx/map/basic/inside_store.png");
	if(!m_inside_store) {
		ERROR << "Failed to load: data/gfx/map/basic/inside_store.png" << endl;
		return;
	}
}

int ClientPhaseSDL_Devel::open()
{
	SDL_Rect dest;

	// Initialize the move counter to zero for every player.
	moveCounter = 0;
	MessagePlayer *pPlayer = getCurPlayer();

	pPlayer->inStore = true;
	pPlayer->direction = 2;
	
	m_pDevel = CreateSurface(SDL_SWSURFACE, SCREEN_WIDTH, SCREEN_HEIGHT);
	data->blitMap(m_pDevel);

	//draw inside store view
	dest.x = 228;
	dest.y = 114;
	dest.w = 342;
	dest.h = 342;
	SDL_BlitSurface(m_inside_store, NULL, m_pDevel, &dest);

	//draw background to screen
	dest.x = 0;
	dest.y = 0;
	dest.w = 798;
	dest.h = 570;
	SDL_BlitSurface(m_pDevel, &dest, screen, &dest);
	InsideStoreView(1);

	//draw player starting position
	pPlayer->pos_x = 389;
	pPlayer->pos_y = 275;
	draw_player(pPlayer, data->playerimg[pPlayer->playerId]);
    dest.x = 0;
	dest.y = 580;
	dest.h = 95;
	dest.w = 798;
	Uint32 color = SDL_MapRGB(screen->format, 0, 0, 0);
	SDL_FillRect(screen, &dest, color);  //erase instructions

	data->gfx.setFont(m_instructionfont, 10);
	data->gfx.drawFont("instructions for land development:", 10, 580);
	data->gfx.drawFont("*Buy a mule and outfit it with a resource", 20, 595);
	data->gfx.drawFont("*Go to your plot of land and set the resource up", 20, 610);
	data->gfx.drawFont(" by pressing the [space bar] key", 20, 625);
	data->gfx.drawFont("*Go to the PUB to end your turn", 20, 640);

	UpdatePlayerInfoBar();
	dest.x = 0;
	dest.y = 0;
	dest.w = SCREEN_WIDTH;
	dest.h = SCREEN_HEIGHT;
	SDL_UpdateRects(screen, 1, &dest); //update ENTIRE screen

	return ClientPhaseSDL::open();
}

int ClientPhaseSDL_Devel::close()
{
	return ClientPhaseSDL::close();
}

int ClientPhaseSDL_Devel::execute()
{
	int i;
	unsigned int playerId = getCurPlayer()->playerId;
	//
	// restrict arrow keys to just the keypad
	//
	for(i = ARROW_UP; i <= ARROW_RIGHT; i++)
		if(data->m_bArrowKey[playerId][i])
			break;
	if(i <= ARROW_RIGHT)
		OnMove();

	return ClientPhaseSDL::execute();
}

int ClientPhaseSDL_Devel::doFrame()
{
	return ClientPhaseSDL::doFrame();
}

int ClientPhaseSDL_Devel::msgHandler(Message& msg)
{
	return ClientPhaseSDL::msgHandler(msg);
}

int ClientPhaseSDL_Devel::OnEvent(SDL_Event& event)
{
	InputType inputType;
	unsigned int playerId = getCurPlayer()->playerId;
	int id;
	bool bStart;
	int arrowKey = -1;

	//
	// resort to mutable input class to translate SDL events into
	// game events.
	//
	inputType = data->m_input.get(event, id, bStart);
	switch(inputType)
	{
	case IO_SELECT:
		OnSelect();
		break;
	case IO_UP:
		arrowKey = ARROW_UP;
		break;
	case IO_DOWN:
		arrowKey = ARROW_DOWN;
		break;
	case IO_LEFT:
		arrowKey = ARROW_LEFT;
		break;
	case IO_RIGHT:
		arrowKey = ARROW_RIGHT;
		break;
	case IO_QUIT:
		return 0;
	default:
		// ignore
		break;
	}

	if(arrowKey >= 0 && arrowKey < MAX_ARROW)
	{
		if(bStart)
			data->m_bArrowKey[playerId][arrowKey] = true;
		else
			data->m_bArrowKey[playerId][arrowKey] = false;
	}
	return 1;
}

int ClientPhaseSDL_Devel::OnMove()
{
	SDL_Rect dest;
	MessagePlayer *pPlayer = getCurPlayer();
	unsigned int playerId = pPlayer->playerId;
	dest.x = pPlayer->pos_x;
	dest.y = pPlayer->pos_y;
	dest.w = 22;
	dest.h = 22;

	data->gfx.setFont(m_screenfont, 10);

	// When player is out of food, the player cannot move anymore,
	// and it's the next players turn.
	if(pPlayer->use[RES_FOOD] == pPlayer->prev[RES_FOOD])  
	{
		SDL_Rect warntext;
		Uint32 color = SDL_MapRGB(screen->format, 0, 0, 0);
		warntext.x = SCREEN_WIDTH - 245;
		warntext.y = SCREEN_HEIGHT - 24;
		warntext.w = 245;
		warntext.h = 11;
		SDL_FillRect(screen, &dest, color); //erase old warning if there
		data->gfx.setFont(data->playersmallfont[pPlayer->playerId], 10);
		data->gfx.drawFontRightJustify("You've run out of food!",
				SCREEN_WIDTH - 5, SCREEN_HEIGHT - 24);
		SDL_UpdateRects(screen,1,&warntext);
		SDL_Delay(2000); // hack to wait
		
		//erase old player, and mule
		SDL_FillRect(screen, &dest, color); //erase old warning
    	SDL_BlitSurface(data->m_pLand, &dest, screen, &dest);
		if(pPlayer->haveMule)
			SDL_BlitSurface(data->m_pLand, &mule_pos, screen, &mule_pos);
		pPlayer->haveMule = false;
		pPlayer->muleRes = RES_NONE;
		MessageEvent event;
		event.code = MSG_CODE_PLAYER_SELECT;
		m_client.dispatch(event);
		return 1;
	}

	// If player is not in store, 1 Food is used for every 200 steps take.
	if(!pPlayer->inStore) 
	{
		moveCounter++;
		if(moveCounter == 200)
		{
			moveCounter = 0;
			pPlayer->prev[RES_FOOD]--;
			UpdatePlayerInfoBar(); //update player money
		}
	}
	if(pPlayer->haveMule)
	{
		mule_pos.x = pPlayer->pos_x + 13;
		mule_pos.y = pPlayer->pos_y + 13;
		mule_pos.w = 22;
		mule_pos.h = 22;
	}

	SDL_Surface *bg;
	if(pPlayer->inStore)
		bg = m_pDevel;
	else
		bg = data->m_pLand;

	//
	//erase old player
	//
	SDL_BlitSurface(m_pDevel, &dest, screen, &dest);
	//
	//erase old mule
	//
	if(pPlayer->haveMule)
		SDL_BlitSurface(m_pDevel, &mule_pos, screen, &mule_pos);

	if(data->m_bArrowKey[playerId][ARROW_UP])
	{
		pPlayer->direction = 0;
		if(pPlayer->inStore)
		{
			//boundry in store
			if(pPlayer->pos_y <= 230 &&
			   pPlayer->pos_x >= 266 &&
			   pPlayer->pos_x <= 275)
			{
				if(pPlayer->haveMule && pPlayer->money >= g_resType[RES_OIL].outfitCost)
				{
					if(pPlayer->muleRes != RES_OIL)
						pPlayer->money -= g_resType[RES_OIL].outfitCost;
					pPlayer->muleRes = RES_OIL;
					InsideStoreView(2);
					UpdatePlayerInfoBar(); //update player money
				}
			}
			else if(pPlayer->pos_y <= 230 &&
					pPlayer->pos_x >= 343 &&
					pPlayer->pos_x <= 354)
			{
				if(pPlayer->haveMule && pPlayer->money >= g_resType[RES_ORE].outfitCost)
				{
					if(pPlayer->muleRes != RES_ORE)
						pPlayer->money -= g_resType[RES_ORE].outfitCost;
					pPlayer->muleRes = RES_ORE;
					InsideStoreView(2);
					UpdatePlayerInfoBar(); //update player money
				}
			}
			else if(pPlayer->pos_y <= 230 &&
					pPlayer->pos_x >= 422 &&
					pPlayer->pos_x <= 431)
			{
				if(pPlayer->haveMule &&  pPlayer->money >= g_resType[RES_ENERGY].outfitCost)
				{
					if(pPlayer->muleRes != RES_ENERGY)
						pPlayer->money -= g_resType[RES_ENERGY].outfitCost;
					pPlayer->muleRes = RES_ENERGY;
					InsideStoreView(2);
					UpdatePlayerInfoBar(); //update player money
				}
			}
			else if(pPlayer->pos_y <= 230 &&
					pPlayer->pos_x >= 499 &&
					pPlayer->pos_x <= 510)
			{
				if(pPlayer->haveMule && pPlayer->money >= g_resType[RES_FOOD].outfitCost)
				{
					if(pPlayer->muleRes != RES_FOOD)
						pPlayer->money -= g_resType[RES_FOOD].outfitCost;
					pPlayer->muleRes = RES_FOOD;
					InsideStoreView(2);
					UpdatePlayerInfoBar(); //update player money
				}
			}
			if(pPlayer->pos_y <= 251 &&
			   pPlayer->pos_y > 200 &&
			   pPlayer->pos_x >= 266 &&
			   pPlayer->pos_x <= 275)
			{
				//in oil store door
				pPlayer->pos_y--;
			}
			else if(pPlayer->pos_y <= 251 &&
					pPlayer->pos_y > 200 &&
					pPlayer->pos_x >= 343 &&
					pPlayer->pos_x <= 354)
			{
				//in ore store door
				pPlayer->pos_y--;
			}
			else if(pPlayer->pos_y <= 251 &&
					pPlayer->pos_y > 200 &&
					pPlayer->pos_x >= 422 &&
					pPlayer->pos_x <= 431)
			{
				//in energy store door
				pPlayer->pos_y--;
			}
			else if(pPlayer->pos_y <= 251 &&
					pPlayer->pos_y > 200 &&
					pPlayer->pos_x >= 499 &&
					pPlayer->pos_x <= 510)
			{
				//in food store door
				pPlayer->pos_y--;
			}
			else if(pPlayer->pos_y > 251) {
				//in the black area in middle
				pPlayer->pos_y--;
				InsideStoreView(2);
			}
		}
		else
		{
			//
			// Not in store
			//
			if(pPlayer->pos_y == 322 &&
			   pPlayer->pos_x > 325 &&
			   pPlayer->pos_x < 452)
			{
				//entering store from the bottom
				pPlayer->inStore = true;
				pPlayer->pos_x = 389;
				pPlayer->pos_y = 275;
				InsideStoreView(1);
			}
			else if(pPlayer->pos_y > 0)
				pPlayer->pos_y--;
		}
	}
	if(data->m_bArrowKey[playerId][ARROW_DOWN])
	{
		pPlayer->direction = 2;
		if(pPlayer->inStore)
		{
			//boundry in store
			if(pPlayer->pos_y >= 317 &&
			   pPlayer->pos_x >= 360 &&
			   pPlayer->pos_x <= 418)
			{
				//actually in the mule store, do in store commands here
				if(!pPlayer->haveMule && pPlayer->money >= STORE_INITIAL_MULE_PRICE)
				{
					//dont have a mule yet && player money is enough,
					//then buy one
					pPlayer->money -= STORE_INITIAL_MULE_PRICE;
					pPlayer->muleRes = RES_NONE;
					UpdatePlayerInfoBar(); //update player money
					pPlayer->haveMule = true;
				}
			}
			else if(pPlayer->pos_y >= 317 &&
					pPlayer->pos_x >= 505 &&
					pPlayer->pos_x <= 514 && !pPlayer->haveMule)
			{
				//actually in the pub store, do in store commands here
				srand(time(NULL));
				int gamble = rand() %150; //can win up to 150 bux
				pPlayer->money += gamble;
				dest.x = 245;
				dest.y = 420;
				dest.w = 330;
				dest.h = 30;
				UpdatePlayerInfoBar(); //update player money
				//erase old store text
				SDL_BlitSurface(m_pDevel, &dest, screen, &dest);
				char* moneytext = (char*)malloc(50);
				snprintf(moneytext, 49, "You just won $%d from gambling!", gamble);
				//update gamble winnings
				data->gfx.setFont(m_screenfont, 10);
				data->gfx.drawFont(moneytext, 245, 435);
				free(moneytext);

				SDL_UpdateRects(screen, 1, &dest);
				SDL_Delay(1000);
				//
				// when ready to go to production stage:
				// send a PLAYER_SELECT message to the server
				//
				pPlayer->inStore = false;
				MessageEvent event;
				event.code = MSG_CODE_PLAYER_SELECT;
				m_client.dispatch(event);
				return 1;
			}
			if(pPlayer->pos_y >= 297 &&
			   pPlayer->pos_y < 345 &&
			   pPlayer->pos_x >= 360 &&
			   pPlayer->pos_x <= 418)
			{
				//in mule store door
				pPlayer->pos_y++;
			}
			else if(pPlayer->pos_y >= 297 &&
					pPlayer->pos_y < 317 &&
					pPlayer->pos_x >= 505 &&
					pPlayer->pos_x <= 514 && !pPlayer->haveMule)
			{
				//in PUB store door
				pPlayer->pos_y++;
			}
			else if(pPlayer->pos_y >= 297 &&
					pPlayer->pos_y < 347 &&
					pPlayer->pos_x >= 505 &&
					pPlayer->pos_x <= 514 && pPlayer->haveMule)
			{
				// OMFG THEY ARE TRYING TO TAKE A MULE INTO THE PUB
				dest.x = 245;
				dest.y = 435;
				dest.w = 335;
				dest.h = 15;
				// erase 3rd line of text
				SDL_BlitSurface(m_pDevel, &dest, screen, &dest);
				data->gfx.drawFont("no mules allowed!", 245, 435);
				SDL_UpdateRects(screen, 1, &dest);
			}
			else if(pPlayer->pos_y < 297)
			{
				//in the black area in middle
				pPlayer->pos_y++;
			}
		}
		else
		{
			//
			// not in store
			//
			if(pPlayer->pos_y == 237 &&
			   pPlayer->pos_x > 325 &&
			   pPlayer->pos_x < 452)
			{
				//entering store from the top
				pPlayer->inStore = true;
				pPlayer->pos_x = 389;
				pPlayer->pos_y = 275;
				InsideStoreView(1);
			}
			else if(pPlayer->pos_y < (114*PLOT_NY-21) && !pPlayer->haveMule)
				pPlayer->pos_y++;
			else if(pPlayer->pos_y < (114*PLOT_NY-21-13) && pPlayer->haveMule)
				pPlayer->pos_y++;
		}
	}
	if(data->m_bArrowKey[playerId][ARROW_LEFT])
	{
		pPlayer->direction = 3;
		if(pPlayer->inStore)
		{
			//boundry in store
			if(pPlayer->pos_y >= 297 &&
			   pPlayer->pos_x > 360 &&
			   pPlayer->pos_x < 422)
			{
				//inside mule store, left wall
				pPlayer->pos_x--;
			}
			else if(pPlayer->pos_y >= 297 &&
					pPlayer->pos_x > 505)
			{
				//inside PUB store, left wall
				pPlayer->pos_x--;
			}
			else if(pPlayer->pos_y <= 251 &&
					pPlayer->pos_x > 266 &&
					pPlayer->pos_x < 313)
			{
				//inside oil store, left wall
				pPlayer->pos_x--;
			}
			else if(pPlayer->pos_y <= 251 &&
					pPlayer->pos_x > 343 &&
					pPlayer->pos_x < 388)
			{
				//inside ore store, left wall
				pPlayer->pos_x--;
			}
			else if(pPlayer->pos_y <= 251 &&
					pPlayer->pos_x > 422 &&
					pPlayer->pos_x < 469)
			{
				//inside energy store, left wall
				pPlayer->pos_x--;
			}
			else if(pPlayer->pos_y <= 251 &&
					pPlayer->pos_x > 499 &&
					pPlayer->pos_x < 544)
			{
				//inside food store, left wall
				pPlayer->pos_x--;
			}
			else if(pPlayer->pos_x == 242)
			{
				//we have left the store on the left side
				pPlayer->inStore = false;
				pPlayer->pos_x = 321;
				pPlayer->pos_y = 275;
				dest.x = 228;
				dest.y = 114;
				dest.w = 342;
				dest.h = 342;
				SDL_BlitSurface(data->m_pLand, &dest, screen, &dest);
				SDL_UpdateRects(screen, 1, &dest);
			}
			else if(pPlayer->pos_y <= 297 &&
					pPlayer->pos_y >= 251 &&
					pPlayer->pos_x > 242)
			{
				//in the black area in middle
				pPlayer->pos_x--;
				InsideStoreView(2);
			}
		}
		else
		{
			//
			// not in store
			//
			if(pPlayer->pos_x == 453 &&
			   pPlayer->pos_y > 237 &&
			   pPlayer->pos_y < 323)
			{
				//entering store from the right
				pPlayer->inStore = true;
				pPlayer->pos_x = 389;
				pPlayer->pos_y = 275;
				InsideStoreView(1);
			}
			else if(pPlayer->pos_x > 0)
				pPlayer->pos_x--;
		}
	}
	if(data->m_bArrowKey[playerId][ARROW_RIGHT])
	{
		pPlayer->direction = 1;
		if(pPlayer->inStore)
		{
			//boundry in store
			if(pPlayer->pos_y > 297 &&
			   pPlayer->pos_x < 418)
			{
				//inside mule store, right wall
				pPlayer->pos_x++;
			}
			else if(pPlayer->pos_y >= 297 &&
					pPlayer->pos_x < 514 &&
					pPlayer->pos_x > 418)
			{
				//inside PUB store, right wall
				pPlayer->pos_x++;
			}
			else if(pPlayer->pos_y <= 251 &&
					pPlayer->pos_x < 275)
			{
				//inside oil store, right wall
				pPlayer->pos_x++;
			}
			else if(pPlayer->pos_y <= 251 &&
					pPlayer->pos_x < 354 &&
					pPlayer->pos_x > 275)
			{
				//inside ore store, right wall
				pPlayer->pos_x++;
			}
			else if(pPlayer->pos_y <= 251 &&
					pPlayer->pos_x < 431 &&
					pPlayer->pos_x > 354)
			{
				//inside energy store, right wall
				pPlayer->pos_x++;
			}
			else if(pPlayer->pos_y <= 251 &&
					pPlayer->pos_x < 510 &&
					pPlayer->pos_x > 431)
			{
				//inside food store, right wall
				pPlayer->pos_x++;
			}
			else if(pPlayer->pos_x == 535)
			{
				//we have left the store on right side
				pPlayer->inStore = false;
				pPlayer->pos_x = 456;
				pPlayer->pos_y = 275;
				dest.x = 228;
				dest.y = 114;
				dest.w = 342;
				dest.h = 342;
				SDL_BlitSurface(data->m_pLand, &dest, screen, &dest);
				SDL_UpdateRects(screen, 1, &dest);
			}
			else if(pPlayer->pos_y <= 297 &&
					pPlayer->pos_y >= 251 &&
					pPlayer->pos_x < 535)
			{
				//in the black area in middle
				pPlayer->pos_x++;
				InsideStoreView(2);
			}
		}
		else
		{
			//
			// not in store
			//
			if(pPlayer->pos_x == 326 &&
			   pPlayer->pos_y > 237 &&
			   pPlayer->pos_y < 323)
			{
				//entering store from the left
				pPlayer->inStore = true;
				pPlayer->pos_x = 389;
				pPlayer->pos_y = 275;
				InsideStoreView(1); //update inside store view
			}
			else if(pPlayer->pos_x < (114*PLOT_NX-21) && !pPlayer->haveMule)
				pPlayer->pos_x++;
			else if(pPlayer->pos_x < (114*PLOT_NX-21-13) && pPlayer->haveMule)
				pPlayer->pos_x++;
		}
	}
	draw_player(pPlayer, data->playerimg[pPlayer->playerId]);
	if(pPlayer->haveMule) {
		mule_pos.x = pPlayer->pos_x + 13;
		mule_pos.y = pPlayer->pos_y + 13;
        draw_mule(pPlayer, m_mulesprite);
	}

	SDL_Delay(10); //after player moves, delay to slow movement
	//SDL_Flip(screen);
	return 1;
}


int ClientPhaseSDL_Devel::OnSelect()
{
	SDL_Rect dest, playerdest;
	Uint32 clr = 0;
	SDL_Surface* res;
	SDL_Surface* house;
	int nPlot;
	int plots[4][2];
	ResourceType swap;

	MessagePlayer *pPlayer = getCurPlayer();

	if(pPlayer->inStore || !pPlayer->haveMule)
		return 1; // select has no effect

	nPlot = get_plot(pPlayer, plots);
	if(nPlot != 1) {
		// valid selection only if plyaer is on one plot.
		// TODO: make mule run off...
		return 1; 
	}

	if(m_state.plot[plots[0][0]][plots[0][1]]->owner == pPlayer->playerId)
	{
		//
		// we are the owner of this plot
		//
		if(pPlayer->muleRes > RES_NONE && pPlayer->muleRes < MAX_RESOURCE)
			res = data->m_pPlotRes[pPlayer->muleRes];
		else
		{
			SDL_Rect warntext;
			Uint32 color = SDL_MapRGB(screen->format, 0, 0, 0);
			warntext.x = SCREEN_WIDTH - 245;
			warntext.y = SCREEN_HEIGHT - 24;
			warntext.w = 245;
			warntext.h = 11;
			SDL_FillRect(screen, &warntext, color);  //erase old warning text
			data->gfx.setFont(data->playersmallfont[pPlayer->playerId], 10);
			data->gfx.drawFontRightJustify("Mule has no resource!",
					SCREEN_WIDTH - 5, SCREEN_HEIGHT - 24);
			SDL_UpdateRects(screen,1,&warntext);
			SDL_Delay(2000); // hack to wait
			SDL_FillRect(screen, &warntext, color);  //erase old warning text
			SDL_UpdateRects(screen,1,&warntext);
			return 0;
		}

		swap = m_state.plot[plots[0][0]][plots[0][1]]->resOutfit;
		m_state.plot[plots[0][0]][plots[0][1]]->resOutfit = pPlayer->muleRes;
		if(swap == RES_NONE) // No resource previously on plot;
		{
			pPlayer->haveMule = false;
			pPlayer->muleRes = RES_NONE;
			// erase the mule from the screen;
			SDL_BlitSurface(data->m_pLand, &mule_pos, screen, &mule_pos);
			SDL_UpdateRects(screen, 1, &mule_pos);
		}
		else if(swap != RES_NONE)
		{
			// If plot has a resource already,
			// mule gets outiftted with that resource.
			house = data->m_pPlotHouse;
			if(!house) {
				ERROR << "Failed to load: ./data/gfx/sprites/house.png" << endl;
				return 0;
			}
			pPlayer->muleRes = swap;
			// Redraw the entire plot with the plot type, the rect around it,
			// the mountain, and the house, so we can put new resource on it.
			// Redraw the plot itself
			dest.x = plots[0][0] * 114;
			dest.y = plots[0][1] * 114;
			dest.w = 114;
			dest.h = 114;
			switch(m_state.plot[plots[0][0]][plots[0][1]]->terrain)
			{
			case TERRAIN_DIRT:
				SDL_BlitSurface(data->pTileDirt, NULL, screen, &dest);
				break;
			case TERRAIN_GRASS:
				SDL_BlitSurface(data->pTileGrass, NULL, screen, &dest);
				break;
			case TERRAIN_RIVER:
				SDL_BlitSurface(data->pTileWater, NULL, screen, &dest);
				break;
			case TERRAIN_STORE:
				SDL_BlitSurface(data->pTileStore, NULL, screen, &dest);
				break;
			default:
				break;
			}
			// Redraw the mountain
			if(m_state.plot[plots[0][0]][plots[0][1]]->mountain)
				SDL_BlitSurface(data->pTileMountain, NULL, screen, &dest);
			// Redraw the house
			dest.x = plots[0][0]*114 + 10;
			dest.y = plots[0][1]*114 + 10;
			dest.w = 40;
			dest.h = 40;
			SDL_BlitSurface(house, NULL, screen, &dest); //put house symbol

			// Redraw the rect
			switch(pPlayer->playerId)
			{
			case 0:
				clr = SDL_MapRGB(screen->format, 255, 0, 0); //red
				break;
			case 1:
				clr = SDL_MapRGB(screen->format, 240, 0, 255); //pink
				break;
			case 2:
				clr = SDL_MapRGB(screen->format, 255, 246, 0); //yellow
				break;
			case 3:
				clr = SDL_MapRGB(screen->format, 0, 252, 255); //light_blue
				break;
			}
			dest.x = plots[0][0] * 114;
			dest.y = plots[0][1] * 114;
			dest.w = 114;
			dest.h = 114;
			data->gfx.drawRect(&dest, 4, clr);
			SDL_BlitSurface(screen, &dest, data->m_pLand, &dest);
		} // end elseif swap != 0

		playerdest.x = pPlayer->pos_x;
		playerdest.y = pPlayer->pos_y;
		playerdest.w = 22;
		playerdest.h = 22;
		// temp erase player from screen
		SDL_BlitSurface(data->m_pLand, &playerdest, screen, &playerdest);
		if(pPlayer->haveMule)
			// temp erase old mule
			SDL_BlitSurface(data->m_pLand, &mule_pos, screen, &mule_pos);

		//put resource on the plot
		dest.x = plots[0][0]*114 + 65;
		dest.y = plots[0][1]*114 + 65;
		dest.w = 40;
		dest.h = 40;
		SDL_BlitSurface(res, NULL, screen, &dest);

		//write current screen to background
		SDL_BlitSurface(screen, &dest, data->m_pLand, &dest);

		draw_player(pPlayer, data->playerimg[pPlayer->playerId]);
		if(pPlayer->haveMule) {
			draw_mule(pPlayer, m_mulesprite); //redraw mule
		}
		dest.x = plots[0][0] * 114;
		dest.y = plots[0][1] * 114;
		dest.w = 114;
		dest.h = 114;
		SDL_UpdateRects(screen, 1, &dest); //show updated screen
	}
	else if(m_state.plot[plots[0][0]][plots[0][1]]->owner != pPlayer->playerId)
	{
		//
		// this is someone elses land... take off, eh!
		//
		//get rid of mule if they try to select a plot they cannot put it on
		pPlayer->haveMule = false;
		pPlayer->muleRes = RES_NONE;
		// erase the mule from the screen;
		SDL_BlitSurface(data->m_pLand, &mule_pos, screen, &mule_pos);
		SDL_UpdateRects(screen,1,&mule_pos);
		// redraw player because mule overlap
		draw_player(pPlayer, data->playerimg[pPlayer->playerId]);
	}
	return 1;
}

//
// whattodo == 1: store view is JUST openning, need to draw
//				  the store and write text at top + mule cost
// whattodo != 1: update what players mule is outfitted for
//
int ClientPhaseSDL_Devel::InsideStoreView(int whattodo)
{
	SDL_Rect dest;
	MessagePlayer *pPlayer = getCurPlayer();

	data->gfx.setFont(m_screenfont, 10);

	if(whattodo == 1)
	{
		//draw inside store view
		dest.x = 228;
		dest.y = 114;
		dest.w = 342;
		dest.h = 342;
		SDL_BlitSurface(m_pDevel, &dest, screen, &dest);
		data->gfx.drawFont("Land Development #", 301, 128);
		data->gfx.drawFont(m_state.sys->round, 481, 128);
		//update info text
		char* moneytext = new char[50];
		snprintf(moneytext, 49, "Mule's cost $%d", STORE_INITIAL_MULE_PRICE);
		data->gfx.drawFont(moneytext, 245, 405);
		delete[] moneytext;
		SDL_UpdateRects(screen, 1, &dest);
	}
	if(pPlayer->muleRes > 0)
	{
		dest.x = 245;
		dest.y = 435;
		dest.w = 280;
		dest.h = 11;
		SDL_BlitSurface(m_pDevel, &dest, screen, &dest); //erase old text

		//update what resource
		data->gfx.drawFont("You are outfitted for ", 245, 435);
		switch(pPlayer->muleRes)
		{
		case RES_OIL:
			data->gfx.drawFont("oil", 465, 435);
			break;
		case RES_ORE:
			data->gfx.drawFont("ore", 465, 435);
			break;
		case RES_FOOD:
			data->gfx.drawFont("food", 465, 435);
			break;
		case RES_ENERGY:
			data->gfx.drawFont("energy", 465, 435);
			break;
		default:
			ERROR << "mulRes: " << pPlayer->muleRes << endl;
			return -1;
		}
		SDL_UpdateRects(screen, 1, &dest);
	}
	return 1;
}

