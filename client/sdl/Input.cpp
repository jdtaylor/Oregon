
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include "Input.h"
#include "common.h"
#include "commonSDL.h"

using namespace std;


Input::Input()
{
	int i;
	SDL_Event event;

	event.type = SDL_KEYDOWN;
	//
	// reserve the escape key and 'q' keys to quit the application
	event.key.keysym.sym = SDLK_ESCAPE;
	for(i = 0; i < MAX_PLAYER; i++)
		m_event[i][IO_QUIT].push_back(event);

	// reserve select key as the space bar
	event.key.keysym.sym = SDLK_SPACE;
	for(i = 0; i < MAX_PLAYER; i++)
		m_event[i][IO_SELECT].push_back(event);

	//
	// directional key defaults for both down and up events
	//
	event.type = SDL_KEYDOWN;
	event.key.keysym.sym = SDLK_UP;		m_event[0][IO_UP].push_back(event);
	event.key.keysym.sym = SDLK_DOWN;	m_event[0][IO_DOWN].push_back(event);
	event.key.keysym.sym = SDLK_LEFT;	m_event[0][IO_LEFT].push_back(event);
	event.key.keysym.sym = SDLK_RIGHT;	m_event[0][IO_RIGHT].push_back(event);
	event.key.keysym.sym = SDLK_q;	m_event[1][IO_UP].push_back(event);
	event.key.keysym.sym = SDLK_a;	m_event[1][IO_DOWN].push_back(event);
	event.key.keysym.sym = SDLK_t;	m_event[2][IO_UP].push_back(event);
	event.key.keysym.sym = SDLK_g;	m_event[2][IO_DOWN].push_back(event);
	event.key.keysym.sym = SDLK_o;	m_event[3][IO_UP].push_back(event);
	event.key.keysym.sym = SDLK_l;	m_event[3][IO_DOWN].push_back(event);
	

	event.type = SDL_KEYUP;
	event.key.keysym.sym = SDLK_UP;		m_event[0][IO_UP].push_back(event);
	event.key.keysym.sym = SDLK_DOWN;	m_event[0][IO_DOWN].push_back(event);
	event.key.keysym.sym = SDLK_LEFT;	m_event[0][IO_LEFT].push_back(event);
	event.key.keysym.sym = SDLK_RIGHT;	m_event[0][IO_RIGHT].push_back(event);
	event.key.keysym.sym = SDLK_q;	m_event[1][IO_UP].push_back(event);
	event.key.keysym.sym = SDLK_a;	m_event[1][IO_DOWN].push_back(event);
	event.key.keysym.sym = SDLK_t;	m_event[2][IO_UP].push_back(event);
	event.key.keysym.sym = SDLK_g;	m_event[2][IO_DOWN].push_back(event);
	event.key.keysym.sym = SDLK_o;	m_event[3][IO_UP].push_back(event);
	event.key.keysym.sym = SDLK_l;	m_event[3][IO_DOWN].push_back(event);
}

InputType Input::get(SDL_Event& event, int& playerId, bool& bStart)
{
	unsigned int i, j;
	bStart = false;

	for(j = 0; j < MAX_PLAYER; j++)
	{
		for(i = IO_NULL; i < MAX_IO; i++)
		{
			vector<SDL_Event>::iterator iEvent = m_event[j][i].begin();
			for( ; iEvent != m_event[j][i].end(); iEvent++)
			{
				SDL_Event e = *iEvent;
				if(e == event) {
					if(e.type == SDL_KEYDOWN)
						bStart = true;
					if(e.type == SDL_MOUSEBUTTONDOWN)
						bStart = true;
					break;
				}
			}
			if(iEvent != m_event[j][i].end())
				break;
		}

		if(i != MAX_IO)
			break;
	}
	if(j < MAX_PLAYER) {
		playerId = j;
		return (InputType)i;
	}
	return IO_NULL;
}

int Input::add(InputType type, int playerId, SDL_Event& event)
{
	if(type == IO_NULL || type == MAX_IO) {
		ERROR << "Invalid input type: " << type << endl;
		return -1;
	}
	m_event[playerId][type].push_back(event);
	return 1;
}


