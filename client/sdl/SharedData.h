
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */


#ifndef SHARED_DATA_H
#define SHARED_DATA_H


#include "SDL.h"
#include "IClient.h"
#include "Input.h"
#include "GFX.h"

enum
{
	ARROW_UP=0,
	ARROW_DOWN,
	ARROW_LEFT,
	ARROW_RIGHT,
	MAX_ARROW
};

class SharedData
{
public:
	SharedData(IClient& client);
	~SharedData();

	void blitMap(SDL_Surface *pSurface);

//	SDL_Surface *m_pPlayer;
	SDL_Surface *m_pLand;
	SDL_Surface *pTileDirt;
	SDL_Surface *pTileGrass;
	SDL_Surface *pTileWater;
	SDL_Surface *pTileStore;
	SDL_Surface *pTileMountain;

	//
	// The resource icon that shows up at the bottom
	// right of the plot after outfitting it.
	//
	SDL_Surface *m_pPlotRes[MAX_RESOURCE];
	//
	// the little house that is placed at the top 
	// right of each plot when owned.
	//
	SDL_Surface *m_pPlotHouse;

	SDL_Surface* playersmallfont[MAX_PLAYER]; //size 10 font img_load to here
	SDL_Surface* playerbigfont[MAX_PLAYER]; //size 24 font img_load to here
	SDL_Surface* playerimg[MAX_PLAYER];
	int footstep[MAX_PLAYER]; // 0=first, 1=second
	int footstepcount[MAX_PLAYER];

	bool m_bArrowKey[MAX_PLAYER][MAX_ARROW];
	Input m_input;
	GFX gfx;

protected:
	IClient& m_client;

private:
	SharedData();
	SharedData& operator=(const SharedData&);
};

#endif

