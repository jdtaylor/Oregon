
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include "ClientPhaseSDL_Standings.h"
#include "common.h"

using namespace std;


ClientPhaseSDL_Standings::ClientPhaseSDL_Standings(IClient& client)
	: ClientPhaseSDL(client)
{
	m_totalfont = IMG_Load("./data/gfx/fonts/24br.png");
	if(!m_totalfont) {
		ERROR << "Failed to load: data/gfx/fonts/24br.png" << endl;
		return;
	}
	m_instructionfont = IMG_Load("./data/gfx/fonts/10w.png");
	if(!m_instructionfont) {
		ERROR << "Failed to load: data/gfx/fonts/10w.png" << endl;
		return;
	}
}

int ClientPhaseSDL_Standings::open()
{
	int i;
	SDL_Rect dest;
	MessagePlayer *pPlayer;
	int total[MAX_PLAYER];
	int highesttotal = -1;
	int colonytotal = 0;
	int goodsValue;
	showingCredits = false;
	Uint32 color = SDL_MapRGB(screen->format, 57, 57, 57);

	dest.x = 0;
	dest.y = 0;
	dest.w = SCREEN_WIDTH;
	dest.h = 570;
	//display solid black background
	data->gfx.drawFilledRect(&dest, color);
	data->gfx.setFont(m_totalfont, 24);
	data->gfx.drawFont("Status Summary #", 189, 15);
	data->gfx.drawFont(m_state.sys->round, 573, 15);

	//calculate totals
	for(i = 0; i < MAX_PLAYER; i++)
	{
        pPlayer = m_state.player[i];
		goodsValue = m_state.playerGoodsValue(pPlayer);
		total[i] = pPlayer->money + pPlayer->nPlotOwned*PLOT_VALUE + goodsValue;
		if(total[i] > highesttotal)
			highesttotal = total[i];
	}
	for(int i = 0; i < MAX_PLAYER; i++)
	{
		pPlayer = m_state.player[i];
		data->gfx.setFont(data->playerbigfont[i], 24);

        goodsValue = m_state.playerGoodsValue(pPlayer);

		pPlayer->direction = 2;
		pPlayer->pos_x = 10;
		pPlayer->pos_y = 62+i*120;
		draw_player(pPlayer, data->playerimg[pPlayer->playerId]);
		
		data->gfx.drawFontRightJustify(total[i], 610, 135+i*120);
		if(total[i] == highesttotal)
			data->gfx.drawFont("*", 615, 135+i*120);

		data->gfx.drawFont("Money", 295, 60+i*120);
		data->gfx.drawFontRightJustify(pPlayer->money, 610, 60+i*120);
		data->gfx.drawFont("Land", 295, 85+i*120);
		data->gfx.drawFontRightJustify(pPlayer->nPlotOwned * PLOT_VALUE,
				610, 85+i*120);
		data->gfx.drawFont("Goods", 295, 110+i*120);
		data->gfx.drawFontRightJustify(goodsValue, 610, 110+i*120);
		data->gfx.drawFont(pPlayer->name.data(), 10, 135+i*120);
		data->gfx.drawFont("Total", 295, 135+i*120);

		colonytotal += total[i];
	}

	data->gfx.setFont(m_totalfont, 24);
	data->gfx.drawFont("Colony", 271, 540);
	data->gfx.drawFontRightJustify(colonytotal, 610, 540);

	data->gfx.setFont(m_instructionfont, 10);
	data->gfx.drawFont("instructions for player standings:", 10, 580);
	data->gfx.drawFont("*Press the [space bar] key to continue", 20, 595);
	if(m_state.sys->round == MAX_GAME_ROUNDS && !showingCredits) {
		data->gfx.drawFont("*That was the last round, the game has ended",
				20, 610);
	}

	dest.x = 0;
	dest.y = 0;
	dest.w = SCREEN_WIDTH;
	dest.h = SCREEN_HEIGHT;
	SDL_UpdateRects(screen, 1, &dest);

	return ClientPhaseSDL::open();
}

int ClientPhaseSDL_Standings::close()
{
	return ClientPhaseSDL::close();
}

int ClientPhaseSDL_Standings::execute()
{
	return ClientPhaseSDL::execute();
}

int ClientPhaseSDL_Standings::doFrame()
{
	return ClientPhaseSDL::doFrame();
}

int ClientPhaseSDL_Standings::msgHandler(Message& msg)
{
	return ClientPhaseSDL::msgHandler(msg);
}

int ClientPhaseSDL_Standings::OnEvent(SDL_Event& event)
{
	InputType inputType;
	int playerId;
	bool bStart;
	//
	// resort to mutable input class to translate SDL events into
	// game events.
	//
	inputType = data->m_input.get(event, playerId, bStart);
	switch(inputType)
	{
	case IO_SELECT:
		OnSelect(playerId);
		break;
	case IO_QUIT:
		return 0;
	default:
		// ignore
		break;
	}
	return 1;
}

int ClientPhaseSDL_Standings::OnSelect(int playerId)
{
	int i;

	if(m_state.sys->round == MAX_GAME_ROUNDS && !showingCredits)
	{
		//
		// GAME IS OVER DO SOMETHING ABOUT IT
		//
		SDL_Rect dest;
		showingCredits = true;
		Uint32 color = SDL_MapRGB(screen->format, 0, 0, 0);

		data->gfx.setFont(m_totalfont, 24);

		dest.x = 0;
		dest.y = 0;
		dest.w = SCREEN_WIDTH;
		dest.h = SCREEN_HEIGHT;
		data->gfx.drawFilledRect(&dest, color); //erase screen
		SDL_Surface* m = IMG_Load("./data/gfx/sprites/donkey.png");
		SDL_Rect picarea;
		picarea.x = 0;
		picarea.y = 0;
		picarea.w = 22;
		picarea.h = 22;
		dest.h = 22;
		dest.w = 22;
		dest.y = 0;
		for(i = 0; i < 10; i++) {
			dest.x = i*SCREEN_WIDTH/10 + SCREEN_WIDTH/20 - 11;
			SDL_BlitSurface(m, &picarea, screen,&dest);
		}
		data->gfx.drawFont("CREDITS", 315, 26);
		color = SDL_MapRGB(screen->format, 255, 255, 255);
		dest.x = 315;
		dest.y = 52;
		dest.w = 168;
		dest.h = 3;
		data->gfx.drawFilledRect(&dest, color);
		data->gfx.drawFont("Reza Assadi", 267, 100);
		data->gfx.drawFont("Danny Bousleiman", 207, 175);
		data->gfx.drawFont("Jeffrey Jardiolin", 195, 250);
		data->gfx.drawFont("John Knieper", 255, 325);
		data->gfx.drawFont("Mike Mack", 291, 400);
		data->gfx.drawFont("Rajiv Shah", 279, 475);
		data->gfx.drawFont("James D. Taylor", 219, 550);
		data->gfx.drawFont("Roland Vassallo", 219, 625);

		dest.x = 0;
		dest.y = 0;
		dest.w = SCREEN_WIDTH;
		dest.h = SCREEN_HEIGHT;
		SDL_UpdateRects(screen, 1, &dest);
    }
	else if(showingCredits)
	{
		//close window
		SDL_Quit();
		exit(0);
	}
	else
	{
		//
		//when ready to go back to grant stage:
		// send a PLAYER_SELECT message to the server
		//
		MessageEvent event;
		event.code = MSG_CODE_PLAYER_SELECT;
		m_client.dispatch(event);
	}
	return 1;
}

