
/*
 * This file is part of Oregon - A game of capitalism.
 *
 *   Copyright (C) 2006  UH COSC4351 Oregon Team Fall 2006
 * 
 *   Oregon is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 * 
 *   Oregon is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 * 
 *   You should have received a copy of the GNU General Public License
 *   along with Oregon; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Contacts:
 *		Reza Assadi <gassadi@uh.edu>
 *		Danny Bousleiman <dan_man89@hotmail.com>
 *		Jeffrey Jardiolin <jjardiolin@uh.edu>
 *		John Knieper <jknieper@gmail.com>
 *		Mike Mack <mikemackjr@gmail.com>
 *		Rajiv Shah <brainix@gmail.com>
 *		James D. Taylor <james.d.taylor@gmail.com>
 *		Roland Vassallo <rcvassallo@gmail.com>
 */

#include "Client.h"
#include "common.h"
#include "Messages.h"
#include "sdl/ClientPhaseFactorySDL.h"
#include "ai/ClientPhaseFactoryAI.h"
#include "RemoteHost.h"

using namespace std;

Client::Client(ClientFrontend fe)
{
	//unsigned int prand; // for calls to rand_r()
	ClientPhaseFactory *pPhaseFactory;

	Server_start();

	//
	// This will construct a factory class which implements the 
	// particular front-end version of the phases in the game.  It
	// allows us to create generic ClientPhase's for each PhaseType.
	//
	// We pass it a reference to ourself as a reference to IClient.
	// The factory will construct each phase while propagating this
	// reference to all of them.  This is so they can call methods of
	// the Client class without recursive includes..
	//
	switch(fe)
	{
	case CLIENT_FRONTEND_AI:
		pPhaseFactory = (ClientPhaseFactory*)new ClientPhaseFactoryAI(*this);
		break;
	default:
	case CLIENT_FRONTEND_SDL:
		pPhaseFactory = (ClientPhaseFactory*)new ClientPhaseFactorySDL(*this);
		break;
	}

	//
	// fill in the m_phase array with pointers to the newly
	// constructed phases.
	//
	pPhaseFactory->create(m_phase); // constructs each phase class

	//
	// we only need the factory for creation.  we don't need it anymore.
	//
	delete pPhaseFactory;

	//
	// start in the title phase
	//
	m_state.sys->phase = PHASE_TITLE;
	/*
	{
		//
		// hack to concentrate on the auction phase
		//
		m_state.sys->phase = PHASE_TITLE;

		int i, res;
		for(i = 0; i < MAX_PLAYER; i++)
		{
			for(res = 0; res < MAX_RESOURCE; res++)
			{
				MessagePlayer *pPlayer = m_state.player[i];

				pPlayer->prev[res] = RAND(&prand, 0, g_playerResMax[res]);
				pPlayer->use[res] = RAND(&prand, 0, 10);
				pPlayer->spoil[res] = RAND(&prand, 0, 10);
				pPlayer->prod[res] = RAND(&prand, 0, 10);
				pPlayer->need[res] = RAND(&prand, 0, g_playerResNeedMax[res]);

				if(pPlayer->use[res] > pPlayer->prev[res])
					pPlayer->use[res] = pPlayer->prev[res];

				pPlayer->spoil[res] = RAND(&prand, 0, 10);
				if(pPlayer->spoil[res] + pPlayer->use[res] 
					> pPlayer->prev[res])
				{
					pPlayer->use[res] = pPlayer->prev[res] / 2;
					pPlayer->spoil[res] = pPlayer->use[res];
				}
			}
		}
	}
	*/

	//
	// tell the phase to wake up and draw its stuff
	//
	m_phase[m_state.sys->phase]->open();
}

Client::~Client()
{
}

int Client::setServer(const char *host, short port)
{
	//
	// ClientTransit::connect()
	//
	return m_server.connect(host, port);
}

int Client::execute()
{
	vector<Message*> msgs;
	//
	// immediately execute the current phase
	//
	if(m_phase[m_state.sys->phase]->execute() <= 0)
		return -1; // exit app

	//
	// check for any messages from the server
	//
/*
	try
	{
		msgs = m_server.poll();
		if(msgs.size() == 0)
			return 1; // no messages from server
	}
	catch(RemoteCloseException& e)
	{
		// server closed connection
		ERROR << "Server closed network connection." << endl;
		return 1;
	}
*/
	vector<Message*>::iterator iter = msgs.begin();
	for(; iter != msgs.end(); ++iter)
	{
		Message* msg = *iter;
		//
		// catch any messages we would be interested in at the low level,
		// then pass the message to the current phase.
		//
		switch(msg->type)
		{
			case MSG_EVENT:
				if(OnMsgEvent(*dynamic_cast<MessageEvent*>(msg)) <= 0)
					return -1;
				break;
			case MSG_PLAYER:
				if(OnMsgPlayer(*dynamic_cast<MessagePlayer*>(msg)) <= 0)
					return -1;
				break;
			default:
				break;
		}
		//
		// pass message to the current phase
		//
		if(m_phase[m_state.sys->phase]->msgHandler(*msg) <= 0)
			return -1;
	}
	return 1;
}

int Client::dispatch(Message& msg)
{
	//MessageEvent *pEvent;
	//
	// NOTE: this is a temporary hack to emulate a server.  catch any
	// outgoing events and try to do what is expected based on those
	// events.
	//
	switch(msg.type)
	{
	case MSG_EVENT:
		return Server_OnMsgEvent((MessageEvent&)msg);
	default:
		break;
	}
	return 1; // don't try to send any messages to the server

	//
	// no biggie here, just forward the message to ClientTransit::send()
	//
	return m_server.send(msg);
}

int Client::OnMsgEvent(MessageEvent& event)
{
	switch(event.code)
	{
	case MSG_CODE_GAME_START:
		break;
	case MSG_CODE_GAME_STOP:
		break;
	case MSG_CODE_PHASE_CHANGE:
		//
		// call the current phases close() method, advance to new
		// phase, and call the new phases open() method.
		//
		break;
	default:
		break;
	}
	return 1;
}

int Client::OnMsgPlayer(MessagePlayer& player)
{
	return 1;
}

int Client::Server_start()
{
	m_state.mapRandomize();
	return 1;
}

int Client::Server_advancePhase()
{
	PhaseType newPhase;
	State& state = getState();

	switch(state.sys->phase)
	{
	case PHASE_TITLE:
		newPhase = PHASE_GRANT;
		//newPhase = PHASE_AUCTION;
		break;
	case PHASE_GRANT:
		state.sys->round++;
		state.sys->curPlayer = 0;
		newPhase = PHASE_DEVEL;
		break;
	case PHASE_DEVEL:
		newPhase = PHASE_PROD;
		break;
	case PHASE_PROD:
		newPhase = PHASE_AUCTION;
		break;
	case PHASE_AUCTION:
		newPhase = PHASE_STANDINGS;
		/*
		//
		// hack to concentrate on the auction phase
		//
		newPhase = PHASE_AUCTION;

		int i, res;
		for(i = 0; i < MAX_PLAYER; i++)
		{
			for(res = 0; res < MAX_RESOURCE; res++)
			{
				MessagePlayer *pPlayer = m_state.player[i];

				pPlayer->prev[res] -= pPlayer->use[res];
				pPlayer->prev[res] -= pPlayer->spoil[res];
				pPlayer->prev[res] += pPlayer->prod[res];

				pPlayer->use[res] = RAND(&prand, 0, 10);
				pPlayer->spoil[res] = RAND(&prand, 0, 10);
				pPlayer->prod[res] = RAND(&prand, 0, 10);
				pPlayer->need[res] = RAND(&prand, 0, g_playerResNeedMax[res]);

				if(pPlayer->use[res] > pPlayer->prev[res])
					pPlayer->use[res] = pPlayer->prev[res];

				pPlayer->spoil[res] = RAND(&prand, 0, 10);
				if(pPlayer->spoil[res] + pPlayer->use[res] 
					> pPlayer->prev[res])
				{
					pPlayer->use[res] = pPlayer->prev[res] / 2;
					pPlayer->spoil[res] = pPlayer->use[res];
				}
			}
		}
		*/
		break;
	case PHASE_STANDINGS:
		newPhase = PHASE_GRANT;
		break;
	default:
		newPhase = PHASE_TITLE;
		break;
	}
	//
	// close() old phase, and open() new phase
	//
	m_phase[state.sys->phase]->close();
	state.sys->phase = newPhase;
	m_phase[state.sys->phase]->open();
	return 1;
}

int Client::Server_OnMsgEvent(MessageEvent& event)
{
	switch(event.code)
	{
		case MSG_CODE_PLAYER_SELECT:
			return Server_OnPlayerSelect(event);
		default:
			break;
	}
	return 1;
}

int Client::Server_OnPlayerSelect(MessageEvent& event)
{
	State& state = getState();

	switch(state.sys->phase)
	{
	case PHASE_TITLE:
		state.sys->curPlayer++;
		if(state.sys->curPlayer >= MAX_PLAYER) {
			//
			// all the players have selected their names, etc.
			// Advance to the land Grant stage.
			//
			state.sys->curPlayer = 0;
			Server_advancePhase();
		}
		else
			m_phase[state.sys->phase]->open(); // re-render screen
		break;
	case PHASE_GRANT:
		state.sys->curPlayer++;
		if(state.sys->curPlayer >= MAX_PLAYER) {
			//
			// all the players have selected their plots of land.
			// Advance to the land Development stage.
			//
			state.sys->curPlayer = 0;
			Server_advancePhase();
		}
		else
			m_phase[state.sys->phase]->open(); // re-render screen
		break;
	case PHASE_DEVEL:
		state.sys->curPlayer++;
		if(state.sys->curPlayer >= MAX_PLAYER) {
			//
			// Advance to the land Production phase.
			//
			state.sys->curPlayer = 0;
			Server_advancePhase();
		}
		else
			m_phase[state.sys->phase]->open(); // re-render screen
		break;
	case PHASE_PROD:
		state.sys->curPlayer = 0;
		Server_advancePhase();
		break;
	case PHASE_AUCTION:
		Server_advancePhase();
		break;
	case PHASE_STANDINGS:
		Server_advancePhase();
		break;
	default:
		break;
	}
	return 1;
}

